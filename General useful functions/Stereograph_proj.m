function[] = Stereograph_proj(edgecoor_sph,stainint,ext,viewpoint,ah)

if nargin == 1
    stainint = edgecoor_sph(:,3);
    stainmap = brewermap(128,'Rdbu');
elseif isempty(stainint)
    stainint = edgecoor_sph(:,3); 
    stainmap = brewermap(128,'Rdbu');
else 
    stainmap = repmat(linspace(.05,.95,256)',1,3);
end

if nargin == 2
    ext = 0.2;
end

if nargin < 4
    viewpoint = 'both';
elseif isempty(viewpoint)
    viewpoint = 'both';
end

if nargin <5
    ah = [];
else
    if isempty(viewpoint)
        error('If an axis is specified, viewpoint needs to be "base" or "top"')
    elseif strcmp(viewpoint,'both')
        error('If an axis is specified, viewpoint needs to be "base" or "top"')
    end
end

theta = edgecoor_sph(:,1);
phi   = edgecoor_sph(:,2);
R     = edgecoor_sph(:,3);

%[theta,phi] = Align_spheres_by_Rotation([theta phi],double([0.5*pi 0.5*pi]));
%theta = theta{:};
%phi   = phi{:};
%[theta,phi] = Align_spheres_by_Rotation([theta phi],double([-0.5*pi 0]));
%theta = theta{:};
%phi   = phi{:};
 
if any(strcmp(viewpoint,{'both','base'}))
    % Determine the points for each hemisphere
    nhsphere = phi>-ext;
    % Calculate radial position for each point
    Rnorth = cot( (phi(nhsphere)  +0.5*pi) /2 );
    % Convert polar to cartesian coordinates
    [xn,yn] = pol2cart(theta(nhsphere) ,Rnorth);
    % triangulate data
    trn = delaunay(xn,yn);
end

%Repeat for southern hemisphere
if any(strcmp(viewpoint,{'both','top'}))
    shsphere = phi< ext;
    Rsouth = cot( (-phi(shsphere)+0.5*pi) /2 );
    [xs,ys] = pol2cart(theta(shsphere),Rsouth);
    trs = delaunay(xs,ys);
end

% make a circle to show the equator
ang=0:0.01:2*pi; 
xp=cos(ang);
yp=sin(ang);

if isempty(ah)
    figure
    if strcmp(viewpoint,'both')
        ah = subplot(1,2,1);
    else
        ah = axes;
    end
end
   
% show the northern hemisphere
if any(strcmp(viewpoint,{'both','base'}))
    stainintn = stainint(nhsphere);
    patch(ah,'Faces',trn,'Vertices',[xn yn R(nhsphere)],'facevertexcdata',...
        mean(stainintn(trn),2),'facecolor','flat','edgecolor','none')
    set_plot_opts
    plot3(xp,yp,repmat(max(R(nhsphere)),length(xp),1),'--r');
    hold off
    cl1 = caxis(ah);
end

if strcmp(viewpoint,'both')
    ahn = ah;
    ah = subplot(1,2,2);
end

% show the southern hemisphere
if any(strcmp(viewpoint,{'both','top'}))
    stainint = stainint(shsphere);
    patch(ah,'Faces',trs,'Vertices',[xs ys R(shsphere)],'facevertexcdata',...
        mean(stainint(trs),2),'facecolor','flat','edgecolor','none')
    set_plot_opts
    plot3(xp,yp,repmat(max(R(shsphere)),length(xp),1),'--r');
    hold off
    cl2 = caxis(ah);
end

% Make sure colormaps are the same
if strcmp(viewpoint,'both')
    cl = [min(cl1(1),cl2(1)) max(cl1(2),cl2(2))];
    caxis(ahn,cl);
    caxis(ah,cl);
end

    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function set_plot_opts

        ah.Visible = 'off';
        axis(ah,'equal')
        grid(ah,'off')
        view(ah,0,90);
        set(ah,'Clipping','off')
        ah.Colormap = stainmap;  
        hold(ah,'on')
    
    end
end

    
    
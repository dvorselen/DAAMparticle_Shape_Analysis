function[varargout] = convert_structfield(signal,convert_to)

iter = length(signal);


if strcmp(convert_to,'logical')
    
    for i = 1:iter
        signal{i} = logical(signal{i});
    end
    
elseif strcmp(convert_to,'single')
    
    for i = 1:iter
        signal{i} = single(signal{i});
    end
    
elseif strcmp(convert_to,'double')
    
    for i = 1:iter
        signal{i} = double(signal{i});
    end
    
else
    
    error('Second argument should be "single", "double", or "logical"');
    
end

signal=signal(:).';   % Then make the cell into a CSL
varargout=signal(1:nargout); % varargout is used to fill up the fields of a structure for example
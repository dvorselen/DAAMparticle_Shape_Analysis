function[Xmw,Ymw,TRI] = MollweidProj(Theta,Phi)

% This function calculates map coordinates for the Mollweide projection (an
% equal area projection). It needs the Mapping toolbox.

if isempty(which('defaultm'))
    error(['Mapping toolbox appears to be missing. Please install it to use Mollweid projections.'...
        ' Alternatively, use equirectangular map projections.'])
end

mstruct = defaultm('mollweid');
if ~(any(Theta>2*pi))
    Theta = Theta/pi*180;
    Phi   = Phi/pi*180;
end
mstruct.scalefactor = 1;
mstruct = defaultm(mstruct);

[Xmw, Ymw] = mollweid(mstruct, Phi, Theta, 'geopoint', 'forward');

nanvalues = isnan(Phi);
if any(nanvalues)
    Phi(~nanvalues) = Xmw;
    Xmw = Phi;
    Phi(~nanvalues) = Ymw;
    Ymw = Phi;
end

if nargout > 2
    TRI = delaunay(Xmw,Ymw);
end



function [] = Save_Open_Figures(filetypes)
% Quick function to save open figures. Uses figure names, or ylabels for
% filenames. Can work with various filetypes, with default: epsc and tiff
% Always saves as .fig as well.

if ~exist('filetypes','var')
    filetypes = {'tiff','epsc'};
end

fhs = get(0,'Children'); 

savedir = fullfile(pwd,datestr(now,'yymmdd'));
if ~isfolder(savedir);     mkdir(savedir);      end

for ifig = 1:length(fhs)
    
    set(fhs(ifig),'renderer','Painters')
    
    if ~isempty(fhs(ifig).Name)
        fname = [num2str(fhs(ifig).Number) '_' fhs(ifig).Name];
    else
        if numel(fhs(ifig).Children)>1
            notanaxes = 1;
            whichchild = 0;
            while notanaxes
                whichchild = whichchild +1;
                if isa(fhs(ifig).Children(whichchild),'matlab.graphics.axis.Axes')
                    notanaxes = 0;
                end
            end
        else
            whichchild = 1;
        end        
        currylabel = fhs(ifig).Children(whichchild).YLabel.String;
        if iscell(currylabel)
            currylabel = [currylabel{:}];
        end
        fname = [num2str(fhs(ifig).Number) '_' currylabel];
    end
    
    fname(fname == ' ') = '_';  % Remove spaces and special characters
    fname(regexp(fname,'\W')) = [];
    
    if isempty(fname)
        fname = num2str(fhs(ifig).Number);
    end
    
    savefig(fhs(ifig),fullfile(savedir,fname))
    
    for jext = 1:length(filetypes)
        if contains(filetypes{jext},'eps')
            printeps(fhs(ifig), fullfile(savedir,fname ))
        else
            saveas(fhs(ifig),fullfile(savedir,fname ),filetypes{jext})
        end
    end
    
end
function varargout=ArraytoCSL(C)
% Turns an array into a comma seperated list. Useful for putting arrays
% into structures.

% Input: 
% C: an array

% Output:
% varargout: inputs of array as cell.

% First put the array into a cell
if isnumeric(C) || isstruct(C) 
    C=num2cell(C); 
end

% varargout is used to fill up the fields of a structure for example
varargout=repmat(C,1,nargout);
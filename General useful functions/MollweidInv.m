function[labda,phi] = MollweidInv(Xmw,Ymw)

theta = asin(Ymw/sqrt(2));
phi   = asin((2*theta + sin(2*theta)) / pi);
labda = pi*Xmw./(2*sqrt(2)*cos(theta));
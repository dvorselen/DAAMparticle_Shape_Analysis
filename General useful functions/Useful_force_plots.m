%% Plot forces 2D proj

MW = 1; % Use Mollweide projections
turn = 0; % Reassign longitudes from 0 to 360 degrees instead of -180 to 180
quiversize = 10; % Set the size of the quiver
reduce_dens = 25; % Quiver plots can be overwhelming, this gives an option to reduce the number of arrows

if exist('mesh_T_sph','var')
    tmesh_Sph = flipud(mesh_T_sph);
elseif exist('TMesh','var')
    tmesh_Sph = TMesh(:,:,[1 2 3]);
end

% Extract forces from .mat files
Fmesh = flipud(tmesh_Sph);
Fn    = Fmesh(:,:,1);
Fs    = sqrt(Fmesh(:,:,2).^2 + Fmesh(:,:,3).^2);

% Make sure that the variables latitude and longitude exist and are in
% degrees (not radians)
lons = linspace(-180,180,size(Fmesh,2)+1);
lons = lons(1:end-1);
lats = linspace(-90,90,size(Fmesh,1));
if all(lats(:)<(2*pi))
    lats = lats/pi*180;
    lons = lons/pi*180;
end

% Here we reassign longitudes from 0 to 360
if ~turn
    lonswrapped = wrapTo180(lons+180);
    [lonswrapped,sortidx] = sortrows(lonswrapped');
    lonswrapped = lonswrapped';
    Fn = Fn(:,sortidx);
    Fs = Fs(:,sortidx);
    Fscomp1 = -Fmesh(:,sortidx,2);
    Fscomp2 = Fmesh(:,sortidx,3);
else
    lonswrapped = lons;
    Fscomp1 = -Fmesh(:,:,2);
    Fscomp2 = Fmesh(:,:,3);
end

% Normal: 2D projection
figure('Name','Normal Force Map')
lons_plot = repmat(lonswrapped,size(Fs,1),1);
lats_plot = repmat(lats',1,size(Fs,2));
if MW
    [Xmw,Ymw,TRI] = MollweidProj(lons_plot(:),lats_plot(:));
    patch('faces',TRI,'vertices',[Xmw Ymw],'facevertexcdata',mean(Fn(TRI),2),'facecolor','flat','edgecolor','none');
else
    surf(lonswrapped/180*pi,lats/180*pi,zeros(size(Fn)),Fn);
end
colormap(brewermap([],'RdBu'))
axis equal
axis off
set(gca,'CameraViewAngleMode','Manual','Clipping','off')
maxc = max(abs(prctile(Fn(:),[.01 99.99])));
caxis([-maxc maxc])
view(0,90)
zoom(1)
set(get(gca,'children'),'EdgeColor','None')
add_colorscalebar

% Shear
figure('Name','Shear Force Map')
if MW
    patch('faces',TRI,'vertices',[Xmw Ymw],'facevertexcdata',mean(Fs(TRI),2),'facecolor','flat','edgecolor','none','FaceAlpha',.6);
else
    surf(lonswrapped/180*pi,lats/180*pi,zeros(size(Fs)),Fs,'FaceAlpha',.6)
end
kleurmapje = brewermap(128,'RdBu');
colormap(flipud(kleurmapje(1:64,:)));
axis equal
axis off
set(gca,'CameraViewAngleMode','Manual','Clipping','off')
view(0,90)
zoom(1)
maxcs = max(abs(prctile(Fs(:),[.01 99.99])));
caxis([0 maxcs])
set(get(gca,'children'),'EdgeColor','None')
add_colorscalebar

% Add arrows
hold on

% Average shear forces over an area to have the quivers reflect the average
% forces over the area
if reduce_dens > 1
    % Average by using the equivalent of a moving average window (circular) on a sphere  
    dims = size(lons_plot);
    ltFS = Smooth_spherical_mesh([lons_plot(:)/360*pi lats_plot(:)/360*pi Fscomp1(:) Fscomp2(:)],pi/dims(2)*reduce_dens);
    Fscomp1 = reshape(ltFS(:,3),dims);
    Fscomp2 = reshape(ltFS(:,4),dims);
    
    % Select the array with the desired number of data points
    d1sel    = 1:reduce_dens:dims(1);
    d2sel    = 1:reduce_dens:dims(2);
    d1sel    = d1sel + floor((dims(1)-max(d1sel))/2);
    if ~MW
        d2sel= d2sel + floor((dims(2)-max(d2sel))/2);
        indx = sub2ind(dims,repmat(d1sel',1,length(d2sel)),repmat(d2sel,length(d1sel),1));
    else
        Nd2sel = ceil(cosd(lats_plot(d1sel,1))*length(d2sel));
        Nd2cumsum = cumsum(Nd2sel);
        d1all = zeros(Nd2cumsum(end),1);
        d2all = zeros(Nd2cumsum(end),1);
        for i  = 1:length(d1sel)
            d2atthislat = round(1:(dims(2)/Nd2sel(i)):dims(2));
            d2atthislat = d2atthislat + floor((dims(2)-max(d2atthislat))/2);
            if i == 1
                d1all(1:Nd2cumsum(1)) = d1sel(i);
                d2all(1:Nd2cumsum(1)) = d2atthislat;
            elseif i ~= length(d1sel)
                d1all(Nd2cumsum(i-1)+1:Nd2cumsum(i)) = d1sel(i);
                d2all(Nd2cumsum(i-1)+1:Nd2cumsum(i)) = d2atthislat;
            else
                d1all(Nd2cumsum(i-1)+1:Nd2cumsum(end)) = d1sel(i);
                d2all(Nd2cumsum(i-1)+1:Nd2cumsum(end)) = d2atthislat;
            end
        end
        indx = sub2ind(dims,d1all,d2all);
    end          
    thetasel = lons_plot(indx);
    phisel   = lats_plot(indx);
    Fscomp1   = Fscomp1(indx);
    Fscomp2   = Fscomp2(indx);
else
    thetasel = lons_plot;
    phisel   = lats_plot;
end
    
% Add quivers to plot. In case of a MW projection this is pretty tricky,
% since it doesn't preserve angles. So, care has to be taken to get the angle 
% as well as the magnitude correct.
if ~ MW
    qh = quiver3(thetasel/180*pi,phisel/180*pi,ones(size(thetasel))*max(Fs(:)+1),...
        Fscomp2*quiversize/180*pi,Fscomp1*quiversize/180*pi,zeros(size(thetasel)),1,'k');
else
    quivstart_theta = thetasel(:);
    quivstart_phi   = phisel(:);
    quivend_theta = quivstart_theta + Fscomp2(:)*quiversize;
    quivend_phi   = quivstart_phi   + Fscomp1(:)*quiversize;
    % remove points beyond the map limit
    beyondmap = quivstart_theta>180 | quivstart_theta<-180 | quivstart_phi>90 | quivstart_phi<-90 ...
        | quivend_theta>180 | quivend_theta<-180  | quivend_phi>90 | quivend_phi<-90;
    quivstart_theta(beyondmap) = [];
    quivstart_phi(beyondmap) = [];
    quivend_theta(beyondmap) = [];
    quivend_phi(beyondmap) = [];

    [Xmwqs,Ymwqs] = MollweidProj(quivstart_theta,quivstart_phi);
    [Xmwqe,Ymwqe] = MollweidProj(quivend_theta,quivend_phi);

    % Check for quivers going around the map edge
    edgeloc = MollweidProj(ones(length(Ymwqs),1)*179,Ymwqs/pi*180);
    aroundedge = (Xmwqs-Xmwqe)>edgeloc;
    Xmwqe(aroundedge) = Xmwqe(aroundedge)+2*edgeloc(aroundedge);
    aroundedge = (Xmwqe-Xmwqs)>edgeloc;
    Xmwqe(aroundedge) = Xmwqe(aroundedge)-2*edgeloc(aroundedge);
    
    % Now the direction of the quivers is correct, but their magnitude
    % isn't necessarily
    Fsmag = sqrt(Fscomp1.^2+Fscomp2.^2);
    Fsmag(beyondmap) = [];
    Xdist = Xmwqe-Xmwqs;
    Ydist = Ymwqe-Ymwqs;
    Fappmag = sqrt(Xdist.^2+Ydist.^2);
    RatioFrealapp = Fsmag./Fappmag;
    indxmidpoint = round(Nd2cumsum(end)/2)+(1-rem(length(d1sel),2))*round(length(d2sel)/2);
    normratio = RatioFrealapp/RatioFrealapp(indxmidpoint);
    Xdist = Xdist.*normratio;
    Ydist = Ydist.*normratio;
    
    qh = quiver3(Xmwqs,Ymwqs,ones(size(Xmwqs))*2,Xdist,Ydist,zeros(size(Xmwqs)),1,'k');

end

set(qh,'AutoScale','off','MaxHeadSize',.5)
set(gcf,'renderer','painters')

%% Halve the normal force color map 

kleurmapje = brewermap(128,'RdBu');
colormap(kleurmapje(1:64,:));
caxis([-maxc -144.6])

% -58.5
% -144.6

%% Plot forces 3D

if exist('UMesh_cart','var')
    umesh_Cart = UMesh_cart;
    umesh_Sph = UMesh;
end

lons = linspace(0,360,size(Fmesh,2)+1);
lons = lons(1:end-1);
lats = linspace(-90+90/size(Fmesh,1),90-90/size(Fmesh,1),size(Fmesh,1));
if any(lats>(2*pi))
    lats = lats/180*pi;
    lons = lons/180*pi;
end

% Extract spherical and cartesian coordinates
theta = repmat(lons ,length(lats),1);
phi   = repmat(lats',1,length(lons));

if exist('mesh_xyz','var')
    x = mesh_xyz(:,:,1);
    y = mesh_xyz(:,:,2);
    z = mesh_xyz(:,:,3);
    x = x(:);
	y = y(:);
    z = z(:);
else
    Umesh = flipud(umesh_Cart);
    r     = repmat(r0,length(lats),length(lons));
    [x,y,z] = sph2cart(theta(:),phi(:),r(:));
    x = x+reshape(Umesh(:,:,1),[],1);
    y = y+reshape(Umesh(:,:,2),[],1);
    z = z+reshape(Umesh(:,:,3),[],1);
end

Fmesh = flipud(tmesh_Sph);
Fn    = Fmesh(:,:,1);
Fs    = sqrt(Fmesh(:,:,2).^2 + Fmesh(:,:,3).^2);

% Create triangulation
[usx,usy,usz] = sph2cart(theta(:),phi(:),ones(numel(theta),1));
K = convhulln([usx usy usz]);
TRI = triangulation([K(:,2) K(:,1) K(:,3)],x,y,z);

% Normal
figure('Name','Normal Forces Sphere')
trisurf(TRI,Fn(:))

colormap(brewermap([],'RdBu'))
axis equal
axis off
set(gca,'CameraViewAngleMode','Manual','Clipping','off')
maxc = max(abs(prctile(Fn(:),[.01 99.99])));
caxis([-maxc maxc])
set(get(gca,'children'),'EdgeColor','None')
view(0,0)

% Shear forces
figure('Name','Shear Forces Sphere')
trisurf(TRI,Fs,'FaceAlpha',1)
kleurmapje = brewermap(128,'RdBu');
colormap(flipud(kleurmapje(1:64,:)));
zoom(1.25)
axis equal
axis off
set(gca,'CameraViewAngleMode','Manual','Clipping','off')
view(0,0)
maxcs = max(abs(prctile(Fs(:),[.01 99.99])));
caxis([0 maxcs])
set(get(gca,'children'),'EdgeColor','None')

%% Add arrows 3D
hold on

quiversize = 0.02; % Set the size of the quiver
reduce_dens = 3; % Quiver plots can be overwhelming, this gives an option to reduce the number of arrows

Umesh = flipud(umesh_Cart);

dims = size(theta);
d1sel = (reduce_dens*3):reduce_dens:(dims(1)-2*reduce_dens);
%d2sel = 1:reduce_dens:dims(2);
d2sel = round(0.5*dims(2)):reduce_dens:round(1*dims(2));
%d1sel = d1sel + floor((dims(1)-max(d1sel))/2);
d2sel = d2sel + floor((dims(2)-max(d2sel))/2);

% select forces
thetasel = theta(d1sel,d2sel);
phisel   = phi(d1sel,d2sel);
rsel     = r(d1sel,d2sel);
F_the    = -Fmesh(d1sel,d2sel,2);
F_phi    = Fmesh(d1sel,d2sel,3);

%reshape and select coordinates
[xsel,ysel,zsel] = sph2cart(thetasel,phisel,rsel);

% Calculate the forces parallel to the surface
Fy   = cos(thetasel-pi).*(F_phi+F_the.*sin(phisel)); 
Fx   = sin(thetasel-pi).*(F_phi+F_the.*sin(phisel)); 
Fz   = cos(phisel).*F_the;
Ftot = sqrt(sum([Fx(:) Fy(:) Fz(:)].^2,2));
Xcoor = reshape(xsel+Umesh(d1sel,d2sel,1),[],1);
Ycoor = reshape(ysel+Umesh(d1sel,d2sel,2),[],1)-1.5;
Zcoor = reshape(zsel+Umesh(d1sel,d2sel,3),[],1);

QHs = cell(1,numel(Fx));
%qh = quiver3(Xcoor(:),Ycoor(:),Zcoor(:),Fx(:)*quiversize,Fy(:)*quiversize,Fz(:)*quiversize,1,'k');
for i = 1:numel(Fx)
    qh = quiver3(Xcoor(i),Ycoor(i),Zcoor(i),Fx(i)*quiversize,Fy(i)*quiversize,Fz(i)*quiversize,1,'k');
    set(qh,'AutoScale','off','MaxHeadSize',2,'Showarrowhead','off','LineWidth',Ftot(i)/100)
    QHs{i} = qh;
end
    set(gcf,'renderer','painters')

%get the data from regular quiver
%headWidth = 8;
%headLength = 8;
%LineLength = 0.08;

%U = qh.UData;
%V = qh.VData;
%W = qh.WData;
%X = qh.XData;
%Y = qh.YData;
%Z = qh.ZData;

%hold on;
%for iarrow = 1:length(X)
%        headWidth = 5;
%        ah = annotation('arrow',...
%            'headStyle','cback1','HeadLength',headLength,'HeadWidth',headWidth);
%        set(ah,'parent',gca);
%        set(ah,'position',[X(iarrow) Y(iarrow) Z(iarrow) LineLength*U(iarrow) LineLength*V(iarrow) LineLength*W(iarrow)]);
%end

%delete(qh)

%% Add a plane (to obscure the more distant hemisphere)

VP = get(gca,'view');
lims = get(gca,{'xlim','ylim','zlim'});
hold on

surf(cosd(VP(1))*[-10 -10; 10 10],sind(VP(1))*[-10 -10; 10 10],[-10 10; -10 10])
set(gca,{'xlim','ylim','zlim'},lims)

%% Plot the (normal) force profile along the phagocytic axis

kleurmapje = brewermap(256,'RdBu');
meanforce = mean(flipud(Fn'),1);
nsteps = length(meanforce);
xvals = linspace(-0.5*pi,0.5*pi,nsteps);

maxc = max(abs(prctile(Fn(:),[5 95])));

figure('Name','Force profile')
hold on

for i = 1:(nsteps-1)
    
    % Determine color for each segment
    currvals = [meanforce(i) meanforce(i+1)];
    coloridx  = round((mean(currvals)/maxc+1)*128);
    if coloridx < 1; coloridx = 1; end
    if coloridx > 256; coloridx = 256; end
    
    plot([xvals(i) xvals(i+1)],currvals,'Color',kleurmapje(coloridx,:))
end

if exist('ccRot_phi','var')
    
    ylims = get(gca,'YLim');
    plot(nanmean(ccRot_phi{1})*[1 1],ylims,'k')
    set(gca,'YLim',ylims);
    
end
    
xlabel('latitude (degrees)')
ylabel('Normal Force (Pa)')

xlim([-0.5 0.5]*pi)
set(gca,'xtick',[-0.5 -0.25 0 0.25 0.5]*pi)
set(gca,'xticklabel',{'-90','-45','0','45','90'});

%% Save open figures

filetypes = {'epsc'};

fhs = get(0,'Children'); 

savedir = fullfile(pwd,datestr(now,'yymmdd'));
if ~isfolder(savedir)
    mkdir(savedir)
end


for ifig = 1:length(fhs)
    
    set(fhs(ifig),'renderer','Painters')
    
    fname = ['Particle4_' num2str(fhs(ifig).Number) '_' fhs(ifig).Name];
    if isempty(fname)
        fname = num2str(fhs(ifig).Number);
    end
    
    savefig(fhs(ifig),fullfile(savedir,fname))
    
    for jext = 1:length(filetypes)
        saveas(fhs(ifig),fullfile(savedir,fname ),filetypes{jext})
    end
    
end

%% Calculate forces by integration over area in a GUI

% First open a 2D figure
Calculate_forces_from_stressmap(r0,'polygon');

%% Functions

function[] = add_colorscalebar()

    ch = colorbar;
    chpos = get(ch,'Position');
    original_axis_pos  = get(gca,'Position');
    original_axis_pos(2) = (1-original_axis_pos(4))/2;
    set(ch,'Position',[chpos(1) 0.5-original_axis_pos(4)/4 chpos(3) original_axis_pos(4)/2]);
    set(gca,'Position',original_axis_pos);
    ch.TickLength = 0.03;

end
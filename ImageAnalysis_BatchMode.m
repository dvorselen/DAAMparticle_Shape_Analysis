% ImageAnalysis_BatchMode

% This code will allow you to analyze the deformation of multiple large fields of
% view with multiple particles or many timepoints (fixed samples)

% Run the first section (by pressing ctrl-enter (or cmnd-enter)) to provide
% all user input, the second section will then execute all analysis

%% Ask for all userinput required for the analysis

if ~exist('DL2','class')  % Check if deconvolutionlab2 can be found by matlab
    javaaddpath([matlabroot filesep 'java' filesep 'jar' filesep 'DeconvolutionLab_2.jar'])
end
if ~exist('DL2','class')
    warning(['Deconvolutionlab2 is required for deconvolution. Please follow the instructions on '...
        'http://bigwww.epfl.ch/deconvolution/deconvolutionlab2/ and put DeconvolutionLab_2 '...
        'into the java folder of Matlab. Currently, deconvolution will be impossible.']);
end

% Open UI to select the data files and output folder
[PathName,FileName] = uigetMultiFilesandFolders(fullfile(fileparts(cd)),'.tif','file'); 
OutputFolder = uigetdir(PathName{1},'Select an output folder'); 

% Read the first image to obtain image metadata
data = ReadBFImages('.tif',PathName(1),FileName(1));

% Get required information from metadata
[MPchannel,Stain_channel,Stain_channel2,NChannels,SlicesFirst,VoxelSize,isLLSMdata]...
    = Extract_Essential_Metadata_batch(data);

%if exist('Check_z_direction','file')==2
%    try
%        [IMStats] = Check_z_direction(IMStats,'IM3D',data,xtra_metadata,0);
%    catch
%         warning(['Failed to automatically detect acquistion direction of stack (up or down),'...
%             ' this is only important when using deconvolution. If you are, confirm that the '...
%             ' acquisition direction of the PSF and the data to analyze are the same.']);
%    end
%end

% Ask user if deconvolution should be performed
perform_deconvolution = questdlg(['Do you want to deconvolve the data?',...
    ' You will be asked for a PSF.'],'Deconvolution','yes','no','yes');
perform_deconvolution = strcmp(perform_deconvolution,'yes');

if perform_deconvolution
    warning(['Since PSFs are assymetric, it is important that all images - and the '...
        'PSF - have all been captured in the same direction (top-to-bottom or bottom-to-top. '...
        'If necessary, you can flip individual images using:' newline 'MPStats(i).IM3D = MPStats(i).IM3D(:,:,end:-1:1);'...
        newline 'where i is the number of the image that you want to flip']); 
   
    % To add: Ask user if they want to use one PSF for all channels or a
    % separate PSF for each

    % Ask user for loading a PSF file
    [PSFFileName,PSFPathName] = uigetfile(fullfile(fileparts(cd),'*.*'),'Select your  PSFfile');
    
    % Ask user for the size (in um)
    PSFdims = inputdlg({'Full PSF x and y (um)',' Full z (um)','Absolute PixelSize x and y (um)',...
        'Absolute PixelSize z (um)','relative PixelSize x and y','relative PixelSize Z'},...
        'Enter the absolute PSF dimensions, or absolute/relative pixelsize',...
        [1 50; 1 50;1 50; 1 50;1 50; 1 50],{ 'NaN', 'NaN', 'NaN', 'NaN', '1', 'NaN'}); 
    PSFdims = str2double(PSFdims);
end

% Ask user if a z-correction needs to be performed
zcorrfactor = str2double(inputdlg(['Refractive index mismatch between immersion fluid and sample ',...
        'can cause apparent elongation along the z-direction' newline ...
        '(see methods section in https://www.biorxiv.org/content/early/2018/10/09/431221)'...
        newline newline 'Please enter the measured/estimated correction factor'],'Z-correction factor',[1 120],{'1'}));

% Ask user what to do with negative pixel values:
replaceorkeep = questdlg(['Negative valued pixels could be present. This is expected ',...
    'for deconvolved data, but otherwise likely indicate corrupted pixel values. ' newline newline ,...
    'Do you want to: ' newline ' -replace them (''replace''; recommended for non-deconvolved data)',...
    ', or ' newline ' -keep them (''keep'', recommended for deconvolved data)'], 'Negative valued pixels',...
    'keep','replace','keep');

% Ask user for shape reconstruction input
userinput = inputdlg({'Desired (max) spacing between the points (in um)?',...
    'Do you want to use a regular spaced grid (reg) or equidistant points (equi)'},...
    'Gridding options',[1 60],{'0.25','equi'});

% Ask the user for a smoothing pixelsize
smoothpixelsize = str2double(inputdlg(['How much smoothing do you want to apply (in pixels)?',...
    ' If you don''t know, you can run ImageAnalysis for a single image to find '...
    ' the appropriate amount of smoothing'],'Smoothing Standard Deviation',[1 60],{num2str(1)}));

% Ask user for stain analysis options
stainoptions = str2double(inputdlg({'Search margin for stain from known particle edge location (in um)?',...
            'Stain smoothing standard deviation','Do you want to analyze the stain as a Gaussian?'},...
            'Stain analysis options',[1 70],{'1.5','1','0'}));

% Choose to use the maximum intensity or integrated intensity. 
userinputstain = inputdlg({'Use maximum intensity (max) or integrated (int) intensity signal for fluorescent signals?',...
            'Does (the first) stain indicate contact (fluorescent labeled cell, 1) or lack of contact (free boundary stain, 0)',...
            'Use both stains (if applicable) for determination of the contact area ("both"), or the first alone ("first")'},...
            'Stain analysis options',[1 70],{'max','0','first'});
use_integrated_intensity = strcmp(userinputstain(1),'int')   ;
stain_indicates_contact  = str2double(userinputstain(2))     ;
use_both_stains          = ~strcmp(userinputstain(3),'first');

%% Read the images a few at a time, and perform shape analysis in a loop

Batch_Analysis_Loop

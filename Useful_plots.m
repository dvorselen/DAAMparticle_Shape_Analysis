% Useful_plots.m

% This code allows you to make a couple of plots that I have found
% particularly useful. They are meant to be examples for how the deformed
% particle shape can be visualized. 

% Run the appropriate cell (after analyzing your data using ImageAnaalysis
% or ImageAnalasys_SingleParticleMovie), or by reloading a previously saved
% "MPStats" structure 

%% Plot LabelMatrix with Identifiers

if ~exist('IMStats','var')
    error('This only works if you started with an image with many particles, imported using ImageAnalysis')
end
    
if ~exist('iIM','var'); iIM = 1; end
iIM = Select_particle_for_plot(IMStats,iIM);

figure('Name','LabelMatrix (Maximum intensity Projection)')
imagesc(max(IMStats(iIM).IMlm,[],3))
colormap([0.92 0.92 0.92; repmat(brewermap(8,'dark2'),ceil(single(max(IMStats(iIM).IMlm(:)))/8),1)])
axis equal
axis off
hold on

if exist('MPStats','var')
    whichMPs = find([MPStats.IMnumber] == iIM);

    for iMP = whichMPs
        text(MPStats(iMP).Centroid(1),MPStats(iMP).Centroid(2),num2str(iMP),'Color','k','FontSize',16)
    end
end

%% Plot edgecoordinates particles

if ~exist('iMP','var'); iMP = 1; end
iMP = Select_particle_for_plot(MPStats,iMP);

% Check where in the processing pipeline we are 
[edgecoor,centroidcoor,edgecoor_sph] = check_processing_status(MPStats(iMP));
edgecoor(isnan(edgecoor(:,1)),:) = [];

% plot points on surface (cartesian)
figure 
h = plot3(edgecoor(:,1),edgecoor(:,2),edgecoor(:,3),'.k');
hold on
plot3(centroidcoor(1),centroidcoor(2),centroidcoor(3),'or')
axis equal
if isfield(MPStats,'CupBase')
   view(0,0)
   rotate(h,[0 0 -1],MPStats(iMP).CupBase(1)/pi*180)
   rotate(h,[1 0 0],MPStats(iMP).CupBase(2)/pi*180)
end
xlabel('X (\mum)'); ylabel('Y (\mum)'); zlabel('Z (\mum)') 

% 2D plot (Theta, Phi, R)
figure
plot3(edgecoor_sph(:,1)+pi,edgecoor_sph(:,2),edgecoor_sph(:,3),'.k')
xlabel('longitude (rad)'); ylabel('latitude (rad)'); zlabel('R (\mum)') 
xlim([-.1 2*pi+.1])

%% Plot triangulated surface of particles

if ~exist('iMP','var'); iMP = 1; end
iMP = Select_particle_for_plot(MPStats,iMP);

dlgopts.Interpreter = 'tex';
if ~exist('MW','var');    MW = 1;     end
plotopts = inputdlg({'Area of the circle used for smoothing (in \mum^2)'...
    ' apply a moving average filter (mean) or moving median filter (median)?','Mollweide Projection?'},...
    'Choose smoothing and plotting options',[1 80],{'0','mean',num2str(MW)},dlgopts);
MW = str2double(plotopts{3});

[~,centroidcoor,edgecoor_sph,TRI_nosph] = check_processing_status(MPStats(iMP));
[ThetaPhiR] = Smooth_spherical_mesh(edgecoor_sph,sqrt(str2double(plotopts{1}).^2/pi),'arc',1,plotopts{2});
R = ThetaPhiR(:,3);
[X,Y,Z] = sph2cart(ThetaPhiR(:,1),ThetaPhiR(:,2),ThetaPhiR(:,3));

% Plot the sphere
figure('Name','Distance to Centroid Sphere')
Make_3D_plot(MPStats(iMP).TRI_Connectivity_SPHbound,[X,Y,Z],R);
colormap(brewermap([],'RdBu'))
caxis([prctile(R,1) prctile(R,99.5)])

% And the 2D surface
figure('Name','Distance to Centroid MAP')
Make_2D_plot(TRI_nosph,ThetaPhiR,R,MW);
colormap(brewermap([],'RdBu'))
caxis([prctile(R,1) prctile(R,99.5)])
add_colorscalebar()

%% Plot curvature on triangulated surface

if ~exist('iMP','var'); iMP = 1; end
iMP = Select_particle_for_plot(MPStats,iMP);

dlgopts.Interpreter = 'tex';
if ~exist('MW','var');    MW = 1;     end
plotopts = inputdlg({'Area of the circle used for smoothing (in \mum^2)'...
    ' apply a moving average filter (mean) or moving median filter (median)?',...
    'Only smooth for curvature calculation (1) or also for shape visualization (0)?','Mollweide Projection?'},...
    'Choose smoothing and plotting options',[1 80],{'0.5','mean','1',num2str(MW)},dlgopts);
MW = str2double(plotopts{4});

% Check processing status
[edgecoor,~,edgecoor_sph,TRI_nosph] = check_processing_status(MPStats(iMP));
[ThetaPhiR] = Smooth_spherical_mesh(edgecoor_sph,sqrt(str2double(plotopts{1}).^2/pi),'arc',1,plotopts{2});
[X,Y,Z] = sph2cart(ThetaPhiR(:,1),ThetaPhiR(:,2),ThetaPhiR(:,3));

FVsmooth.faces    = MPStats(iMP).TRI_Connectivity_SPHbound;
FVsmooth.vertices = [X,Y,Z];

pc = GetCurvatures(FVsmooth,0);
gc = pc(1,:).*pc(2,:);
mc = mean(pc);

if logical(str2double(plotopts{3}))
    vertices = edgecoor;
else
    vertices = [X,Y,Z];
end

% Plot mean curvature on the sphere
figure('Name','Mean Curvature deviation Sphere')
mccentred = mc - median(mc);
Make_3D_plot(MPStats(iMP).TRI_Connectivity_SPHbound,vertices,mccentred);
colormap(greenmagenta)
mclim = max(abs([prctile(mccentred,1) prctile(mccentred,99)]));
caxis([-mclim mclim])

% Plot mean curvature world map
figure('Name','Mean Curvature deviation MAP')
Make_2D_plot(TRI_nosph,edgecoor_sph,mccentred,MW);
colormap(greenmagenta)
mclim = max(abs([prctile(mccentred,1) prctile(mccentred,99)]));
caxis([-mclim mclim])
if isfield(MPStats,'CupBase')
    hold on
    plot3(-.5*pi,0,MPStats(iMP).CupBase(3)+1,'pk','MarkerFaceColor','none')
end
add_colorscalebar()

% Plot gaussian curvature on the sphere
figure('Name','Gaussian Curvature Sphere')
gccentred = gc;
Make_3D_plot(MPStats(iMP).TRI_Connectivity_SPHbound,vertices,gccentred);
colormap(turquoisebrown)
gclim = max(abs([prctile(gccentred,1) prctile(gccentred,99)]));
caxis([-gclim gclim])

% Plot gaussian curvature world map
figure('Name','Gaussian Curvature MAP')
Make_2D_plot(TRI_nosph,edgecoor_sph,gccentred,MW);
colormap(turquoisebrown)
gclim = max(abs([prctile(gccentred,1) prctile(gccentred,99)]));
caxis([-gclim gclim])
if isfield(MPStats,'CupBase')
    hold on
    plot3(-.5*pi,0,MPStats(iMP).CupBase(3)+1,'pk','MarkerFaceColor','none')
end
add_colorscalebar()

%% Plot secondary stain on particle 

if ~exist('iMP','var'); iMP = 1; end
iMP = Select_particle_for_plot(MPStats,iMP);

[edgecoor,~,edgecoor_sph,TRI_nosph] = check_processing_status(MPStats(iMP));

stainname = select_stain(MPStats);

% There a couple of options: 
% a radial maximumum intensity plot, an integrated intensity plot; linear or logarithmic scale.
if ~exist('MW','var');    MW = 1;     end
plotopts = inputdlg({'Plot log intensity (log) or linear intensity (lin)?',...
            'Plot integrated intensity (sum) or maximum intensity (max)?',...
            'Gray, Green or Magenta','gamma-correction?','Mollweide Projection?'},'Choose plot options',[1 70],{'lin','max','Gray','1',num2str(MW)});
MW = str2double(plotopts{5});
        
stainmap = ones(256,3)*.05;
% Create a green or magenta colormap 
if ~strcmpi(plotopts{3},'magenta')
    stainmap(:,2) = linspace(.05,.9,256);
end
if ~strcmpi(plotopts{3},'green')
    stainmap(:,1) = linspace(.05,.9,256);
    stainmap(:,3) = linspace(.05,.9,256);
end

% Process plot options
if strcmp(plotopts{2},'max')    
    stainint = MPStats(iMP).([stainname '_int']);
    fignameappend = ' Max Intensity';
else    
    stainint = MPStats(iMP).([stainname '_int_integrated']);
    fignameappend = ' Integrated Intensity';
end           
if strcmp(plotopts{1},'lin')    
    fignameappend = [fignameappend ' Linear'];  
else    
    fignameappend = [fignameappend ' Log']; 
    stainint = real(log(stainint)); 
    %stainint(stainint <= 0 ) = 1;
end

%perform gamma correction
if str2double(plotopts{4})~=1
    stainint = real(stainint.^str2double(plotopts{4}));
end

% plot 3D representation
figure('Name',['Stain Sphere' stainname fignameappend])
Make_3D_plot(MPStats(iMP).TRI_Connectivity_SPHbound,edgecoor,stainint);
colormap(stainmap)
caxis([prctile(stainint,1) prctile(stainint,99)])

% plot 2D surface
figure('Name',['Stain Map' stainname fignameappend])
Make_2D_plot(TRI_nosph,edgecoor_sph,stainint,MW);
colormap(stainmap)
caxis([prctile(stainint,1) prctile(stainint,99)])
%if isfield(MPStats,'CupBase')
%    hold on
%    plot3(-.5*pi,0,MPStats(iMP).CupBase(3)+1,'or','MarkerFaceColor','r')
%end
add_colorscalebar()

%% Plot two stains on a particle

if ~exist('iMP','var'); iMP = 1; end
iMP = Select_particle_for_plot(MPStats,iMP);

[edgecoor,~,edgecoor_sph,TRI_nosph] = check_processing_status(MPStats(iMP));

Fnames = fieldnames(MPStats);
channelstrings  = regexp(Fnames,'_ch\d','match');
channelstrings  = unique([channelstrings{:}]);
if length(channelstrings)>=2
    channelopts     = ["_ch1","_ch2","_ch3","_ch4"];
    channelspresent = ismember(channelopts,channelstrings);
    channelspresent = strjoin(channelopts(channelspresent),',  ');
    select_channel = inputdlg(['Which two signals do you want to visualize? '...
        'Options are:' newline 'IMstain,  ' channelspresent{:}],...
        'Choose a stain',[1 70],{['IMstain, ' channelstrings{1}]});
    channelnumbers = string(regexp(select_channel{:},'\d','match'));
    if contains(select_channel,'IMstain')
        stainname1 = 'stain';
        stainname2 = ['stain_ch' channelnumbers{:}];
    else
        stainname1 = ['stain_ch' channelnumbers{1}];
        stainname2 = ['stain_ch' channelnumbers{2}];
    end
elseif length(channelstrings) == 1
    stainname1 = 'stain';
    stainname2 = ['stain' channelstrings{1}];
else
    error('At least 2 stains should be analyzed!')
end

% There a couple of options: 
% a radial maximumum intensity plot, an integrated intensity plot; linear or logarithmic scale.
if ~exist('MW','var');    MW = 1;     end
plotopts = inputdlg({'Plot log intensity (log) or linear intensity (lin)?',...
            'Plot integrated intensity (sum) or maximum intensity (max)?',...
            'Plot added (add) or averaged (ave) signal?','Invert colors?','Mollweide Projection?'},...
            'Choose plot options',[1 70],{'lin','max','add','0',num2str(MW)});
MW = str2double(plotopts{5});

% Process plot options
if strcmp(plotopts{2},'max')    
    stainint1  = MPStats(iMP).([stainname1  '_int']);
    stainint2 = MPStats(iMP).([stainname2 '_int']);
    fignameappend = ' Max Intensity';
else    
    stainint1 = MPStats(iMP).([stainname1 '_int_integrated']);
    stainint2 = MPStats(iMP).([stainname2 '_int_integrated']);
    fignameappend = ' Integrated Intensity';
end    
if logical(str2double(plotopts{4}))
    stainint1rep = stainint1;
    stainint1 = stainint2;
    stainint2 = stainint1rep;
end
if strcmp(plotopts{1},'lin')    
    fignameappend = [fignameappend ' Linear'];  
else    
    fignameappend = [fignameappend ' Log']; 
    stainint1 = log(stainint1);   stainint2 = log(stainint2);
end

% Construct color maps
stainint1_range = prctile(stainint1,[1 99.8]);
stainint2_range = prctile(stainint2,[1 99.8]);
normstain1 = (stainint1-stainint1_range(1))/(diff(stainint1_range));
normstain2 = (stainint2-stainint2_range(1))/(diff(stainint2_range));
if strcmp(plotopts{3},'ratio')

    ratio = real(log2((stainint1/(mean(stainint1)))./(stainint2/(mean(stainint2)))));
    ratio(stainint1<prctile(stainint1,80) & stainint2<prctile(stainint2,80)) = 0;
    [~,~,stainint] = Smooth_spherical_mesh(edgecoor_sph,sqrt(1.^2/pi),'arc',1,'mean',[],ratio);
    
else
    
    rgb1 = normstain1 * [0.05 0.95 0.05];
    rgb2 = normstain2 * [0.95 0.05 0.95];
    if strcmp(plotopts{3},'add')
        stainint = min(rgb1+rgb2,1);
    else
        stainint = min((rgb1+rgb2)/2,1);
    end
    
end
if ~strcmp(plotopts{3},'ratio')
    stainint = max(stainint,0   );
end

% plot 3D representation
figure('Name',['Stain Sphere' stainname1 fignameappend])
Make_3D_plot(MPStats(iMP).TRI_Connectivity_SPHbound,edgecoor,stainint);
if strcmp(plotopts{3},'ratio')
    colormap(Cmasher_iceburn)
    caxis(max(abs(caxis))*[-1 1])
end

% plot 2D surface
figure('Name',['Stain Map' stainname1 fignameappend])
Make_2D_plot(TRI_nosph,edgecoor_sph,stainint,MW);
if strcmp(plotopts{3},'ratio')
    colormap(Cmasher_iceburn)
    caxis(max(abs(caxis))*[-1 1])
    cbh = colorbar;
    cbh.TickLabels = cellfun(@(x) num2str(round(2^str2double(x),3,'significant')), cbh.TickLabels, 'UniformOutput', false);
end

%if isfield(MPStats,'CupBase')
%    hold on
%    plot3(-.5*pi,0,MPStats(iMP).CupBase(3)+1,'or','MarkerFaceColor','r')
%end

%% Plot surface mask and masked deformation map

if ~exist('iMP','var'); iMP = 1; end
iMP = Select_particle_for_plot(MPStats,iMP);
[edgecoor,~,edgecoor_sph,TRI_nosph] = check_processing_status(MPStats(iMP));

% Ask user if the mask should be dilated
dlgopts.Interpreter = 'tex';
dil = str2double(inputdlg('Desired dilation of the mask (in \mum)?','Dilate Mask?',[1 50],{'0'},dlgopts));

% Create color map (green, linear)
stainmap = ones(64,3)*.05;
stainmap(:,2) = linspace(.05,.9,64);

if ~dil 
    contmask = single(MPStats(iMP).isincontact);
else
    Theta    = MPStats(iMP).stain_coor_sph(:,1);
    Phi      = MPStats(iMP).stain_coor_sph(:,2);
    gridsize = MPStats(iMP).edgecoor_gridsize;
    pgcdist  = bwdist_spherical(Theta,Phi,MPStats(iMP).actcont_mask);
    contmask = pgcdist*(3/4/pi*MPStats(iMP).Volume)^(1/3)<dil;
    GI = griddedInterpolant({[unique(Theta)' -Theta(1)],Phi(1:gridsize(1):end)},double([contmask; contmask(1,:)]));
    iscovered = logical(round(GI(MPStats(iMP).edgecoor_sph_wcatorigin(:,1),MPStats(iMP).edgecoor_sph_wcatorigin(:,2))));  
    iscovered(iscovered<0) = 0;
    contmask = single(iscovered);
end

% plot 3D representation mask
figure('Name','Mask 3D')
ph = Make_3D_plot(MPStats(iMP).TRI_Connectivity_SPHbound,edgecoor,contmask);
caxis([-0.05 1.05])
colormap(gray)
set(ph,'EdgeColor',[.2 .2 .2])

% Plot the sphere
figure('Name','Masked 3D surface')
R = edgecoor_sph(:,3);
if MPStats(iMP).stain_indicates_contact
    R(logical(~contmask)) = NaN;
else
    R(logical(contmask)) = NaN;
end
hold on
ph2 = Make_3D_plot(MPStats(iMP).TRI_Connectivity_SPHbound,edgecoor,R);
set(ph2,'EdgeColor',[.2 .2 .2])
colormap([0,0,0; brewermap(128,'RdBu')])
meanR = nanmean(R);
scaleamp = max(abs([nanmin(R)-meanR nanmax(R)-meanR]));
caxis([(meanR-scaleamp)*32/33 meanR+scaleamp]); 

% plot 2D surface mask
figure('Name','Mask 2D')
Make_2D_plot(TRI_nosph,edgecoor_sph,contmask,1);
colormap(gray)
caxis([-0.05 1.05])

% And the 2D surface
figure('Name','Masked 2D surface')
Make_2D_plot(TRI_nosph,edgecoor_sph,R,1);
colormap(brewermap(128,'RdBu'))
meanR = nanmean(R);
scaleamp = max(abs([nanmin(R)-meanR nanmax(R)-meanR]));
caxis([meanR-scaleamp meanR+scaleamp]);    
 
%% Add Centroid of secondary stain (3D sphere)

% First create any 3D plot and select the appropriate axes
achildren = get(gca,'children');
patchchildren = findobj(achildren,'type','patch');
set(patchchildren,'FaceAlpha',.75)
hold on
disttopoints = squareform(pdist([[-0.5*pi; edgecoor_sph(:,1)],[0; edgecoor_sph(:,2)]]));
[~,closestpoint] = min(disttopoints(1,2:end));
plot3(edgecoor(closestpoint,1),edgecoor(closestpoint,2),edgecoor(closestpoint,3),'or','MarkerFaceColor','r','MarkerSize',20)
hold off

%% Add Snake (2D map)!

% Determine if 2D map is a mollweid projection
try 
    xdat = get(get(gca,'children'),'Xdata');
    if iscell(xdat)
        lengthdat = cellfun(@length,xdat);
        xdat = xdat{lengthdat == max(lengthdat)};
        if all(size(xdat)>1)
            [~,minloc] = min(size(xdat));
            xdat = mean(xdat,minloc);
        end        
    end
    ncounts = histcounts(xdat,10);
    if mean(ncounts([1 end]))<(0.6*max(ncounts))
        MW = 1;    
        warning('Auto detected that data is visualized as Mollweide projection')
    else
        MW = 0;
        warning('Auto detected that data is visualized as equirectangular projection')
    end
catch
    MW = str2double(inputdlg('Mollweide Projection?','Choose mask option',[1 70],{'1'}));
end

baseLOC = questdlg('Where would you like to visualize the centroid of the contact area (cup base)?',...
    'Cup base location','South Pole','West','Center','South Pole');
if strcmp(baseLOC,'South Pole')
    base = [0 .5*pi];
elseif strcmp(baseLOC,'Center')
    base = [0 0];
else
    base = [.5*pi 0];
end

% Ask user if the mask should be dilated
dlgopts.Interpreter = 'tex';
if ~exist('dil','var');     dil = 0;        end
dil = str2double(inputdlg('Desired dilation of the mask (in \mum)?','Dilate Mask?',[1 50],{num2str(dil)},dlgopts));

if ~exist('iMP','var'); iMP = 1; end
iMP = Select_particle_for_plot(MPStats,iMP);

gridsize = MPStats(iMP).edgecoor_gridsize;
if dil ~= 0
    actcontmaxdiff = max(real(MPStats(iMP).actcont_phi(:)))-min(real(MPStats(iMP).actcont_phi(:)));
    Phi      = reshape(repmat(linspace(-.5*pi,.5*pi,gridsize(2))',1,gridsize(1))',[],1);   
    Theta    = repmat(linspace(-pi,pi-2*pi/gridsize(1),gridsize(1))',gridsize(2),1);
    pgcdist  = bwdist_spherical(Theta,Phi,MPStats(iMP).actcont_mask)*(3/4/pi*MPStats(iMP).Volume)^(1/3);
    pgcdistmaxdiff  = max(pgcdist(:))-min(pgcdist(:));
    corrdil = dil/pgcdistmaxdiff*actcontmaxdiff;
else 
    corrdil = 0;
end  
cc = contourc(linspace(-pi,pi*(1-2/gridsize(1)),gridsize(1)),...
    linspace(-.5*pi,.5*pi,gridsize(2)),...
    double(real(MPStats(iMP).actcont_phi))'/gridsize(1)*MPStats(iMP).Equiv_Diameter_um*2*pi,...
    [corrdil corrdil]);
cc(:,1) =[];
ccoutliers = find(abs(cc(2,:)) > 0.5*pi);
cc(:,ccoutliers) = [];

[ccRot_theta,ccRot_phi] = Align_spheres_by_Rotation({cc'},MPStats(iMP).CupBase([1 2]));
[ccRot_theta,ccRot_phi] = Align_spheres_by_Rotation({[ccRot_theta{:},ccRot_phi{:}]},base);

% deal with discontinuities: find segments and their direction
discc = find(abs(diff(ccRot_theta{1}(:,1)))>(0.5*(pi)));
if length(discc) > 1
    Nsegments = length(discc)+1;
    Segs   = cell(Nsegments,1);
    minseg = zeros(1,Nsegments);
    segdir = ones(1,Nsegments) ;
    for i = 1:Nsegments
        if i == 1
            curridxs = 1:discc(i);
        elseif i ~= Nsegments
            curridxs = discc(i-1)+1:discc(i);
        else
            curridxs = discc(i-1)+1:length(cc);
        end
    
        Segs{i} = curridxs;
    
        minseg(i) = min(ccRot_theta{1}(curridxs));
        if ccRot_theta{1}(curridxs(1)) > ccRot_theta{1}(curridxs(end))
        segdir(i) = -1;
        end
    end

    ccRot_theta{2} = [];
    ccRot_phi{2}   = [];

    % reconstruct line piece by piece
    [~,sortidx] = sort(minseg);
    Segs = Segs(sortidx);
    segdir = segdir(sortidx);
    for i = 1:Nsegments
        if segdir(i) == 1
            ccRot_theta{2} = [ccRot_theta{2}; ccRot_theta{1}(Segs{i})];
            ccRot_phi{2}   = [ccRot_phi{2}; ccRot_phi{1}(Segs{i})];
        else
            ccRot_theta{2} = [ccRot_theta{2}; flipud(ccRot_theta{1}(Segs{i}))];
            ccRot_phi{2}   = [ccRot_phi{2}; flipud(ccRot_phi{1}(Segs{i}))];
        end
    end


    ccRot_theta(1) = [];
    ccRot_phi(1) = [];
end

mapdiscc = find(abs(diff(ccRot_theta{1}(:,1)))>(1.5*(pi)));
% large breaks can still be present, if the map "edge" is crossed multipletimes
if ~isempty(mapdiscc)
    for i = flipud(mapdiscc)'
        ccRot_theta{1} = [ccRot_theta{1}(1:i); NaN; ccRot_theta{1}(i+1:end)];
        ccRot_phi{1} = [ccRot_phi{1}(1:i); NaN; ccRot_phi{1}(i+1:end)];
    end
end

if ~contains(get(gcf,'Name'),'Snake')
    set(gcf,'Name',[get(gcf,'Name') ' with Snake'])
end

xlims = xlim;
ylims = ylim;
hold on

try
    zheight = max(cellfun(@(x) max(x(:)),get(get(gca,'children'),'Zdata')))+0.5;
catch
    zheight = MPStats(iMP).Equiv_Diameter_um/2+1;
end

if any(all(get(gca,'Colormap')>[0.7 0 0] & get(gca,'Colormap')<[1.1 0.2 0.2],2))
    snakecolor = 'k';
else
    snakecolor = 'r';
end

if ~MW
    plot3(ccRot_theta{:},ccRot_phi{:},ones(1,length(ccRot_theta{:}))*zheight, snakecolor,'LineWidth',1);
else
    [Xmw,Ymw] = MollweidProj(ccRot_theta{:},ccRot_phi{:});
    plot3(Xmw,Ymw,ones(1,length(Xmw))*zheight, snakecolor,'LineWidth',1);
end
xlim(xlims);
ylim(ylims);
hold off

%% Turn 3D plot sideways

cfh = gcf;
ch = get(cfh,'children');
nh = figure;
for i = 1:length(ch)
    copyobj(ch,nh,'legacy')
end
set(nh,'colormap',get(cfh,'colormap'))
set(nh,'Name',get(cfh,'Name'))

view(270,0)
if ~contains(get(gcf,'Name'),'Side')
    set(gcf,'Name',[get(gcf,'Name') ' from Side'])
end

%% Turn 3D plot to the Front

cfh = gcf;
ch = get(cfh,'children');
nh = figure;
for i = 1:length(ch)
    copyobj(ch,nh,'legacy')
end
set(nh,'colormap',get(cfh,'colormap'))
set(nh,'Name',get(cfh,'Name'))

view(180,0)
if ~contains(get(gcf,'Name'),'Front')
    set(gcf,'Name',[get(gcf,'Name') ' from Front'])
end

%% Save open figures

Save_Open_Figures

%% Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Select particle for plot
function iMP = Select_particle_for_plot(MPStats,iMP)

    NMPs = length(MPStats);

    if NMPs > 1

        iMP = str2double(inputdlg('which particle/frame do you want to visualize?',...
            'Select particle',[1 50],{num2str(iMP)}));

        if iMP > NMPs
            error(['Particle ' num2str(iMP) ' doesn''t exist']);
        end

    else

        iMP = 1;

    end
end

% Set 3D plots options
function[ph] = Make_3D_plot(TRI,vert,color)

    if size(color,2) == 3
        R = color(:,1);
        G = color(:,2);
        B = color(:,3);
        color = [mean(R(TRI),2) mean(G(TRI),2) mean(B(TRI),2)];
    else
        color = mean(color(TRI),2);
    end
    ph = patch('faces',TRI,'vertices',vert,'facevertexcdata',color,'facecolor','flat','edgecolor','none');
    view(3)
    axis equal
    zoom(1.25)
    axis off
    set(gca,'CameraViewAngleMode','Manual','Clipping','off')
    view(0,0)

end

% Set 2D plot options
function[ph] = Make_2D_plot(TRI,vert,color,MW)

    if nargin < 4;        MW = 0;       end

    if MW
        [vert(:,1),vert(:,2),TRI] = MollweidProj(vert(:,1),vert(:,2));
    end
    
    if size(color,2) == 3
        R = color(:,1);
        G = color(:,2);
        B = color(:,3);
        color = [mean(R(TRI),2) mean(G(TRI),2) mean(B(TRI),2)];
    else
        color = mean(color(TRI),2);
    end

    ph = patch('faces',TRI,'vertices',vert,'facevertexcdata',color,'facecolor','flat','edgecolor','none');
    view(3)
    axis off
    axis equal
    view(0,90);
    set(gca,'Clipping','off')

end

% Add colorbar to 2D plot, while keeping the same axis position
function[] = add_colorscalebar()

    ch = colorbar;
    chpos = get(ch,'Position');
    original_axis_pos  = get(gca,'Position');
    original_axis_pos(2) = (1-original_axis_pos(4))/2;
    set(ch,'Position',[chpos(1) 0.5-original_axis_pos(4)/4 chpos(3) original_axis_pos(4)/2]);
    set(gca,'Position',original_axis_pos);
    ch.TickLength = 0.03;

end

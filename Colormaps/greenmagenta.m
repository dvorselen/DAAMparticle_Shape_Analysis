function c = greenmagenta(m)
% Diverging colors: Shades of green to magenta color map

if nargin < 1, m = size(get(gcf,'colormap'),1); end

if (mod(m,2) == 0)
    halfm = m/2;
    r = [linspace(0.25,0.975,halfm) fliplr(linspace(0,0.975,halfm))   ]';
    g = [linspace(0,0.975,halfm)    fliplr(linspace(0.25,0.975,halfm))]';
    b = [linspace(0.3,0.975,halfm)  fliplr(linspace(0.1,0.975,halfm)) ]';
else
    halfm = floor(m*0.5);
    r = [linspace(0.25,0.975,halfm-1) fliplr(linspace(0,0.975,halfm))   ]';
    g = [linspace(0,0.975,halfm-1)    fliplr(linspace(0.25,0.975,halfm))]';
    b = [linspace(0.3,0.975,halfm-1)  fliplr(linspace(0.1,0.975,halfm)) ]';
end

c = [r g b]; 


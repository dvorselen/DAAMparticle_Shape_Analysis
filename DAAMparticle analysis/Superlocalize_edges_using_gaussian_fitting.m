function[MPStats,Residuals_Problems_excluded] = ...
    Superlocalize_edges_using_gaussian_fitting(MPStats,point_to_point_dist,gridding,varargin)

% Check function input and set default parameters
DefStruct = struct('usestablemode',0,'usePPM',1,'verbose',1,'DirectDerivative',0,...
    'smooth',0,'RadialBounds',[0.25 0.75],'RelMinPeakSize',90);
Args = parseArgs(varargin,DefStruct,[]);
usestablemode    = Args.usestablemode;
usedirderivative = Args.DirectDerivative;

% Check user input
testuserinput = strcmp(gridding,{'equi','reg'});
if any(testuserinput)
    [MPStats.Gridding] = deal(gridding);
    reggrid = testuserinput(2);
else
    error(['Choose ''reg'' for a regular spaced grid or ''equi'' for equidistant point, you entered: ''' gridding ''''])
end 

NMPs = length(MPStats);

%set minimum peak size
relminpeaksize = str2double(Args.RelMinPeakSize);
minpeaksize = zeros(1,NMPs);
for i = 1:NMPs
    minpeaksize(i) = double(2*sqrt(prctile(MPStats(i).IM3D(:),relminpeaksize)));
end

% Start a parallel loop and show a progress bar
p = gcp; %#ok<NASGU>

if Args.usePPM
    try
        jScreenSize = java.awt.Toolkit.getDefaultToolkit.getScreenSize;
        scrwidth = jScreenSize.getWidth;   scrheight = jScreenSize.getHeight;     
        ppm = ParforProgMon('Performing many Gaussian fits, this may take a while: ',NMPs,[],scrwidth/4,scrheight/16);
        useppm = 1;
    catch
        ppm = [];
        warning('Problem with loading progress monitor, this could indicate that all java files need to be cleared')
        useppm = 0;
    end
else
    ppm = [];
    useppm = 0;
end

% For troubleshooting
Residuals_Problems_excluded = cell(NMPs,4);
[MPStats.edgecoor_cart] = deal([]);
[MPStats.edgecoor_sph] = deal([]);
if reggrid;     [MPStats.edgecoor_gridsize] = deal([]);     end

% Reduce broadcast variables (input)
centroid     = {MPStats.Centroidzm}   ;
diam         = {MPStats.EquivDiameter};
im           = {MPStats.IM3D         };
if ~usedirderivative
    edges        = {MPStats.IMedges  };
else
    edges        = cell(1,NMPs)       ;
end
voxsize      = [vertcat(MPStats.PixelSizeXY), vertcat(MPStats.PixelSizeZ)];
llsmmask     = {MPStats.LLSMMask     };
verbose      = Args.verbose           ;
smoothpxsize = Args.smooth            ;
radialbounds = Args.RadialBounds      ;
% (output)
edgecoor_cart = cell(1,NMPs);
edgecoor_sph  = cell(1,NMPs);
edgecoor_grid = cell(1,NMPs);

% Now try to superresolve the edge by finding/fitting the maximum in
% outward pointing lines from the particle Centroid

parfor iMP = 1:NMPs
        
    [currMPbounds,currRes,currProblems,GridSize] = Super_resolve_edge_3D(centroid{iMP},diam{iMP},...
        edges{iMP},voxsize(iMP,:),'regulargridding',reggrid,'gridspacing',point_to_point_dist,...
        'MinPeakSize',minpeaksize(iMP),'mask',llsmmask{iMP},'originalImage',im{iMP},'RadialBounds',radialbounds,...
        'StableMode',usestablemode,'Directderivative',usedirderivative,'smooth',smoothpxsize);      
    
    if reggrid
        
        % In case we want a regular grid, we fill empty values using interpolation
        missing = isnan(currMPbounds(:,1));
        InterPolF = scatteredInterpolant(currMPbounds(~missing,4),currMPbounds(~missing,5),currMPbounds(~missing,6));
        currMPbounds(missing,6) = InterPolF(currMPbounds(missing,4),currMPbounds(missing,5));
        [xfill,yfill,zfill] = sph2cart(currMPbounds(missing,4),currMPbounds(missing,5),currMPbounds(missing,6));
        currMPbounds(missing,1:3) = [xfill, yfill, zfill];
        
        % We also save the GridSize
        edgecoor_grid{iMP} = GridSize; 
        
    else
        
        missing = isnan(currMPbounds(:,1));
        currMPbounds = currMPbounds(~missing,:);
        
    end
    
    % Store the coordinates in cell arrays
    edgecoor_cart{iMP} = currMPbounds(:,1:3);
    edgecoor_sph{iMP}  = currMPbounds(:,4:6);
       
    % Useful for trouble shooting:
    Residuals_Problems_excluded(iMP,:) = {currRes,currProblems,missing,currMPbounds};
    % Warn the user if problems were encountered
    Nproblems = sum(~cellfun(@isempty,currProblems));
    Nmissing  = sum(missing);
    if verbose && Nproblems + Nmissing ~= 0 
        warning([num2str(Nproblems) ' problem(s) were encountered, and ' num2str(Nmissing)...
            ' point(s) is/are missing for particle ' num2str(iMP)])
    end
    
    % Update the progress bar
    if useppm
        try
            ppm.increment();    %#ok<PFBNS>
        catch
        end
    end    
end

[MPStats.edgecoor_cart    ] = edgecoor_cart{:};
[MPStats.edgecoor_sph     ] = edgecoor_sph{:};
[MPStats.edgecoor_gridsize] = edgecoor_grid{:}; 
[MPStats.IM3D] = convert_structfield({MPStats.IM3D},'single');
MPStats = orderfields(MPStats);

% Close progress bar
if useppm == 1
    if exist('ppm','var')      
        ppm.delete
    end
end
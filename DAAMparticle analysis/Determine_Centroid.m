function [theta_base,phi_base,R_centr,theta_centr,phi_centr] = Determine_Centroid(ThetaPhi, Weights)

% Check the number of particles, and make sure the weights (secondary antibody signal) is in matrix format
if ~iscell(Weights)
    Nparticles = 1;
    % Make sure that the surface weighted centroid will be weighted similarly to the weighted centroid
    Weights = Weights/sum(Weights(:))*length(Weights); 
else
    Nparticles = length(Weights);
    Weights = cellfun(@(x) x./sum(x(:))*length(x(:)),Weights,'UniformOutput',false);
    Weights = cell2mat(reshape(Weights,Nparticles,1));  % Make sure Weights is a nx1 
end

% Make sure that the coordinates of the points are in matrix format
if iscell(ThetaPhi)    
    NPoints  = cellfun(@length,ThetaPhi);
    ThetaPhi = cell2mat(reshape(ThetaPhi,length(ThetaPhi),1)); % Make sure Weights is a nx1 
else   
    NPoints = length(ThetaPhi);
    ThetaPhi = repmat(ThetaPhi,Nparticles,1);      
end

% Calculate XYZ of each point on a unit sphere
[X,Y,Z] = sph2cart(ThetaPhi(:,1),ThetaPhi(:,2),ones(length(ThetaPhi),1));
Xweighted = X.* Weights;    Yweighted = Y.* Weights;      Zweighted = Z.* Weights;

% If every particle has the same number of points, we can do the calculations in matrix format
if all(NPoints == NPoints(1))   
    % Multiply each point by its weight
    X = reshape(X,NPoints(1),Nparticles);
    Y = reshape(Y,NPoints(1),Nparticles);
    Z = reshape(Z,NPoints(1),Nparticles);
    Xweighted = reshape(Xweighted,NPoints(1),Nparticles);
    Yweighted = reshape(Yweighted,NPoints(1),Nparticles);
    Zweighted = reshape(Zweighted,NPoints(1),Nparticles);
    % Calculate shift between mean of all the points and the surface weighted centroid 
    xyzshift  = [mean(Xweighted)'-mean(X)',mean(Yweighted)'-mean(Y)',mean(Zweighted)'-mean(Z)'];
    if all(xyzshift == 0) || all(Weights == Weights(1)) % This will guarantee that things still work if only the masked data points are provided
        xyzshift = [mean(Xweighted)',mean(Yweighted)',mean(Zweighted)'];
    end
        
else   
    % If particles have various number of points, the calculations are done in a loop/cells
    meanxyz = cellfun(@mean,mat2cell([X Y Z Xweighted Yweighted Zweighted],NPoints,[1 1 1 1 1 1]));
    xyzshift = meanxyz(:,4:6)-meanxyz(:,1:3);    
end

xyznorm = xyzshift./sqrt(sum(xyzshift.^2,2)); % Avoid problems with rounding of very small numbers
[theta_centr,phi_centr,R_centr] = cart2sph(xyznorm(:,1),xyznorm(:,2),xyznorm(:,3));

theta_base = wrapToPi(theta_centr+pi);
phi_base   = -phi_centr;

    


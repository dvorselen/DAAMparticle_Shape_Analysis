% PSF_Preparation

% You will need a PSF measured on your microscope. 
warning(['Since PSFs are assymetric, it is important that all images - and the '...
    'PSF - have all been captured in the same direction (top-to-bottom or bottom-to-top. '...
    'If necessary, you can flip individual images using:' newline 'MPStats(i).IM3D = MPStats(i).IM3D(:,:,end:-1:1);'...
    newline 'where i is the number of the image that you want to flip']); 

if ~exist('PSFFileName','var')
    [PSFFileName,PSFPathName] = uigetfile(fullfile(fileparts(cd),'*.*'),'Select your  PSFfile');
elseif all(PSFFileName == 0)
    [PSFFileName,PSFPathName] = uigetfile(fullfile(fileparts(cd),'*.*'),'Select your  PSFfile');
elseif ~exist('PSF','var')
    warning(['Using PSF from file: ' PSFPathName PSFFileName newline...
        ' If not desired. Run' newline 'clear PSFFileName' newline 'in the command window and rerun this code section']);
end

% Ask user for loading a PSF file
if ~exist('PSF','var') 
        
    [~,~,ext] = fileparts(PSFFileName); 
    if strcmp(ext,'.mat')
        loadstructure = load([PSFPathName PSFFileName]);
        fnames = fieldnames(loadstructure);
        if length(fnames)>1
            clear('PSFFIleName','PSFPathName')
            error('not a valid PSF, multiple variables present in the .mat file')
        else
            PSF = loadstructure.(fnames{:});
        end
    else
        PSF = double(imreadstack([PSFPathName PSFFileName],'all'));
    end
    
else    
    warning(['Using PSF already loaded in the workspace. If not desired. Run ' newline...
        'clear PSF PSFdims' newline 'and potentially' newline 'clear PSFFileName' newline...
        'in the command window and rerun this code section']);    
end

if ~exist('PSFdims','var')
    % If the size is not imbedded in the PSF image file, ask user for the size (in um)
    PSFdims = inputdlg({'Full PSF x and y (um)',' Full z (um)','Absolute PixelSize x and y (um)',...
        'Absolute PixelSize z (um)','relative PixelSize x and y','relative PixelSize Z'},...
        'Enter the absolute PSF dimensions, or absolute/relative pixelsize',...
        [1 50; 1 50;1 50; 1 50;1 50; 1 50],{ 'NaN', 'NaN', 'NaN', 'NaN', '1', 'NaN'}); 
    PSFdims = str2double(PSFdims);
end

allpixsizexy = [MPStats.PixelSizeXY]        ;
allpixsizez  = [MPStats.PixelSizeZ_original];

% Here we determine the size of the PSF in similarly sized pixels as the image (note that I round, which causes a small error)
if ~isnan(PSFdims(1))
    PSF_NewSizeXY = round(PSFdims(1)./(allpixsizexy));
elseif ~isnan(PSFdims(3)) 
    PSF_NewSizeXY = round(size(PSF,1)*PSFdims(3)./allpixsizexy);
elseif ~isnan(PSFdims(5))
    PSF_NewSizeXY = round(size(PSF,1)*PSFdims(5)*ones(1,length(MPStats)));
end
if ~isnan(PSFdims(2))
    PSF_NewSizeZ = round(PSFdims(2)./(allpixsizez));
elseif ~isnan(PSFdims(4)) 
    PSF_NewSizeZ = round(size(PSF,3)*PSFdims(4)./allpixsizez);
elseif ~isnan(PSFdims(6))
    PSF_NewSizeZ = round(PSFdims(6)*size(PSF,3)*ones(1,length(MPStats)));
end

% Correction when pixelsize was equalized in XY and Z
if ~all([MPStats.PixelSizeZ_original]./[MPStats.PixelSizeZ] == [MPStats.zcorrfactor]) 
    PSF_NewSizeZ = PSF_NewSizeZ.*allpixsizez./[MPStats.PixelSizeZ];
end

for iMP = 1:length(MPStats)
    
    prevMP = max([1 iMP-1]); % Check for different image dimensions in which case we resize the PSF
    
    if iMP == 1 || PSF_NewSizeXY(prevMP) ~= PSF_NewSizeXY(iMP) || PSF_NewSizeZ(prevMP) ~= PSF_NewSizeZ(iMP)
        PSF3D = imresize3(PSF,[PSF_NewSizeXY(iMP), PSF_NewSizeXY(iMP), PSF_NewSizeZ(iMP)]);
        PSF3D = PSF3D/sum(PSF3D(:));
    end
    
    MPStats(iMP).PSF3D = PSF3D;
    
end

clear('PSF_NewSizeXY','PSF_NewSizeZ','allpixsizexy','loadstructure','fnames','ext','allpixsizez','iMP','prevMP','PSF3D','NMPs')
function[channelcolors] = Get_ChannelsColors_OmeTiff(metadata,data,Nchannels,MPchannel,slicesfirst)

% This function tries to obtain the colors used for the different channels
% in the metadata. These can then be used for plotting later.
Nvolimages = size(data,1);
channelcolors = cell(Nvolimages,1);

if ~isempty(metadata{1}.getChannelColor(0,0))
    
    for iIM = 1:Nvolimages
        for jchannel = 1:Nchannels
            colID = 16777216 + str2double(metadata{iIM}.getChannelColor(0,jchannel-1).char);
            B = rem(colID,256);
            G = floor(rem(colID/256,256));
            R = floor(rem(colID/65536,256));
            if all([R==255 G==255 B==255]);   R = 0; B = 0; G = 0; end
            channelcolors{iIM}{jchannel} = [R G B]/255;
        end
    end
    
elseif ~isempty(regexp(data{1}{1,2},'\d\d\dnm','once'))
    
    % Here I need to know the order of the channels to get the right slices
    for iIM = 1:Nvolimages
        
        if slicesfirst
            Nplanes = size(data{iIM},1);
            whichslices = 1:(Nplanes/Nchannels):Nplanes;
        else
            whichslices = 1:Nchannels;
        end
        
        for jchannel = whichslices
            labda = data{iIM}{jchannel,2}(regexp(data{iIM}{jchannel,2},'\d\d\dnm')'+(0:2));
            channelcolors{iIM}{jchannel} = spectrumRGB(str2double(labda)); 
        end
    end
    
%else
    
    %colors = cell(1,Nchannels);
    %colors{MPchannel} = uisetcolor([0.75 0.3 0.8]);
    %[channelcolors{:}] = deal(colors);
    
end

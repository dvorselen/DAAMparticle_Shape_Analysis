function[] = Save_Coordinates_Triangulation_Mask(MPStats,OutputFolder,singlefile)

if isempty(which('writematrix'))
    error(['This function requires the function "writematrix", which was introduced in matlab 2019a.'...
        ' If you want to use this functionality please update your Matlab to a more recent version.'])
end

if nargin < 2
    OutputFolder = '';
end
if nargin < 3
    singlefile = false;
end

for iMP = 1:length(MPStats)
   
    [edgecoor,~,edgecoor_sph,~] = check_processing_status(MPStats(iMP)); 
    
    if isempty(OutputFolder)
        basefilenamei = ['Particle' num2str(iMP,'%02d')];
    else
        if ~isfolder(fullfile(OutputFolder,'CSVfiles'))
            mkdir(fullfile(OutputFolder,'CSVfiles'))
        end
        [~,startfilename] = fileparts(MPStats(iMP).FileName);
        basefilenamei     = fullfile(OutputFolder,'CSVfiles',[startfilename '__Particle' num2str(iMP)]);
    end
    
    if isfield(MPStats,'Fraction_engulfed')
        if MPStats(iMP).Fraction_engulfed == 1
            basefilenamei = [basefilenamei '_Fully_Internalized']; %#ok<AGROW>
        elseif MPStats(iMP).Fraction_engulfed == 0
            basefilenamei = [basefilenamei '_Fully_External']; %#ok<AGROW>
        end
    end
    
    if ~isfield(MPStats,'Curvature_principals')
        [PC,GC,MC,XYZsmooth,ThetaPhiRsmooth] = Smooth_and_Get_Curvatures(MPStats(iMP),1);
        pc = PC{1};     gc = GC{1};     mc = MC{1};
    else
        pc = MPStats(iMP).Curvature_principals;
        gc = MPStats(iMP).Curvature_gaussian  ;
        mc = MPStats(iMP).Curvature_mean      ;            
        % Obtain smoothed version
        ThetaPhiRsmooth = Smooth_spherical_mesh(edgecoor_sph,sqrt(1/pi),'arc'); 
        [X,Y,Z] = sph2cart(ThetaPhiRsmooth(:,1),ThetaPhiRsmooth(:,2),ThetaPhiRsmooth(:,3));
        XYZsmooth = [X,Y,Z];
    end

    % Also obtain a dilated mask (dilated by 1 um)
    if isfield(MPStats(iMP),'isincontact') && ~any(MPStats(iMP).Fraction_engulfed == [0,1])
        gridsize = MPStats(iMP).edgecoor_gridsize;
        Phi      = reshape(repmat(linspace(-.5*pi,.5*pi,gridsize(2))',1,gridsize(1))',[],1);   
        Theta    = repmat(linspace(-pi,pi-2*pi/gridsize(1),gridsize(1))',gridsize(2),1);
        pgcdist  = bwdist_spherical(Theta,Phi,MPStats(iMP).actcont_mask);
        contmask = pgcdist*(3/4/pi*MPStats(iMP).Volume)^(1/3)<1;
        iscovered = Interpolate_spherical_surface(MPStats(iMP).edgecoor_sph_wcatorigin(:,1),...
            MPStats(iMP).edgecoor_sph_wcatorigin(:,2),Theta,Phi,contmask)>0.5;
        %iscovered(iscovered<0) = 0;
        if size(iscovered) ~= size(MPStats(iMP).isincontact)
            iscovered = iscovered' | MPStats(iMP).isincontact;
        else
            iscovered = iscovered | MPStats(iMP).isincontact;
        end
        
    end
    
    if ~singlefile           % save all variables in individual text files
        writematrix(edgecoor                              , [basefilenamei '_Coordinates_Cart.csv'             ])
        writematrix(edgecoor_sph                          , [basefilenamei '_Coordinates_Sph.csv'              ])
        writematrix(MPStats(iMP).TRI_Connectivity_SPHbound, [basefilenamei '_Connectivity.csv'                 ])
        writematrix(ThetaPhiRsmooth                       , [basefilenamei '_Coordinates_Sph_smoothed.csv'     ])  
        writematrix(XYZsmooth                             , [basefilenamei '_Coordinates_Cart_smoothed.csv'    ])   
        writematrix(pc                                    , [basefilenamei '_Principal_curvatures_smoothed.csv'])
        writematrix(gc                                    , [basefilenamei '_Gaussian_curvature_smoothed.csv'  ])
        writematrix(mc                                    , [basefilenamei '_Mean_curvature_smoothed.csv'      ])

        if isfield(MPStats(iMP),'isincontact')         % save masks
            writematrix(~MPStats(iMP).isincontact         , [basefilenamei '_Mask.csv'                         ])
            if ~any(MPStats(iMP).Fraction_engulfed == [0,1]) 
                writematrix(~iscovered                    , [basefilenamei '_Mask_1um_dilated.csv'             ]) 
            end
        end
        
    else % Save for each particle all statistics in a single excel file
        
        triangulationTable = table(MPStats(iMP).TRI_Connectivity_SPHbound,'VariableNames',{'Triangulation'});
        dataTable = table(edgecoor,edgecoor_sph,XYZsmooth,ThetaPhiRsmooth,pc',gc',mc',...
            'VariableNames',{'Coor_Cart','Coor_Sph','Coor_Cart_smoothed','Coor_Sph_smoothed',...
            'Principal_curvatures','Gaussian_curvature','Mean_curvature'});
        
        if isfield(MPStats(iMP),'isincontact')
            dataTable.Stain_Max = MPStats(iMP).stain_int;
            dataTable.Stain_Int = MPStats(iMP).stain_int_integrated;
            dataTable.Mask      = ~MPStats(iMP).isincontact;
            if ~any(MPStats(iMP).Fraction_engulfed == [0,1])
                dataTable.Mask_dilated = ~iscovered;
            end
        end
        
        if isfield(MPStats(iMP),'stain_ch1_int')
            dataTable.Stain2_Max = MPStats(iMP).stain_ch1_int;
            dataTable.Stain2_Int = MPStats(iMP).stain_ch1_int_integrated;            
        end
        
        writetable(triangulationTable,[basefilenamei '.xls'] ,'Range','A1') % triangulation has different dims and is written separately
        writetable(dataTable         ,[basefilenamei '.xls'] ,'Range','D1')
    
    end 
end

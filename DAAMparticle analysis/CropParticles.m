function[MPStats] = CropParticles(MPStats,IMStats,padding)

for iMP = 1:length(MPStats)
    
    IMnumberiMP = MPStats(iMP).IMnumber;
    
    xyFromEdge = round(padding(1)    / MPStats(iMP).PixelSizeXY); 
    zFromEdge  = round(padding([2 3])/ MPStats(iMP).PixelSizeZ );
    
    currIM           = IMStats(IMnumberiMP).IM3D;
    currIMsize       = size(currIM);
    [idxx,idxy,idxz] = ind2sub(currIMsize,MPStats(iMP).PixelIdxList);

    [xbl,xblidx] = max([ 1 , min(idxx) - xyFromEdge ]);
    [xbu,xbuidx] = min([ currIMsize(1)  , max(idxx) + xyFromEdge - 1]);
    [ybl,yblidx] = max([ 1 , min(idxy) - xyFromEdge ]);
    [ybu,ybuidx] = min([ currIMsize(2)  , max(idxy) + xyFromEdge - 1]);
    [zbl,zblidx] = max([ 1 , min(idxz) - zFromEdge(1) ]);
    [zbu,zbuidx] = min([ currIMsize(3)  , max(idxz) + zFromEdge(2) ]);

    % Warn user about particles close to the edge
    boundaryproblems = [xblidx xbuidx yblidx ybuidx zblidx zbuidx]==1;
    if any(boundaryproblems)
        warning(['Particle ' num2str(iMP) ' close to image boundaries, can cause problems with deconvolution'])
        whichboundaryproblem  = {'lower x limit', 'upper x limit', 'lower y limit','upper y limit', 'lower z limit', 'upper z limit'};
        MPStats(iMP).warnings = ['Particle close to ' fullfile(whichboundaryproblem{boundaryproblems}) ' image boundary'];
    end
    
    MPStats(iMP).IM3D       = currIM(xbl:xbu,ybl:ybu,zbl:zbu);
    MPStats(iMP).zoomBox    = [xbl ybl zbl; xbu ybu zbu];
    MPStats(iMP).Centroidzm = MPStats(iMP).Centroid - [ybl,xbl,zbl] + 1;
    % Background subtraction
    MPStats(iMP).IM_bgcorr  = currIM(xbl:xbu,ybl:ybu,zbl:zbu) - IMStats(IMnumberiMP).BackgroundInt;
    
end 

function[Frac_engulfed] = Calc_Frac_engulfed(Theta,Phi,iscovered,Nbins)
% This function calculates the fraction of the sphere covered by the mask.
% It works by binning the data in equally sized bins, and checking for each 
% bin if it is covered or not. This way, it is insensitive to the local
% density of points

if ~islogical(iscovered)
    if all(iscovered == 0 | iscovered == 1)
        iscovered = logical(iscovered);
    else
        error('Input argument 3 should be a binary mask (only containing 0s and 1s)')
    end
end

% Determine binnning in both directions
nphibins = round(sqrt(Nbins/2));
thetabins = linspace(-pi,pi,2*nphibins);
phibins   = linspace(-1,1,nphibins);

% Calculate the number of covered and uncovered points in each bin
Ncov  = histcounts2(Theta(iscovered),sin(Phi(iscovered)),thetabins,phibins);
Nopen = histcounts2(Theta(~iscovered),sin(Phi(~iscovered)),thetabins,phibins);

% Use the two estimates to calculate the fraction engulfed
%Frac_engulfed = sum(Ncov(:)>Nopen(:))./sum((Ncov(:)+Nopen(:))>0);
Frac_engulfed = nanmean(Ncov(:)./(Ncov(:)+Nopen(:)));
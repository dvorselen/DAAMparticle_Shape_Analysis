function [MPbounds,ResNorm,Problems_encountered,GridSizeOut] = ...
    Super_resolve_edge_3D(MPCentroid, MPDiam, currIMedges, PixelSizeXYZ, varargin)

% Check user input and otherwise use default method
DefStruct = struct('superResolveEdge',1,'regulargridding',1,'gridspacing',250,'MinPeakSize',100,...
    'mask',ones(size(currIMedges)),'dilatemask',5,'originalImage',[],'StableMode',0,'MaxWidth',3,...
    'Directderivative',0,'smooth',0,'RadialBounds',[0.25 0.75]);
Args = parseArgs(varargin,DefStruct,[]);

% Check user input and update parameters accordingly
if Args.Directderivative
    if ~isempty(Args.originalImage)
        directderivative = 1;
        currIMedges      = double(Args.originalImage);
    else
        directderivative = 1;
    end
else
    directderivative = 0;
end
if ~any(Args.superResolveEdge == [0,1])
    warning(['superResolveEdge should be a logical: 1 for sub-pixel localization, '...
        '0 for localization of highest intensity pixel. Assumed 1'])
    Args.superResolveEdge = 1;
end
if ~any(Args.regulargridding == [0,1])
    warning('regulargridding should be a logical: 1 for a regular grid, 0 for equally spaced grid. Assumed 1')
    Args.regulargridding = 1;
end
if ~isa(Args.gridspacing,'double')
    warning('gridspacing should be the wanted spacing of the grid, and should be numeric, assumed the default (0.25 um)')
    Args.gridspacing = 0.25;
end
if ~isa(Args.MinPeakSize,'double')
    warning('MinPeakSize should be the desired threshold for peak detection and should be numeric, used the default (1)')
    Args.MinPeakSize = 1;
end
if isempty(Args.mask)
    Args.mask = ones(size(currIMedges));
elseif ~size(Args.mask) == size(currIMedges) 
    warning('Argument "mask" should have the same size as the current image, assumed no mask')
    Args.mask = ones(size(currIMedges));
end
if ~isa(Args.dilatemask,'double')
    warning('Argument "dilatemask" should be a double, assumed the default (5 pixels)')
    Args.dilatemask = 5;
end
if Args.StableMode 
    npeaks = 3;
else
    npeaks = 1;
end
rangebounds = Args.RadialBounds; 

% Normalize pixelsize
norm_XYZ   = nthroot(PixelSizeXYZ(2)/PixelSizeXYZ(1),3);
norm_XYZsq = norm_XYZ^2;

% Determine the number of points on the surface and find their coordinates
MPDiam_physunit = MPDiam*norm_XYZ*PixelSizeXYZ(1); % Correct radius for different x,y & z pixelsize
if Args.regulargridding % Case regular grid
    GridSize = floor(pi*MPDiam_physunit/Args.gridspacing);
    [Theta,Phi,GridSizeOut] = Pack_regular_Points_on_Sphere(GridSize);
    NAngles = GridSizeOut(1)*GridSizeOut(2);
else % Case uniformly distributed points
    NAngles = floor(.91*4*MPDiam_physunit.^2/(Args.gridspacing^2));
    % .91 comes from optimal circular packing, which covers .91 of the particle surface (if there are many points)
    [Theta,Phi] = Pack_points_on_Sphere(NAngles);
    GridSizeOut = NaN;
end
% Find the cartesian coordinates of the grid
[Xunit,Yunit,Zunit] = sph2cart(Theta,Phi,ones(1,NAngles));

% Create lines from the centroid of the particle crossing the boundary
LineLength = round(MPDiam*(rangebounds(2)+0.05));
Line = 1:LineLength;
% Correct linelength for different pixelsizes
xcoor = MPCentroid(1) + Xunit' * norm_XYZ    * Line;
ycoor = MPCentroid(2) + Yunit' * norm_XYZ    * Line;
zcoor = MPCentroid(3) + 2 + Zunit' / norm_XYZsq  * Line;  % 2 is an emperically determined addition, which leads to more homogenous detected edges at the top and bottom of the particles

if any(~Args.mask(:))
    if Args.dilatemask ~= 0
        Args.mask = imerode(Args.mask,strel('disk',Args.dilatemask));
    end
    currIMedges(~Args.mask) = NaN;
end

% Create a lineprofile using interpolation (while cutting the last three pixels
% of the image as they can be corrupted by edge detection/deconvolution)
if ~directderivative   
    xcoor = xcoor - 3;      ycoor = ycoor - 3;      zcoor = zcoor - 3;  
    LineProfile = interp3(currIMedges(4:end-3,4:end-3,4:end-3),xcoor,ycoor,zcoor,'cubic');
    if ~isempty(Args.originalImage)
        LineProfileOriginalIM = interp3(imgaussfilt3(Args.originalImage(4:end-3,4:end-3,4:end-3),1),xcoor,ycoor,zcoor,'cubic');
        Sign_LineProfile      = [ones(NAngles,1) -sign(diff(LineProfileOriginalIM'))'];
    end
else
    % Note that we add a bit of smoothing on top of the user specified
    % smoothing. This makes the result more similar to sobel filtering
    % where there is some smoothing built into the filter design.
    LineProfileOriginalIM = interp3(imgaussfilt3(currIMedges,sqrt((0.75+Args.smooth)^2)),xcoor,ycoor,zcoor,'cubic');
    LineProfile = convn(LineProfileOriginalIM, [-5 0 5],'same');
    %LineProfile = LineProfile + 0.01*max(LineProfile(:));
    LineProfile(:,[1 end]) = -1;
    Sign_LineProfile       = sign(LineProfile);
end
LineProfile(Sign_LineProfile == -1) = NaN;     

% Create fitting function and set options
gausFun = @(hms,x) hms(1) .* exp (-(x-hms(2)).^2 ./ (2*hms(3)^2));
options = optimset('Display','off','Algorithm','levenberg-marquardt');
optionsbounded = optimset('Display','off');

% Set up inner loop output variables
R = NaN(1,NAngles);
PKS = NaN(1,NAngles); 
ResNorm = NaN(1,NAngles);
Problems_encountered = cell(1,NAngles);

% Determine the coordinates of the maximum line crossing for each
for irot = 1:NAngles
    
    lineprofilei = LineProfile(irot,:);
       
    % Find maxima, and a linear index for them
    range = ceil(MPDiam*rangebounds(1)) : min(floor(MPDiam*rangebounds(2)),length(lineprofilei));
    [pks,locs,w] = findpeaks(lineprofilei(range),'MinPeakProminence',Args.MinPeakSize, 'NPeaks', npeaks,'Sortstr','descend');
    locs = locs + range(1) - 1;
      
    if ~isempty(pks)
        
        if Args.StableMode 
            
            %Determine previously determined points that are spatially close (within 1/4 pi angle)
            if irot < NAngles/8
                simArea = (1:NAngles)<irot;
            else
                simArea = (1:NAngles)<irot & (1:NAngles)>irot-NAngles/6 & (abs(Theta-Theta(irot))<0.5236 | 2*pi-abs(Theta-Theta(irot))<.5236);
            end
            prevpeakrange = prctile(R(simArea),[2.5 97.5]);
            
            % Select the best peak
            if length(pks)>1
                % Select multiple peaks that are at least 50% of the peak height
                selpks = find(pks > .6*max(pks));
                % Select the peak closest to the expected particle radius
                if length(selpks) == 1
                    closest_to_expected_peakloc = selpks;
                elseif irot > 10             
                    if any(locs(selpks)>prevpeakrange(1) & locs(selpks)<prevpeakrange(2))
                        [~,closest_to_expected_peakloc] = min(abs(nanmedian(R(simArea))-locs(selpks))); 
                    else
                        [~,closest_to_expected_peakloc] = min(min(abs(locs(selpks)-prevpeakrange')));
                    end
                    closest_to_expected_peakloc = selpks(closest_to_expected_peakloc);
                else
                    [~,closest_to_expected_peakloc] = min(abs(MPDiam*.95/2-locs(selpks))); % *.95 since often MPDiam is an overestimate
                end
                
                locs = locs(closest_to_expected_peakloc);
                w    = w(closest_to_expected_peakloc)   ;
                pks  = pks(closest_to_expected_peakloc) ;
                
            end
            
            %Check if peak location is very dissimilar compared to expected from previous points
            if irot > NAngles/16
                acceptedrange = max(3,diff(prevpeakrange))*[-1.5 1.5]+prevpeakrange;
                
                % Extra check which compares the local area with the rest of the particle surface
                if diff(prevpeakrange) > 4
                    acceptedrange_entsurf = (prevpeakrange-nanmedian(R(1:irot)))*1.5+prevpeakrange;
                    acceptedrange = [max([acceptedrange(1) acceptedrange_entsurf(1)])  min([acceptedrange(2) acceptedrange_entsurf(2)])];
                end
                
                if locs<acceptedrange(1) || locs>acceptedrange(2)
                    Problems_encountered{irot} = 'Peak detected but very dissimilar to closeby edge and removed';
                    continue;       
                end      
            end                       
        end
        
        if Args.superResolveEdge 
            % Use a gaussian fit to determine the location more accurately
            include = ~excludedata(Line,lineprofilei,'domain',[ceil(locs-w+0.5) floor(locs+w-0.5)]);
            % Make sure that NaNs (arrising from interpolation beyond image
            % boundaries) are not included in the fit
            include(isnan(lineprofilei)) = 0;

            try      % If the fit creates and error, we will ignore it and count it as a missed peak
                lineseg = Line(include);
                lineprofileseg = lineprofilei(include);
                % We first try a faster non-bounded fit
                [fitted_hms,resnormi] = lsqcurvefit(gausFun,[pks,locs,w/2.35],lineseg,lineprofileseg,[],[],options);
                % If the outcome of the fit is off, we try a bounded fit
                if fitted_hms(2) < lineseg(1) || fitted_hms(2) > lineseg(end) || abs(fitted_hms(3)) > w*2  
                    Problems_encountered{irot} = 'Initial fitting parameters out of bound';
                    [fitted_hms,resnormi] = lsqcurvefit(gausFun,[pks,locs,w],lineseg,lineprofileseg,...
                        [0 lineseg(1) 0],[Inf lineseg(end) w*2],optionsbounded);
                end
                m = fitted_hms(2);
            catch ME
                Problems_encountered{irot} = ME;
                continue
            end        
            
            R(irot)       = m;
            PKS(irot)     = pks;
            ResNorm(irot) = resnormi;

        else

            R(irot) = locs(1);
            
        end
    end
end

% Calculate the edge location in cartesian coordinates and save in output variable
if Args.superResolveEdge
    Xloc = MPCentroid(1) + Xunit * norm_XYZ    .* R;
    Yloc = MPCentroid(2) + Yunit * norm_XYZ    .* R;
    Zloc = MPCentroid(3) + 2 + Zunit / norm_XYZsq  .* R;   
    MPbounds = [Xloc; Yloc; Zloc; Theta; Phi; R]';
else
    lin_idx = sub2ind(size(LineProfile),1:NAngles,R);
    MPbounds = [xcoor(lin_idx); ycoor(lin_idx); zcoor(lin_idx); Theta; Phi; R ]';
end

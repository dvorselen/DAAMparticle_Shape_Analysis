%% Plot triangulated 3D surface of particles

% Also plot curvatures and stain
plotcurv      = 0;
plotstain     = 1;
plotmask      = 0;


if ~exist('gamma','var')
    gamma = 1;
end

subplotn = [6,5];
ntot     = subplotn(1)*subplotn(2);

if plotstain
    stainname = 'stain';
    fnames = fieldnames(MPStats);
    second_channel_present = any(contains(fnames,'stain_ch'));
    if second_channel_present
        plotstain2 = 1;
        fnames_secondchannel = fnames(contains(fnames,'stain_ch'));
        stainname2 = fnames_secondchannel{1}(strfind(fnames_secondchannel{1},'stain_ch')+(0:8));
    else
        plotstain2 = 0;
    end
end

nbatches = ceil(length(MPStats)/ntot);
AH = cell(nbatches,1);
if exist('plotstain','var') 
    if plotstain == 1
        AH2 = cell(nbatches,1);
        AH3 = cell(nbatches,1);
    end
end
if exist('plotcurv','var')    
    if plotcurv == 1
        AH4 = cell(nbatches,1);
        AH5 = cell(nbatches,1);
    end
end
if exist('plotmask','var')
    if plotmask == 1
        AH6 = cell(nbatches,1);
    end
end

for jbatch = 1:nbatches
    
    fh = figure('Name','Distance to Centroid Sphere');
    fh.Position = [1100 80 1064 1064];
    ah = tight_subplot(subplotn(1),subplotn(2),[.01 .01],[0.01 0.01],[0.01 0.01]);
    
    if exist('plotstain','var')
        if plotstain == 1
            if isfield(MPStats,[stainname '_int'])
                % Actin
                fh2 = figure('Name','LifeAct');
                fh2.Position = [1100 80 1064 1064];
                ah2 = tight_subplot(subplotn(1),subplotn(2),[.01 .01],[0.01 0.01],[0.01 0.01]);
                stainmap = ones(256,3)*.05;
                stainmap(:,2) = linspace(.05,.9,256);
            end

            if plotstain2
                if isfield(MPStats,[stainname2 '_int'])
                    % IO_Stain
                    fh3 = figure('Name','Stain');
                    fh3.Position = [1100 80 1064 1064];
                    ah3 = tight_subplot(subplotn(1),subplotn(2),[.01 .01],[0.01 0.01],[0.01 0.01]);

                    stainmap2 = ones(256,3)*.05;
                    stainmap2(:,1) = linspace(.05,.9,256);
                    stainmap2(:,3) = linspace(.05,.9,256);
                end
            end
        end
    end
    
    if exist('plotcurv','var')
        if plotcurv == 1
            
            fh4 = figure('Name','Mean Curvature deviation');
            fh4.Position = [1200 150 1064 1064];
            ah4 = tight_subplot(subplotn(1),subplotn(2),[.01 .01],[0.01 0.01],[0.01 0.01]);
            
            fh5 = figure('Name','Gaussian Curvature');
            fh5.Position = [1200 150 1064 1064];
            ah5 = tight_subplot(subplotn(1),subplotn(2),[.01 .01],[0.01 0.01],[0.01 0.01]);
            
        end
    end
    
    if exist('plotmask','var')
        if plotmask == 1           
            fh6 = figure('Name','Mask');
            fh6.Position = [1200 150 1064 1064];
            ah6 = tight_subplot(subplotn(1),subplotn(2),[.01 .01],[0.01 0.01],[0.01 0.01]);                       
        end
    end
    
    for iMP = (1+(jbatch-1)*ntot):min(ntot*jbatch,length(MPStats))
        [~,centroidcoor,edgecoor_sph,TRI_nosph] = check_processing_status(MPStats(iMP));
        ThetaPhiR = edgecoor_sph;
        R = ThetaPhiR(:,3);
        [X,Y,Z] = sph2cart(ThetaPhiR(:,1),ThetaPhiR(:,2),ThetaPhiR(:,3));

        % Plot the shape
        ahi = ah(iMP-(jbatch-1)*ntot); 
        Make_3D_plot(MPStats(iMP).TRI_Connectivity_SPHbound,[Z,Y,X],R,ahi);
        ahi.Colormap = brewermap([],'RdBu');
        caxis(ahi,median(R)+[-.5 .5])

        if exist('plotstain','var')
            if plotstain == 1
                %Plot the LifeAct
                if isfield(MPStats,[stainname '_int'])
                    ahi = ah2(iMP-(jbatch-1)*ntot); 
                    stainint = MPStats(iMP).([stainname '_int']);
                    if gamma ~= 1
                        stainint = real(stainint.^gamma);
                    end                    
                    Make_3D_plot(MPStats(iMP).TRI_Connectivity_SPHbound,[Z,Y,X],stainint,ahi);
                    ahi.Colormap = stainmap;
                    caxis(ahi,[prctile(MPStats(iMP).stain_int,1) prctile(MPStats(iMP).stain_int,99.5)])
                end

                %Plot the stain
                if plotstain2
                    if isfield(MPStats,[stainname2 '_int'])
                        ahi = ah3(iMP-(jbatch-1)*ntot); 
                        stainint = MPStats(iMP).([stainname2 '_int']);
                        if gamma ~= 1
                            stainint = real(stainint.^gamma);
                        end                        
                        Make_3D_plot(MPStats(iMP).TRI_Connectivity_SPHbound,[Z,Y,X],stainint,ahi);
                        ahi.Colormap = stainmap2;
                        caxis(ahi,[prctile(stainint,1) prctile(stainint,99)])
                    end
                end
            end
        end
        
        if exist('plotmask','var')
            if plotmask == 1
                %Plot the Mask
                ahi = ah6(iMP-(jbatch-1)*ntot); 
                stainint = MPStats(iMP).isincontact;
                Make_3D_plot(MPStats(iMP).TRI_Connectivity_SPHbound,[Z,Y,X],stainint);
                ahi.Colormap = gray;
                caxis(ahi,[0 1])
            end
        end
        
        if exist('plotcurv','var')    
            if plotcurv == 1
                
                [ThetaPhiRsmooth] = Smooth_spherical_mesh(edgecoor_sph,sqrt(1^2/pi),'arc',1,'mean');
                [X,Y,Z] = sph2cart(ThetaPhiRsmooth(:,1),ThetaPhiRsmooth(:,2),ThetaPhiRsmooth(:,3));    
            
                FV.faces    = MPStats(iMP).TRI_Connectivity_SPHbound;
                FV.vertices = [X,Y,Z];
                pc = GetCurvatures(FV,0);
                gc = pc(1,:).*pc(2,:);
                mc = mean(pc);

                ahi = ah4(iMP-(jbatch-1)*ntot); 
                Make_3D_plot(MPStats(iMP).TRI_Connectivity_SPHbound,[Z,Y,X], mc - median(mc));
                ahi.Colormap = turquoisebrown;
                caxis(ahi,[-0.3 0.3])

                ahi = ah5(iMP-(jbatch-1)*ntot); 
                Make_3D_plot(MPStats(iMP).TRI_Connectivity_SPHbound,[Z,Y,X],gc);
                ahi.Colormap = greenmagenta;
                caxis(ahi,[-0.18 0.18])
                
            end
        end
        
    end
    
    AH{jbatch}  = ah ;
    if exist('plotstain','var') 
        if plotstain == 1
            if isfield(MPStats,[stainname '_int'])
                AH2{jbatch} = ah2;
                if plotstain2
                    if isfield(MPStats,[stainname2 '_int'])
                        AH3{jbatch} = ah3;
                    end
                end
            end
        end
    end
    if exist('plotcurv','var')    
        if plotcurv == 1
            AH4{jbatch} = ah4;
            AH5{jbatch} = ah5;
        end
    end
    if exist('plotmask','var')
        if plotmask == 1
            AH6{jbatch} = ah6;
        end
    end
end

if rem(iMP,ntot)~=0
    for iax = ntot:-1:rem(iMP,ntot)+1
        delete(ah(iax))
        ah(iax) = [];
        if exist('plotstain','var')
            if plotstain == 1
                if isfield(MPStats,[stainname '_int']) ;    delete(ah2(iax));   ah2(iax) = [];       end
                if plotstain2
                    if isfield(MPStats,[stainname2 '_int']);    delete(ah3(iax));   ah3(iax) = [];       end
                end
            end
        end
        if exist('plotmask','var')
            if plotmask == 1;   delete(ah6(iax));   ah6(iax) = [];      end
        end
        if exist('plotcurv','var')    
            if plotcurv == 1   
                delete(ah4(iax));   delete(ah5(iax));
                ah4(iax) = []   ;   ah5(iax) = []   ;
            end
        end
    end    
end

AH{jbatch}  = ah ; %#ok<NASGU>
if exist('plotstain','var') 
    if plotstain == 1
        if isfield(MPStats,[stainname '_int'])
            AH2{jbatch} = ah2;
            if plotstain2
                if isfield(MPStats,[stainname2 '_int'])
                    AH3{jbatch} = ah3;
                end
            end
        end
    end
end
if exist('plotcurv','var')    
    if plotcurv == 1
        AH4{jbatch} = ah4;
        AH5{jbatch} = ah5;
    end
end
if exist('plotmask','var')
    if plotmask == 1
        AH6{jbatch} = ah6;
    end
end

%% Plot triangulated 2D projection of particle surface

% Use mollweid projections (if not, equirectangular map projections are used)
if ~exist('mollweid_proj','var')
    if ~exist('stereograph_proj','var')
        mollweid_proj    = 1;
        stereograph_proj = 0;
    elseif stereograph_proj == 1
        mollweidproj = 0;
    else
        mollweidproj = 1;        
    end
end

if ~exist('plotstain','var')
    plotstain = 1;
end

if ~exist('gamma','var')
    gamma = 1;
end

if ~exist('plot_integrated','var')
    plot_integrated = [];
end

if stereograph_proj == 1
    subplotn = [7,7];
else
    subplotn = [8,5];
end
ntot     = subplotn(1)*subplotn(2);

if exist('plotstain','var')   
    if plotstain
        stainname = 'stain';
        fnames = fieldnames(MPStats);
        second_channel_present = any(contains(fnames,'stain_ch'));
        if second_channel_present
            plotstain2 = 1;
            fnames_secondchannel = fnames(contains(fnames,'stain_ch'));
            stainname2 = fnames_secondchannel{1}(strfind(fnames_secondchannel{1},'stain_ch')+(0:8));
        else
            plotstain2 = 0;
        end
    end
end

nbatches = ceil(length(MPStats)/ntot);
AH = cell(nbatches,1);
if exist('plotstain','var') 
    if plotstain == 1
        AH2 = cell(nbatches,1);
        AH3 = cell(nbatches,1);
    end
end
if exist('plotcurv','var')    
    if plotcurv == 1
        AH4 = cell(nbatches,1);
        AH5 = cell(nbatches,1);
    end
end
if exist('plotmask','var')
    if plotmask == 1
        AH6 = cell(nbatches,1);
    end
end

for jbatch = 1:nbatches
    
    fh = figure('Name','Distance to Centroid Map');
    fh.Position = [1100 80 1064 1064];
    ah = tight_subplot(subplotn(1),subplotn(2),[.01 .01],[0.01 0.01],[0.01 0.01]);
        
    if exist('plotstain','var')    
        if plotstain
            if isfield(MPStats,[stainname '_int'])
                % Actin
                fh2 = figure('Name','LifeAct');
                fh2.Position = [1100 80 1064 1064];
                ah2 = tight_subplot(subplotn(1),subplotn(2),[.01 .01],[0.01 0.01],[0.01 0.01]);

                stainmap = linspace(.05,.9,256)'.*ones(1,3);
                unif_caxis = [prctile(vertcat(MPStats.(['stain_int' plot_integrated])),1) ...
                    prctile(vertcat(MPStats.(['stain_int' plot_integrated])),99)];
            else
                plotstain = 0;
            end

            if plotstain2
                if isfield(MPStats,[stainname2 '_int'])
                    % IO_Stain
                    fh3 = figure('Name','Stain');
                    fh3.Position = [1100 80 1064 1064];
                    ah3 = tight_subplot(subplotn(1),subplotn(2),[.01 .01],[0.01 0.01],[0.01 0.01]);

                    stainmap2 = ones(256,3)*.05;
                    stainmap2(:,1) = linspace(.05,.9,256);
                    stainmap2(:,3) = linspace(.05,.9,256);
                    unif_caxis2 = [prctile(vertcat(MPStats.([stainname2 '_int' plot_integrated])),1) ...
                        prctile(vertcat(MPStats.([stainname2 '_int' plot_integrated])),99)];
                end
            end
        end
    end
    
    if exist('plotmask','var')
        if plotmask == 1           
            fh6 = figure('Name','Mask');
            fh6.Position = [1200 150 1064 1064];
            ah6 = tight_subplot(subplotn(1),subplotn(2),[.01 .01],[0.01 0.01],[0.01 0.01]);                       
        end
    end
    
    if exist('plotcurv','var')
        if plotcurv == 1
            
            fh4 = figure('Name','Mean Curvature deviation');
            fh4.Position = [1200 150 1064 1064];
            ah4 = tight_subplot(subplotn(1),subplotn(2),[.01 .01],[0.01 0.01],[0.01 0.01]);
            
            fh5 = figure('Name','Gaussian Curvature');
            fh5.Position = [1200 150 1064 1064];
            ah5 = tight_subplot(subplotn(1),subplotn(2),[.01 .01],[0.01 0.01],[0.01 0.01]);
            
        end
    end
           
    for iMP = (1+(jbatch-1)*ntot):min(ntot*jbatch,length(MPStats))
        [edgecoor,centroidcoor,edgecoor_sph,TRI_nosph] = check_processing_status(MPStats(iMP));
        ThetaPhiR = edgecoor_sph;
        R = ThetaPhiR(:,3);
        if mollweid_proj
            [Xmw,Ymw,TRI_nosph] = MollweidProj(ThetaPhiR(:,1),ThetaPhiR(:,2));
            ThetaPhiR(:,[1 2]) = [Xmw,Ymw];
        end
        
        % Plot the shape
        ahi = ah(iMP-(jbatch-1)*ntot);
        Make_2D_plot(TRI_nosph,ThetaPhiR,R,ahi,stereograph_proj);
        ahi.Colormap = brewermap([],'RdBu');
        caxis(ahi,median(R)+[-.6 .6])

        %Plot the LifeAct
        if exist('plotstain','var') 
            if plotstain
                if isfield(MPStats,[stainname '_int'])
                   stainint = MPStats(iMP).([stainname '_int' plot_integrated]);
                   if gamma ~= 1
                       stainint = real(stainint.^gamma);
                   end
                   ahi = ah2(iMP-(jbatch-1)*ntot);
                   Make_2D_plot(TRI_nosph,[ThetaPhiR(:,1:2) stainint],stainint,ahi,stereograph_proj);
                   ahi.Colormap = stainmap;
                   caxis(ahi,[prctile(stainint,1) prctile(stainint,99)])
                end

                if plotstain2
                    if isfield(MPStats,[stainname2 '_int'])
                        stainint = MPStats(iMP).([stainname2 '_int' plot_integrated]);
                        if gamma ~= 1
                            stainint = real(stainint.^gamma);
                        end
                        ahi = ah3(iMP-(jbatch-1)*ntot);
                        Make_2D_plot(TRI_nosph,[ThetaPhiR(:,1:2) stainint],stainint,ahi,stereograph_proj);
                        ahi.Colormap = stainmap2;
                        caxis(ahi,[prctile(stainint,1) prctile(stainint,99)])
                    end
                end
            end
        end
        
        if exist('plotmask','var')
            if plotmask == 1
                %Plot the Mask
                stainint = MPStats(iMP).isincontact;
                ahi = ah6(iMP-(jbatch-1)*ntot);
                Make_2D_plot(TRI_nosph,[ThetaPhiR(:,1:2) stainint],stainint,stereograph_proj);
                ahi.Colormap = gray;
                caxis(ahi,[0 1])
            end
        end     
        
        if exist('plotcurv','var')    
            if plotcurv == 1
                
                [ThetaPhiRsmooth] = Smooth_spherical_mesh(edgecoor_sph,sqrt(1^2/pi),'arc',1,'mean');
                [X,Y,Z] = sph2cart(ThetaPhiRsmooth(:,1),ThetaPhiRsmooth(:,2),ThetaPhiRsmooth(:,3));    
            
                FV.faces    = MPStats(iMP).TRI_Connectivity_SPHbound;
                FV.vertices = [X,Y,Z];
                pc = GetCurvatures(FV,0);
                gc = reshape(pc(1,:).*pc(2,:),[],1);
                mc = reshape(mean(pc),[],1);
                mc = mc - median(mc);

            ahi = ah4(iMP-(jbatch-1)*ntot);
            Make_2D_plot(TRI_nosph,[ThetaPhiR(:,1:2) mc],mc,ahi,stereograph_proj);
            ahi.Colormap = turquoisebrown;
            caxis(ahi,[-0.3 0.3])

            ahi = ah5(iMP-(jbatch-1)*ntot);
            Make_2D_plot(TRI_nosph,[ThetaPhiR(:,1:2) gc],gc,ahi,stereograph_proj);
            ahi.Colormap = greenmagenta;
            caxis(ahi,[-0.18 0.18])
                
            end
        end                              
    end
    
    AH{jbatch}  = ah ;
    if exist('plotstain','var') 
        if plotstain == 1
            AH2{jbatch} = ah2;
            if plotstain2
                if isfield(MPStats,[stainname2 '_int'])
                    AH3{jbatch} = ah3;
                end
            end
        end
    end
    if exist('plotcurv','var')    
        if plotcurv == 1
            AH4{jbatch} = ah4;
            AH5{jbatch} = ah5;
        end
    end
    if exist('plotmask','var')
        if plotmask == 1
            AH6{jbatch} = ah6;
        end
    end
    
    
end

if rem(iMP,ntot)~=0
    for iax = ntot:-1:rem(iMP,ntot)+1
        delete(ah(iax))
        ah(iax) = [];
        if exist('plotstain','var')
            if plotstain == 1
                if isfield(MPStats,[stainname '_int']) ;    delete(ah2(iax));   ah2(iax) = [];       end
                if plotstain2
                    if isfield(MPStats,[stainname2 '_int']);    delete(ah3(iax));   ah3(iax) = [];       end
                end
            end
        end
        if exist('plotmask','var')
            if plotmask == 1;   delete(ah6(iax));   ah6(iax) = [];      end
        end
        if exist('plotcurv','var')    
            if plotcurv == 1   
                delete(ah4(iax));   delete(ah5(iax));
                ah4(iax) = []   ;   ah5(iax) = []   ;
            end
        end
    end    
end

AH{jbatch}  = ah ;
if exist('plotstain','var') 
    if plotstain == 1
        AH2{jbatch} = ah2;
        if plotstain2
            if isfield(MPStats,[stainname2 '_int'])
                AH3{jbatch} = ah3;
            end
        end
    end
end
if exist('plotcurv','var')    
    if plotcurv == 1
        AH4{jbatch} = ah4;
        AH5{jbatch} = ah5;
    end
end
if exist('plotmask','var')
    if plotmask == 1
        AH6{jbatch} = ah6;
    end
end
 
%% Equalize axes and color scales

if ~exist('normalize_radius_for_deformation_coloring','var')
    normalize_radius_for_deformation_coloring = 0;
end
% If the value above is zero, the physical size scaling of the particles will be
% equalized, but the color scale will be centred around the median radius
% for each individual particle. If it is set to 1, all particles will also
% have the same absolute color scale.

if isvalid(AH{1}(1))
    if contains(get(get(ah(1),'parent'),'Name'),'Sphere')
        allxlim = vertcat(vertcat(AH{:}).XLim);
        allylim = vertcat(vertcat(AH{:}).YLim);
        allzlim = vertcat(vertcat(AH{:}).ZLim);
        lowerlims = min([allxlim(:,1), allylim(:,1), allzlim(:,1)]);
        upperlims = max([allxlim(:,2), allylim(:,2), allzlim(:,2)]);
        for i = 1:length(AH)
            setlims(AH{i})
        end
    end

    if normalize_radius_for_deformation_coloring
        for i = 1:length(AH)
            [AH{i}.CLim] = deal([ah(1).CLim]+[-.4 .4]);
        end
    end
end

if exist('ah2','var')
    if isvalid(ah2)
        for i = 1:length(AH2)
            [AH2{i}.CLim] = deal(prctile(vertcat(MPStats.stain_int),[1 99]));
            setlims(AH2{i})
        end
    end
end

if exist('ah3','var')
    if isvalid(ah3)
        for i = 1:length(AH3)
            [AH3{i}.CLim] = deal(prctile(vertcat(MPStats.([stainname2 '_int'])),[1 99]));
            setlims(AH3{i})
        end
    end
end

if exist('ah4','var')
    if isvalid(ah4)
        for i = 1:length(AH4)
            [AH4{i}.CLim] = deal([-.5 .5]);
            setlims(AH4{i})
        end
    end
end

if exist('ah5','var')
    if isvalid(ah5)
        for i = 1:length(AH5)
            [AH5{i}.CLim] = deal([-.3 .3]);
            setlims(AH5{i})
        end
    end
end

if exist('ah6','var')
    if isvalid(ah6)
        for i = 1:length(AH6)
            [AH6{i}.CLim] = deal([0 1]);
            setlims(AH6{i})
        end
    end
end

%% Functions for setting plot options

% Set 3D plots options
function Make_3D_plot(TRI,vert,color,ah)

    patch(ah,'faces',TRI,'vertices',vert,'facevertexcdata',mean(color(TRI),2),'facecolor','flat','edgecolor','none');
    view(ah,3)
    axis(ah,'equal')
    zoom(ah,1.4)
    ah.Visible = 'off';    
    set(ah,'CameraViewAngleMode','Manual','Clipping','off')
    view(ah,0,0)

end


% Set 2D plot options
function Make_2D_plot(TRI,vert,color,ah,stereograph)

    if stereograph
        Stereograph_proj(vert,color,0,'top',ah)
    else
        patch(ah,'faces',TRI,'vertices',vert,'facevertexcdata',mean(color(TRI),2),'facecolor','flat','edgecolor','none');
    end
    view(ah,3)
    ah.Visible = 'off';
    axis(ah,'equal')
    view(ah,0,90);
    set(ah,'Clipping','off')

end

function[] = setlims(AHt)

if exist('ah','var')
    if isvalid(ah(1))
        if contains(get(get(ah(1),'parent'),'Name'),'Sphere')
            [AHt.ZLim] = deal([lowerlims(3) upperlims(3)]);
            [AHt.XLim] = deal([lowerlims(1) upperlims(1)]);
            [AHt.YLim] = deal([lowerlims(2) upperlims(2)]);
        end
    end
end

end

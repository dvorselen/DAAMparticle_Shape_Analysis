function[MPchannel,Stain_channel,Stain_channel2,Nchannels,Slicesfirst,VoxelSize,isLLSMdata]...
    = Extract_Essential_Metadata_batch(data,varargin)
% Note, currently can not deal with images for which the channels are
% provided as separate images

% Check function input and set default parameters
DefStruct = struct('istimeseries',0);
Args = parseArgs(varargin,DefStruct,[]);

% Set Stain_channel 2 to empty
Stain_channel2 = [];

% Organize metadata
metadata = data{1,4};
psc_string  = data{1}{1,2}; % String with the number of planes, slices and colors
Nplanes = cellfun(@(x) size(x,1),data(:,1));

% Find number of timepoints and split stack, if all timepoints were
% provided in a single stack
if Args.istimeseries && size(data,1) == 1
    Ntimepoints = int16(double(metadata.getPixelsSizeT(0).getNumberValue));
    Expanded_data = [mat2cell(data{1},repmat(Nplanes/Ntimepoints,Ntimepoints,1),2)...
        repmat(data(2:4),Ntimepoints,1)];
    data = Expanded_data;
%else
%    Ntimepoints = 1;
end

% Find number of channels
[Nchannels,Slicesfirst] = Get_Number_of_Channels_OmeTiff(metadata,psc_string);
%Nslices                = int16(Nplanes)./Nchannels/Ntimepoints;

if Nchannels == 1 % If only 1 channel is detected, ask the user to force split the channels or return an error
    userinputchls = inputdlg({['Only 1 channel found, this function only works with multiple data channels.'...
        ' Do you want to force the image data into multiple channels? If so, how many?'], ...
        ' What is the order of your z-stacks, are stacks going through slices or channels first?'},...
        'Split channels?',[1 80; 1 80],{'3','Slices'});
    Nchannels   = str2double(userinputchls{1})        ;
    Slicesfirst = strcmpi(userinputchls{2},'slices')  ;
    if Nchannels == 1;              error('Code currently needs at least 2 channels');              end
    if mod(Nplanes,Nchannels) ~= 0; error('Number of planes not devisible by number of channels');  end
end
    
% Ask the user to select the channel that contains the microparticles
if Slicesfirst
    checkchannels = round(double(size(data{1},1)/Nchannels).*(double((1:Nchannels))-.5));
else
    checkchannels = floor(size(data{1},1)/2/Nchannels)*Nchannels+(1:Nchannels);
end
MPchannel = deal(UI_Select_Channel(data{1}(checkchannels,1),[]));

if Nchannels == 2 
    Stain_channel = setdiff([1 2],MPchannel);   
else 
    if Slicesfirst
        checkchannels = round(double(size(data{1},1)/Nchannels).*(double((1:Nchannels))-.5));
    else
        checkchannels = floor(size(data{1},1)/2/Nchannels)*Nchannels+(1:Nchannels);
    end
    nonMPchannels = setdiff(1:Nchannels,MPchannel);
    checkchannels = checkchannels(nonMPchannels);
    Stain_channel = nonMPchannels(UI_Select_Channel(data{1}(checkchannels,1),[],'desired stain'));
    
    if nargout > 2
        % Userinput to ask if third channel should be analyzed
        analyse_3rd_channel = questdlg(['Do you want to analyze a third fluorescent signal',...
            ' along with the bead signal and the inside/outside stain?'],...
            '3rd channel','yes','no','yes');
    
        if analyse_3rd_channel        
            if Nchannels == 3
                Stain_channel2 = setdiff([1 2 3],[MPchannel Stain_channel]);
            else     
                if Slicesfirst
                    checkchannels = round(double(size(data{1},1)/Nchannels).*(double((1:Nchannels))-.5));
                else
                    checkchannels = floor(size(data{1},1)/2/Nchannels)*Nchannels+(1:Nchannels);
                end
                newchannels = setdiff(1:Nchannels,[MPchannel Stain_channel]);
                checkchannels = checkchannels(newchannels);
                Stain_channel2 = newchannels(UI_Select_Channel(data{1}(checkchannels,1),[]));
            end
        end
    end
end

% Find pixel size or ask user
VoxelSize = Get_PixelSize_OmeTiff(metadata);

if nargout > 6
    % Ask user if we are dealing with LLSM data if many zero-valued pixels
    % are present
    isLLSMdata = 0;
    pixvalues  = cat(3,data{1}{:,1});
    fraczeros  = sum(pixvalues(:)==0)/numel(pixvalues);

    if fraczeros > .025
        % Check with the user if there is a singificant fraction of zero-valued pixels
        isLLSMdata = questdlg(['There are many zero-valued pixels. ',...
            'Is this LLSM data and do you want to remove zero-padded pixels?'],...
            'zero-valued pixels','yes','no','yes');
        isLLSMdata = strcmp(isLLSMdata,'yes');
    end
end

function[] = Plots_batchmode(MPStats,IMStats,OutputFolder,stain_opts,extensions)

    if nargin < 5
        extensions = {'epsc','tiff'};
    end

    % Labelmatrix 
    for iIM = 1:length(IMStats)

        fh = figure('Name','LabelMatrix (MiP)','visible','off');
        imagesc(max(IMStats(iIM).IMlm,[],3))
        axis equal
        axis off
        hold on

        whichMPs = find([MPStats.IMnumber] == iIM);

        for iMP = whichMPs
            text(MPStats(iMP).Centroid(2),MPStats(iMP).Centroid(1),num2str(iMP),'Color','r','FontSize',16)
        end

        % Save and close fig
        [~,fname] = fileparts(IMStats(iIM).FileName);
        savefigs(fh,fullfile(OutputFolder,'Labelmatrices',[fname '_' fh.Name]),extensions) 
        close(fh)

    end

    NMPs = length(MPStats);

    for iMP = 1:NMPs

        % Distance to Centroid
        [~,~,edgecoor_sph,TRI_nosph] = check_processing_status(MPStats(iMP));

        % Plot the 2D surface
        fh = figure('Name','Dist_to_Centroid','visible','off');
        trisurf(triangulation(TRI_nosph,edgecoor_sph),edgecoor_sph(:,3),'LineStyle','none');
        colormap(brewermap([],'RdBu'))
        caxis([prctile(edgecoor_sph(:,3),1) prctile(edgecoor_sph(:,3),99.5)])
        Set_2D_plot_opts
        add_colorscalebar() 

        % Save and close fig
        [~,fname] = fileparts(MPStats(iMP).FileName);
        savefigs(fh,fullfile(OutputFolder,'Dist_to_Centroid',[fname '_Particle_' num2str(iMP,'%02d') '_' fh.Name]),extensions)
        close(fh)
        
        
        % Plot Mean Curvature
        fh = figure('Name','Mean Curvature deviation','visible','off');
        mc = MPStats(iMP).Curvature_mean;
        mccentred = mc - median(mc);
        trisurf(triangulation(TRI_nosph,edgecoor_sph),mccentred,'LineStyle','none');
        colormap(greenmagenta)
        mclim = max(abs([prctile(mccentred,1) prctile(mccentred,99)]));
        caxis([-mclim mclim])
        Set_2D_plot_opts
        if isfield(MPStats,'CupBase')
            if ~isempty(MPStats(iMP).CupBase)
                hold on
                plot3(-.5*pi,0,MPStats(iMP).CupBase(3)+1,'pk','MarkerFaceColor','none')
            end
        end
        add_colorscalebar()
        
        % Save and close fig
        [~,fname] = fileparts(MPStats(iMP).FileName);
        savefigs(fh,fullfile(OutputFolder,'Curvatures',[fname '_Particle_' num2str(iMP,'%02d') '_' fh.Name]),extensions)
        close(fh)
        
        
        % Plot gaussian curvature
        fh = figure('Name','Gaussian Curvature','visible','off');
        gc = MPStats(iMP).Curvature_mean;
        trisurf(triangulation(TRI_nosph,edgecoor_sph),gc,'LineStyle','none');
        colormap(turquoisebrown)
        gclim = max(abs([prctile(gc,1) prctile(gc,99)]));
        caxis([-gclim gclim])
        Set_2D_plot_opts
        add_CupBase(MPStats(iMP))
        add_colorscalebar()
        
        % Save and close fig
        [~,fname] = fileparts(MPStats(iMP).FileName);
        savefigs(fh,fullfile(OutputFolder,'Curvatures',[fname '_Particle_' num2str(iMP,'%02d') '_' fh.Name]),extensions)
        close(fh)

        
        % Plot Stain
        stainmap = ones(256,3)*.05; % Create a colormap (linear, green)
        stainmap(:,2) = linspace(.05,.9,256);

        if strcmp(stain_opts,'max')  % Process plot options  
            stainint = MPStats(iMP).stain_int;
            fignameappend = ' Max Intensity';
        else    
            stainint = MPStats(iMP).stain_int_integrated;
            fignameappend = ' Integrated Intensity';
        end           

        % plot 2D surface
        fh = figure('Name',['Stain Map' fignameappend],'visible','off');
        trisurf(triangulation(TRI_nosph,edgecoor_sph),stainint,'LineStyle','none');
        colormap(stainmap)
        Set_2D_plot_opts()
        caxis([prctile(stainint,1) prctile(stainint,99.5)])
        add_CupBase(MPStats(iMP))
        add_colorscalebar()

        % Save stain
        savefigs(fh,fullfile(OutputFolder,'Stains',[fname '_Particle_' num2str(iMP,'%02d') '_' fh.Name]),extensions)
        close(fh)
        
        
        % Plot surface mask
        fh = figure('Name','Mask','visible','off');
        trisurf(triangulation(TRI_nosph,edgecoor_sph),single(MPStats(iMP).isincontact),'LineStyle','none');
        colormap gray
        caxis([-0.05 1.05])
        Set_2D_plot_opts()
        add_colorscalebar()
        
        % Save stain mask
        savefigs(fh,fullfile(OutputFolder,'Stains',[fname '_Particle_' num2str(iMP,'%02d') '_' fh.Name]),extensions)
        close(fh)
       
        
        if isfield(MPStats,'stain_ch1_int')
            % Plot second stain
            stainname = 'stain_ch1';
            % Create a colormap (linear, orange)
            stainmap = ones(256,3)*.05;
            stainmap(:,1) = linspace(.05,.9,256) ;
            stainmap(:,2) = linspace(.05,.66,256);

            % Process plot options
            if strcmp(stain_opts,'max')    
                stainint = MPStats(iMP).([stainname '_int']);
                fignameappend = ' Max Intensity';
            else    
                stainint = MPStats(iMP).([stainname '_int_integrated']);
                fignameappend = ' Integrated Intensity';
            end           

            % plot 2D surface
            fh = figure('Name',['Stain2' stainname fignameappend],'visible','off');
            trisurf(triangulation(TRI_nosph,edgecoor_sph),stainint,'LineStyle','none');
            colormap(stainmap)
            Set_2D_plot_opts()
            caxis([prctile(stainint,1) prctile(stainint,99.5)])
            add_CupBase(MPStats(iMP))
            add_colorscalebar()
            
            % Save figs
            savefigs(fh,fullfile(OutputFolder,'Stains',[fname '_Particle_' num2str(iMP,'%02d') '_' fh.Name]),extensions)
            close(fh)      
        end
        
    end
end

%%%%%%%%

function[] = savefigs(fh,name,filetypes)

    if ~isfolder(fileparts(name))
        mkdir(fileparts(name))
    end

    set(fh,'renderer','Painters')

    savefig(fh,[name,'.fig'])

    for jext = 1:length(filetypes)
        currext = filetypes{jext};
        if any(strcmp(currext,{'epsc','tiff'}))
            saveas(fh,[name '.' currext(1:3)],currext)
        else
            saveas(fh,[name '.' currext],currext)
        end
    end
end

% Set 2D plot options
function[] = Set_2D_plot_opts()

    axis off
    axis equal
    xlabel('Theta')
    ylabel('Phi')
    grid off
    view(0,90);
    set(gca,'Clipping','off')

end

% Add colorbar to 2D plot, while keeping the same axis position
function[] = add_colorscalebar()

    ch = colorbar;
    chpos = get(ch,'Position');
    original_axis_pos  = get(gca,'Position');
    original_axis_pos(2) = (1-original_axis_pos(4))/2;
    set(ch,'Position',[chpos(1) 0.5-original_axis_pos(4)/4 chpos(3) original_axis_pos(4)/2]);
    set(gca,'Position',original_axis_pos);

end

% Add cupbase
function[] = add_CupBase(MPStats)

    if isfield(MPStats,'CupBase')
        if ~isempty(MPStats.CupBase)
            hold on
            plot3(-.5*pi,0,MPStats.CupBase(3)+1,'pk','MarkerFaceColor','none')
        end
    end
end
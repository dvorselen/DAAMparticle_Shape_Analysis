function[MPStats,global_stain_threshold] = Convert_secondary_signal_to_mask(MPStats,stain_indicates_contact,varargin)

% Masks are calculated based on the integrated intensity or on the maximum intensity. 
% Depending on your signal, one or the other may work better. Binary masks are made in two steps: 
% First we make an initial estimate by thresholding, then this mask is used as a seed for a subsequent active
% contour algorithm ("snake")

% Check function input and set default parameters
DefStruct = struct('globalthreshold',[],'use_integrated_intensity',0,'use_global_threshold',1);
Args = parseArgs(varargin,DefStruct,[]);

NMPs = length(MPStats);

% First identify fully internalized based on a global threshold
if Args.use_integrated_intensity
    Stain = cellfun(@(x) x-min(x)+1,{MPStats.stain_int_integrated},'UniformOutput',false);
else
    Stain = cellfun(@(x) x-min(x)+1,{MPStats.stain_int},'UniformOutput',false);
end

NaNs_present = cellfun(@(x) any(isnan(x)),Stain);
if any(NaNs_present)
    error(['Nans present in surface stain for particle(s) ' num2str(find(NaNs_present))...
        newline 'This is likely due to using integrated intensity and the particle being close to '...
        'the image boundaries. You can try: ' newline '- using max intensity instead of integrated intensity'...
        newline '- using a smaller interval during "Determine particle coverage by a secondary signal"'...
        newline '- excluding the particle(s) that are problematic: MPStats([' num2str(find(NaNs_present)) ']) = []'])
end

if Args.use_global_threshold && (~isempty(Args.globalthreshold) || NMPs > 1)
    
    if NMPs == 1
        
        global_stain_threshold  = Args.globalthreshold;     
    
    else
        
        warning('off','all')
        if ~stain_indicates_contact
            [global_stain_threshold,~,~,exitflag] = Find_logIntensity_threshold(vertcat(Stain{:}),'UseLogCounts',0,'Npeaks',2,'useksdensity',1);
        else
            [global_stain_threshold,~,~,exitflag] = Find_logIntensity_threshold(vertcat(Stain{:}),'UseLogCounts',0,'useksdensity',1);
        end
        warning('on','all')
        
        % If we can't find a good threshold we will use the previous one
        if exitflag == -1 && ~isempty(Args.globalthreshold)    
            global_stain_threshold  = Args.globalthreshold;
        end
        
    end
    
    % Determine fully internal particles based on the global threshold
    if ~stain_indicates_contact
        
        % Here we determine two thresholds, since the inside of partial coated
        % particles typically forms a distinct peak from fully internalized particles
        Frac_above_global_threshold = cell2mat(cellfun(@(x) (sum(x>global_stain_threshold')/length(x)),Stain,'UniformOutput',false)');
        
        % Fully internal means majority of the intensity below the first threshold and almost entirely below the second 
        Fully_internalized = all(Frac_above_global_threshold < [0.5 0.025],2);
        % Fully external means almost entirely above the second threshold
        Fully_external     = Frac_above_global_threshold(:,2) > 0.9;
        
    else
        
        Frac_above_global_threshold = cellfun(@(x) (sum(x>global_stain_threshold)/length(x)),Stain);       
        Fully_internalized = Frac_above_global_threshold > 0.9;
        Fully_external     = Frac_above_global_threshold < 0.01;
        
    end
    
    % Determine the overall range of the data
    % dataspan = diff(prctile(log(vertcat(Stain{~Fully_internalized})),[1,99]));
    
else
    
    Fully_internalized = zeros(1,length(MPStats));
    Fully_external     = zeros(1,length(MPStats));
    % dataspan = diff(prctile(log(Stain{1}),[1,99]));
    
end

for iMP = 1:NMPs
    
    % Conversion to regular grid because of the snake algorithm
    if strcmp(MPStats(iMP).Gridding,'equi')
        
       [Theta,Phi,gridsize] = ...
            Pack_regular_Points_on_Sphere(round(sqrt(length(MPStats(iMP).edgecoor_cart_wcatorigin)*2)));
        MPStats(iMP).edgecoor_gridsize = gridsize;
        
        % Interpolate on the sphere
        theta_irr = MPStats(iMP).edgecoor_sph_wcatorigin(:,1);
        phi_irr   = MPStats(iMP).edgecoor_sph_wcatorigin(:,2);
        StainIPi = Interpolate_spherical_surface(Theta,Phi,theta_irr,phi_irr,Stain{iMP});
        MPStats(iMP).IMstain_reg = StainIPi;  
        
    else
        
        Theta = MPStats(iMP).stain_coor_sph(:,1)'; 
        Phi = MPStats(iMP).stain_coor_sph(:,2)'  ; 
        gridsize = MPStats(iMP).edgecoor_gridsize;
        
    end
        
    % We can skip the upcoming steps if the particle is fully internal 
    % (or fully external and we used a cellular stain to determine the contact area)
    if Fully_internalized(iMP) || Fully_external(iMP)
        if strcmp(MPStats(iMP).Gridding,'reg') 
            MPStats(iMP).isincontact = false(gridsize)+stain_indicates_contact;    
        else
            MPStats(iMP).isincontact = false(numel(Stain{iMP}),1)+stain_indicates_contact;
        end
        
        MPStats(iMP).Fraction_engulfed = 1;
        
    else
    
        pixintgrid = reshape(StainIPi,gridsize(1),gridsize(2));
    
        % deal with overpresentation over points at the apices
        if mod(gridsize(:,2),2)   % deal with even and odd number of pixels in phi
            pointsperlat = round(gridsize(1) * sqrt(1-((0:(gridsize(2)/2-.5))/(gridsize(2)/2-.5)).^2));
        else
            pointsperlat = round(gridsize(1) * sqrt(1-((0:(gridsize(2)/2))/(gridsize(2)/2)).^2));
        end
        pointsperlat = [pointsperlat(end:-1:2) pointsperlat]; %#ok<AGROW>
        indices = zeros(sum(pointsperlat),2);
        % Here, for each latitude, we select indices of the appropriate number of pixels
        for ilat = 2:gridsize(2)
            latidx = linspace(0,gridsize(1),pointsperlat(ilat)+1);
            range = (sum(pointsperlat(1:ilat-1))+1):sum(pointsperlat(1:ilat));
            indices(range,1) = ilat;
            indices(range,2) = round(latidx(2:end));
        end
        
        % Now we make a corrected histogram of log intensity values
        smoothstainIM = imgaussfilt(reshape(pixintgrid,gridsize(1),gridsize(2)),3);
        pixints = smoothstainIM(sub2ind([gridsize(1) gridsize(2)],indices(:,2),indices(:,1)));
        %stain_threshold = Find_logIntensity_threshold(pixints,'iteration',iMP,...
        %    'BinWidth',dataspan/(numel(Stain{iMP})/50),'bounds',[0.001 99.9]);
        stain_threshold = Find_logIntensity_threshold(pixints,'iteration',iMP,'bounds',[0.001 99.9],'useksdensity',1);
        
        initialmask = smoothstainIM < stain_threshold; % apply threshold        

        % Area's smaller than 500 nm in radius are excluded
        initialmask = bwareaopen(initialmask,round(0.7854/(pi*MPStats(iMP).Equiv_Diameter_um/gridsize(1))^2));
        initialmask = ~bwareaopen(~initialmask,round(0.7854/(pi*MPStats(iMP).Equiv_Diameter_um/gridsize(1))^2));
        if stain_indicates_contact; initialmask = ~initialmask;     end

        % Use an active contour algorithm to optimize the boundary
        % extend the grid to deal with boundaries in x,y
        [newmask,phi,exitflag] = localized_seg(log(pixintgrid),initialmask,200,5,.1,[],[],1,0);
        if exitflag == -1;   warning(['active contour converged to no boundary for particle ' num2str(iMP)]);    end
        
    
        MPStats(iMP).initial_mask  = single(initialmask);           
        MPStats(iMP).actcont_mask  = single(newmask);
        MPStats(iMP).actcont_phi   = phi;
    
        % Determine for each point if it is inside or outside and calculate fraction engulfed
        if strcmp(MPStats(iMP).Gridding,'reg')

            MPStats(iMP).isincontact = logical(newmask);
        
        else
        
            iscovered = logical(round(Interpolate_spherical_surface(theta_irr,phi_irr,Theta,Phi,newmask)))';
            MPStats(iMP).isincontact = iscovered;
        
        end
        
        % Calculate the fraction engulfed
        MPStats(iMP).Fraction_engulfed = Calc_Frac_engulfed(MPStats(iMP).edgecoor_sph_wcatorigin(:,1),...
            MPStats(iMP).edgecoor_sph_wcatorigin(:,2),MPStats(iMP).isincontact,size(MPStats(iMP).edgecoor_cart,1));
        
    end
end

[MPStats.stain_indicates_contact] = deal(stain_indicates_contact);
MPStats = orderfields(MPStats);

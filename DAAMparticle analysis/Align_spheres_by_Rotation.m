function [theta_rot,phi_rot] = Align_spheres_by_Rotation(Theta0Phi0,angles)

Nparticles = size(angles,1);

% Check input
if iscell(Theta0Phi0)
    Theta0Phi0 = reshape(Theta0Phi0,length(Theta0Phi0),1); % Make sure ThetaPhi is a nx1   
else   
    Theta0Phi0 = repmat({Theta0Phi0},Nparticles,1);     
end

theta_rot = cell(1,Nparticles);
phi_rot   = cell(1,Nparticles);

for ipart = 1:Nparticles
    
    thetarotangle = angles(ipart,1);
    phirotangle   = angles(ipart,2);
    
    % Firstly, rotate theta. Then later we can rotate around the y-axis
    theta_roti = Theta0Phi0{ipart}(:,1);
    theta_roti = double(wrapToPi(theta_roti-thetarotangle));
    
    % Rotation will be done in cartesian coordinates, so transform spherical to cart (R doesn't matter, use unit sphere) 
    [Xrot_th,Yrot_th,Zrot_th] = sph2cart(theta_roti,Theta0Phi0{ipart}(:,2),ones(length(theta_roti),1));
    % Construct rotation matrix around y axis
    Rot_Y_MAT = [cos(phirotangle) 0 sin(phirotangle); 0 1 0; -sin(phirotangle) 0 cos(phirotangle)];
    % Rotate all particle coordinates (already rotated around z)
    XYZrot_ph = (Rot_Y_MAT*[Xrot_th';Yrot_th';Zrot_th'])'; 
    
    % If asked by the user, also rotate around x-axis
    if size(angles,2) == 3 
        alpha = angles(ipart,3);
        Rot_X_MAT = [1 0 0;0 cos(alpha) -sin(alpha); 0 sin(alpha) cos(alpha)];
        XYZrot_ph = (Rot_X_MAT*XYZrot_ph')';
    end
            
    % Convert cart coordinates back to spherical
    [theta_roti,phi_roti]   = cart2sph(XYZrot_ph(:,1),XYZrot_ph(:,2),XYZrot_ph(:,3));    
    
    theta_rot{ipart} = theta_roti;
    phi_rot{ipart}   = phi_roti;
   
end
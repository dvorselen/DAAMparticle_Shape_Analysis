function[edgecoor,centroidcoor,edgecoor_sph,TRI_nosph] = check_processing_status(Stats)

% Complicated scenarios when and when not to use aligned data
usealigned = 0;
if isfield(Stats,'edgecoor_cart_aligned')
    if ~isempty(Stats.edgecoor_cart_aligned)
        %if ~isfield(Stats,'Fraction_engulfed')
            usealigned = 1;
        %elseif ~any(Stats.Fraction_engulfed == [0,1])
        %    usealigned = 1;
        %end
    end
end
    
% Check where in the processing pipeline we are 
if usealigned
    edgecoor     = Stats.edgecoor_cart_aligned;
    centroidcoor = [0 0 0];
elseif isfield(Stats,'edgecoor_cart_wcatorigin')
    edgecoor     = Stats.edgecoor_cart_wcatorigin;
    centroidcoor = [0 0 0];
else
    edgecoor     = Stats.edgecoor_cart;
    centroidcoor = Stats.Centroidzm;
end

if usealigned 
    edgecoor_sph     = Stats.edgecoor_sph_aligned;
    TRI_nosph        = Stats.TRI_Connectivity_aligned;
elseif isfield(Stats,'edgecoor_sph_wcatorigin')
    edgecoor_sph     = Stats.edgecoor_sph_wcatorigin;
    TRI_nosph        = Stats.TRI_Connectivity_wcatorigin;
elseif isfield(Stats,'TRI_Connectivity')
    edgecoor_sph     = Stats.edgecoor_sph;
    TRI_nosph        = Stats.TRI_Connectivity;
else
    edgecoor_sph     = Stats.edgecoor_sph;
end
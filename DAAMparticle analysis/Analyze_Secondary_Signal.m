function[MPStats,IMStats] = Analyze_Secondary_Signal(data,MPStats,IMStats,Stain_channel,stain_name,add_channel_number,userinput)
% the sixth argument (userinput), needs to exist of the following 3
% components: radialmargin, stainsmoothing, gaussian signal

% check input
if nargin < 6 ;         add_channel_number = [];        end
if nargin < 7 ;         userinput          = [];        end

% Add channel number to stain 
if add_channel_number
    stain_name = [stain_name '_ch' num2str(Stain_channel)];
end

NMPs = length(MPStats);

% Obtain stain images from data
if ~isempty(data)
        
    if ~isempty(IMStats) % Case both IMStats and MPStats exist
        [~,~,~,IMStats] = Extract_Images(data,IMStats,Stain_channel,'llsmmask',...
            {IMStats.LLSMMask},'zcorrfactor',IMStats(1).zcorrfactor,'channelname',['IM3D_' stain_name]);
        if isfield('IMStats','Z_step_dir')
            IMStats = Check_z_direction(IMStats,['IM3D_' stain_name],[],[],1);
        end

        % Add secondary signals to MPStats
        for iMP = 1:NMPs
            currIM = IMStats(MPStats(iMP).IMnumber).(['IM3D_' stain_name]);
            currzoomcoor = MPStats(iMP).zoomBox;
            currMPstain = currIM(currzoomcoor(1,1):currzoomcoor(2,1),...
                currzoomcoor(1,2):currzoomcoor(2,2),currzoomcoor(1,3):currzoomcoor(2,3));
            MPStats(iMP).(['IM' stain_name]) = currMPstain;
            IMStats(MPStats(iMP).IMnumber).(['IM3D_' stain_name]) = single(currIM);
        end
               
    else % Case single particle movie, no IMStats
        [~,~,~,MPStats] = Extract_Images(data,MPStats,Stain_channel,'llsmmask',...
            {MPStats.LLSMMask},'zcorrfactor',MPStats(1).zcorrFactor,'channelname',['IM' stain_name]);
    end
    
elseif isfield(MPStats,['IM' stain_name '_nondeconv'])
    
    [MPStats.IMstain] = MPStats.(['IM' stain_name '_nondeconv']);
    
elseif ~isfield(MPStats,['IM' stain_name])
    
    error(['Either data (output of "ReadBFImages" in cell 1) should be present, or MPStats should contain ',...
        'the field "IMstain" if the stain was analyzed previously (meaning that this cell was ran and the output ',...
        'stored in MPStats)']) 
end

if isempty(userinput)
    if isfield(MPStats,'IMstain_radialinterval')
        defmarginvalue = num2str(MPStats(1).IMstain_radialinterval);
    else
        defmarginvalue = '1';
    end
    userinput = str2double(inputdlg({'Search margin for stain from known particle edge location (each way, in um)?',...
                'Stain smoothing standard deviation','Can your signal be radially approximated by a Gaussian?'},...
                'Stain analysis options',[1 70],{defmarginvalue,'0.5','0'}));
end
radialmargin   = userinput(1);
stainsmoothing = userinput(2);
gaussiansignal = userinput(3);

boundaryissues = [];

for iMP = 1:NMPs      
    
    currstain = MPStats(iMP).(['IM' stain_name]);
    if stainsmoothing > 0
        currstain_sm = imgaussfilt3(currstain,stainsmoothing);
    else
        currstain_sm = currstain;
    end
    % background subtraction (helps with thresholding)
    [ele,edges] = histcounts(currstain_sm(:),round(length(currstain(:))/20000));
    centres = mean([edges(2:end);edges(1:end-1)]);
    bgstain = centres(ele == max(ele));
    if numel(bgstain)>1
        bgstain = mean(bgstain);
    end
    currstain = currstain - bgstain; 
    MPStats(iMP).(['IM' stain_name '_bgcorr'   ]) = currstain;
    
    % Deconvolve if this was also done with the particles
    if isfield(MPStats,'IM3D_nondeconv') 
        MPStats(iMP).(['IM' stain_name '_nondeconv']) = MPStats(iMP).(['IM' stain_name]);
        currstain = Deconvolve_using_DeconvolutionLab2({currstain},{MPStats.PSF3D});
        MPStats(iMP).(['IM' stain_name])              = currstain{1};
        currstain = imgaussfilt3(currstain{1},stainsmoothing);
    else
        currstain = currstain_sm - bgstain;
    end
          
    % Obtain estimates of parameters in px units
    WC_pxunits = MPStats(iMP).Weighted_Centroid'./[repmat(MPStats(iMP).PixelSizeXY,1,2) MPStats(iMP).PixelSizeZ];
    
    % Now determine the coverage of the particle
    [stain_peak_coor,stain_int,stain_width,stain_int_int,exitflag] = ...
        Determine_Coverage(currstain,WC_pxunits,MPStats(iMP).EquivDiameter,MPStats(iMP).edgecoor_sph_wcatorigin(:,1),...
        MPStats(iMP).edgecoor_sph_wcatorigin(:,2),[MPStats(iMP).PixelSizeXY MPStats(iMP).PixelSizeZ],...
        'R',MPStats(iMP).edgecoor_sph_wcatorigin(:,3),'radialinterval', radialmargin, 'GaussianSignal', gaussiansignal);

    if exitflag == -1
        boundaryissues = [boundaryissues iMP]; %#ok<AGROW>
    end      
    
    MPStats(iMP).([stain_name '_coor_cart'])      = stain_peak_coor{1}(1:3,:)'; % Stain coordinates
    MPStats(iMP).([stain_name '_coor_sph'])       = stain_peak_coor{1}(4:6,:)';
    MPStats(iMP).(['IM' stain_name '_bgcorr'])    = single(currstain);          % Stain image, background corrected
    MPStats(iMP).([stain_name '_int'])            = stain_int{:};               % Stain intensity across edge
    MPStats(iMP).([stain_name 'width'])           = stain_width{:};             % Stain width
    MPStats(iMP).([stain_name '_int_integrated']) = stain_int_int{:};           % Integrated stain intensity across edge
end

if ~isempty(boundaryissues)
    warning(['Particle(s) ' num2str(boundaryissues) ' close to boundary.'...
            newline 'This may give issues when trying to use integrated intensity for masking.'...
            ' You can try: ' newline '- reducing the size of the search margin'...
            newline '- excluding the particle(s) that are problematic: MPStats([' num2str(boundaryissues) ']) = []'...
            newline '- using max intensity instead of integrated intensity for masking'])
end

[MPStats.(['IM' stain_name])]                   = convert_structfield({MPStats.(['IM' stain_name])},'single');
[MPStats.(['IM' stain_name '_isgaussian'])]     = deal(gaussiansignal);
[MPStats.(['IM' stain_name '_radialinterval'])] = deal(radialmargin  );

MPStats = orderfields(MPStats);

function[selectedchannel] = UI_Select_Channel(data,channelcolors,objectname)
% Let user choose the channel with a given object. 

Nchannels = length(data);
if ~exist('objectname','var');     objectname = 'particle';        end
if isempty(objectname)       ;     objectname = 'particle';        end

if Nchannels > 1
    
    fh = figure();
    set(fh,'Name',['Select (click on) the image with the ' objectname])
    
    % show an image for each channel
    for ichannel = 1:Nchannels % Create a figure with subplots for each image
        
        subplot(1,Nchannels,ichannel)
        im = imagesc(data{ichannel,1});
        set(im,'PickableParts','none'); 
        cmap = colormap('gray');
        
        if isempty(channelcolors)
            colormap(cmap);
        else
            colormap(cmap*channelcolors{ichannel});
        end
        
        axis image
        axis tight
        set(gca,'Visible','off','PickableParts','all')
        
    end
    
    MPaxes    = Select_subplot_GUI('figure',fh); % Let the user pick a figure
    selectedchannel = find(flipud(fh.Children == MPaxes)); % find the corresponding channel number
    delete(fh);
    
else
    
    selectedchannel = 1;
    
end

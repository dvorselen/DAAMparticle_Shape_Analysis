function [totalArea,totalVolume,Sphericity,volumeWcentroid,AreaWcentroid] = Area_Volume_trisurf(TR)
% Given a surface triangulation, compute the area, volume, sphericity and centroid
% Assumption:Triangle nodes are ordered correctly, i.e.,computed normal is outwards

p = TR.Points';
t = TR.ConnectivityList';
       
% Calculate the area, first determine the vectors from one vertex and their crossproduct
d13= [(p(1,t(2,:))-p(1,t(3,:))); (p(2,t(2,:))-p(2,t(3,:)));  (p(3,t(2,:))-p(3,t(3,:)))];
d12= [(p(1,t(1,:))-p(1,t(2,:))); (p(2,t(1,:))-p(2,t(2,:))); (p(3,t(1,:))-p(3,t(2,:)))];
cr = cross(d13,d12,1);
totalArea = sum(0.5*rssq(cr,1)); % sum of root sum of squares
if nargout > 4
    AreaWcentroid = sum(squeeze(mean(reshape([p(1,t) p(2,t) p(3,t)],3,[],3),1)).*rssq(cr,1)')/sum(rssq(cr,1))';
end
    
% Calculate volume by splitting the particle up in tetrahedrons
volume      = dot(p(:,(t(1,:))),cross(p(:,(t(3,:))),p(:,(t(2,:))))) / 6;
totalVolume = abs(sum(volume));

if nargout > 2  % Calculate Sphericity
    Sphericity = pi^(1/3)*(6*totalVolume)^(2/3)/totalArea;
end

if nargout > 3 % Determine Centroid   
    centroid  = (p(:,(t(1,:)))+p(:,(t(2,:)))+p(:,(t(3,:)))) / 4;
    volumeWcentroid = -sum(centroid.*volume,2)/totalVolume;
end

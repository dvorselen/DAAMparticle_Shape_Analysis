function[TRI,TRI_nosphbound] = Triangulate_spherical_object(theta,phi,reggrid,varargin)

if nargin < 4 
    mode = 'convhull';
else
    mode = varargin{1};
    if ~any(strcmp(mode,{'convhull','unwrap'}))
        error('mode should be either "convhull" or "unwrap"')
    end
end

% for non-spherical boundaries triangulation is straight-forward
TRI_nosphbound = delaunay(theta,phi);

if strcmp(mode,'convhull')
    
    [x,y,z] = sph2cart(theta,phi,ones(length(theta),1));
    K = convhulln([x,y,z]);
    TRI = [K(:,1) K(:,3) K(:,2)];
    
elseif strcmp(mode,'unwrap')
    
    if reggrid 

        % Copy the points at the edges to create spherical boundaries
        thetaedge = (theta == -pi);
        thetaSPHbound = vertcat(theta, ones(sum(thetaedge),1)*pi);
        phiSPHbound = vertcat(phi, phi(thetaedge));

        % Triangulate in 2D
        TRI = delaunay(thetaSPHbound,phiSPHbound);

        % Exchange copied indexes for their original
        toReplace   = find( thetaSPHbound == pi );
        Replacewith = find( thetaedge );
        for i = 1:length(toReplace)
            TRI(TRI == toReplace(i)) = Replacewith(i);
        end

    else

        % Determine the number of points to copy around the edges
        Ntotal  = length(theta);
        % We take the maximum from 5% of the points (for large particles) and
        % the sqrt of Ntotal (for small particles)
        NatEdge = max(0.05*Ntotal,ceil(sqrt(Ntotal)));
        % We let the number of values at the apex scale with sqrt(Ntotal)
        NatApex = max(2,floor(sqrt(Ntotal)/6));
        if mod(NatApex,2);  NatApex = NatApex -1;   end

        % Sort the angles to determine the point close to the edges 
        [~,thetasortidx] = sort(theta);
        [~,phisortidx]   = sort(phi);
        Edge1 = 1:NatEdge;
        Edge2 = Ntotal:-1:(Ntotal-NatEdge+1);

        % Wrap the data to make triangulation at the edges work well:
        % Copy N points (NatEdge) close to theta -2pi and 2 pi at the other side 
        % To guarantee full triangulation at the apex, copy points at max and min phi along the apex
        thetaSPHbound = vertcat(theta, theta(thetasortidx(Edge1 ))+2*pi ,...
            theta(thetasortidx(Edge2))-2*pi, linspace(-pi,pi,NatApex)',...
            linspace(-pi,pi,NatApex)');
        phiSPHbound = vertcat(phi, phi(thetasortidx(Edge1 )) ,...
            phi(thetasortidx(Edge2)), ones(NatApex,1)*pi-phi(phisortidx(end)),...
            ones(NatApex,1)*-pi-phi(phisortidx(1))) ;

        % Triangulate in 2D
        TRI = delaunay(thetaSPHbound,phiSPHbound);

        % Remove entries with only copied points
        TRI(all(TRI'>Ntotal),:) = [];
        % Remove entries in between the apex points copied along phi = +- pi
        top_and_bottom = find(abs(phiSPHbound)==(.5*pi));
        TRI(sum(ismember(TRI,top_and_bottom),2)==2,:) = [];

        % Find copied (mirrored, spherical boundary) indexes and the corresponding original index
        IDX_extrapoints = Ntotal+1:length(thetaSPHbound);
        IDXconv = [thetasortidx(Edge1)' thetasortidx(Edge2)'...
            repmat(phisortidx(end),1,NatApex), repmat(phisortidx(1),1,NatApex)];
        % Exchange copied indexes for their original
        for i = 1:length(IDX_extrapoints)
            TRI(TRI == IDX_extrapoints(i)) = IDXconv(i);
        end

        % Remove repeated rows
        [~,uqidx] = unique(sort(TRI,2),'rows','stable'); % We sort, since indices can appear in different order
        TRI = TRI(uqidx,:);
        % Remove rows with repeated values (if any)
        TRI(TRI(:,1)==TRI(:,2) | TRI(:,1) == TRI(:,3)  | TRI(:,2) == TRI(:,3),:) = [];

    end
end


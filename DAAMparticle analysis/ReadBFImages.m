function[data,Nfiles,FileName,PathName,xtra_metadata] = ReadBFImages(format,varargin)

% if no file format is specified, any format can be loaded
if nargin == 0
    extension = '*.*';
elseif strcmp(format(1),'.')
    extension = ['*' format];
else
    extension = ['*.' format]; 
end

askuserforfilenames = 1;
if nargin == 3 && iscell(varargin{1}) && iscell(varargin{2})
    if ischar([varargin{1}{:} varargin{2}{:}])
        askuserforfilenames = 0;
        PathName = varargin{1};
        FileName = varargin{2};
    end
end

% Open UI to select the data files
if askuserforfilenames
    [PathName,FileName] = uigetMultiFilesandFolders(fullfile(fileparts(cd)),extension,'file');
    if isempty(PathName)
        warning('Operation canceled by user')
        [data,Nfiles,xtra_metadata] = deal([]);
        return;
    end
end

% Setup output parms
Nfiles = length(FileName);
data = cell(Nfiles,4);
readnext = 0;

% save the current working directory, such that we can return after running
% this function
currpath = cd;

% Create a cell for extra metadata in case there is a seperate files  
xtra_metadata = cell(Nfiles,4);
checkbf_location

% Read the filesMultiple files can be read at once by bfopen (for example: multiple
% positions acquired in one go) We keep track of that by counting the number of files 
% read, and determining if the next file should be read. 
for ifile = 1:Nfiles 

    if readnext == 0

        periodIDX = strfind(FileName{ifile},'.');

        [~,currdata] = evalc('bfopen([PathName{ifile} FileName{ifile}],1)');
        if exist([PathName{ifile} FileName{ifile}(1:periodIDX(1)-1) '_metadata.txt'],'file') && nargout > 4
            [~,xtra_metadata(ifile,:)] = evalc('bfopen([PathName{ifile} FileName{ifile}(1:periodIDX(1)-1) ''_metadata.txt''])');
        end
        Ndataread = size(currdata,1)-1;
        data(ifile:ifile+Ndataread,:) = currdata;
        readnext = readnext + Ndataread;
    else
        readnext = readnext - 1;
    end

end

cd(currpath);

%% make sure to run bfopen from the correct location

    function[] = checkbf_location()

    bfopens = which('bfopen','-all');
    if iscell(bfopens)
        inbfmatlabfolder = contains(bfopens,'bfmatlab');
        rightfolder = fileparts(bfopens{inbfmatlabfolder});
        cd(rightfolder) 
    end
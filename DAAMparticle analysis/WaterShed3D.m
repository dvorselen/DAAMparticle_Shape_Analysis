function[lm3D] = WaterShed3D(bw3D,varargin)
% This function uses the watershed transform and makes a 3D label matrix,
% it can downsample the orignal image for speed

% Check function input and set default parameters
DefStruct = struct('rescale',.3,'smooth',2);
Args = parseArgs(varargin,DefStruct,[]);

bw3Dshrunk = ceil(imresize3(int8(bw3D),Args.rescale)); % watershed is computationally intensive, so we downsample the image

usedoriginal = 0;
if any(size(bw3Dshrunk) == 1)
    warning(['Resizing image for watershed failed. This is likely due to an error in '...
        'pixel size calibration. Continued working with original image.'])
    bw3Dshrunk = int8(bw3D);
    usedoriginal = 1;
end

D = -bwdist(~bw3Dshrunk);
D = imgaussfilt3(D,Args.smooth);
D(~bw3Dshrunk) = Inf;
lm3D = watershed(D);
if ~usedoriginal
    lm3D = imresize3(lm3D,size(bw3D),'nearest');
end
lm3D(~bw3D) = 0;   


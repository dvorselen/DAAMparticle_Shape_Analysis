function[IM3D,replaceorkeep] = Negative_Value_Check(IM3D,replaceorkeep)

if any(IM3D(:)<0)

    if isempty(replaceorkeep)
        replaceorkeep = questdlg(['Negative valued pixels are present. This is expected ',...
            'for deconvolved data, but otherwise likely indicate corrupted pixel values. ' newline newline ,...
            'Do you want to: ' newline ' -replace them (''replace''; recommended for non-deconvolved data)',...
            ', or ' newline ' -keep them (''keep'', recommended for deconvolved data)'], 'Negative valued pixels',...
            'keep','replace','keep');
    end

    if strcmp(replaceorkeep,'replace')   
        idx = find(IM3D<0);
        [ridx,cidx,pidx] = ind2sub(size(IM3D),idx);
        for ichannel = 1:length(idx)
            ridxi = ridx(ichannel)+[-1,0,1];
            cidxi = cidx(ichannel)+[-1,0,1];
            pidxi = pidx(ichannel)+[-1,0,1];
            ridxi = ridxi(ridxi>0 & ridxi<size(IM3D,1));
            cidxi = cidxi(cidxi>0 & cidxi<size(IM3D,2));
            pidxi = pidxi(pidxi>0 & pidxi<size(IM3D,3));
            neighbourhood = IM3D(ridxi,cidxi,pidxi);
            IM3D(idx(ichannel)) = median(neighbourhood(:)); 

        end
    else
        % If we keep negative values we add background to make sure all
        % pixel values are positive for thresholding (during which we
        % calculate the logarithm of the pixel values)
        IM3D = IM3D - min(IM3D) + 1;
    end

end
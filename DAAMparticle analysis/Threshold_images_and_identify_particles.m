function[Stats] = Threshold_images_and_identify_particles(Stats,Opts)

if nargin == 1;         Opts = {};          end

% Add empty fields to MPStats (such that they can be filled in a loop)
[Stats.IMlm,Stats.CCs,Stats.BackgroundInt,Stats.Centroidzm,...
    Stats.Volume,Stats.EquivDiameter,Stats.PixelIdxList] = deal([]);

increasethreshold = strcmp(Opts,'UseIncreasedThreshold');
if any(increasethreshold)
    increasethresholdamount = Opts{[false increasethreshold(1:end-1)]};
    if increasethresholdamount ~= 0
        increasethreshold = 1;
    else
        increasethreshold = 0;
    end
end

allow_multiple_particles = 0;
user_input_multiple_particles = strcmp(Opts,'multipleparticles');
if any(user_input_multiple_particles)        
    if strcmpi(Opts{[false user_input_multiple_particles(1:end-1)]},'ok')
        allow_multiple_particles = 1;
    end
end

for i = 1:length(Stats)
    currIM_smooth = imgaussfilt3(Stats(i).IM3D,2);
    if isempty(Stats(i).LLSMMask)
        currIMpixvalues = currIM_smooth(:);
    else
        currIMpixvalues = currIM_smooth(Stats(i).LLSMMask);
    end
    
    [threshold,backgroundint,peakint] = Find_logIntensity_threshold(currIMpixvalues,'iteration',i,'useksdensity',1); 
    if increasethreshold       
        threshold = threshold + (peakint-threshold)*increasethresholdamount;
    end
        
    currBW = currIM_smooth>threshold;
    [lm,CCs,CCstats] = Identify_particles(currBW,[Stats(i).PixelSizeXY Stats(i).PixelSizeZ],'particleID',num2str(i),...
        Opts{:},'RescaleFactor',min(round(Stats(i).PixelSizeXY/0.3,2,'significant'),1));    
    
    % Check for and remove over saturated particles
    for icc = CCs.NumObjects:-1:1
        pxvaluescc = Stats(i).IM3D(CCs.PixelIdxList{icc});
        if sum(pxvaluescc == max(pxvaluescc))/length(pxvaluescc)>0.1    %>100
            CCs.PixelIdxList(icc) = []                ;
            CCs.NumObjects        = CCs.NumObjects - 1;
            lm(lm==icc)           = 0                 ;
            lm(lm>icc)            = lm(lm>icc) -1     ;           
            CCstats(icc)          = []                ;
        end
    end
           
    if allow_multiple_particles       
        Stats(i).Centroid   = {CCstats.Centroid};
    else
        nparticles = length(CCstats);
        if nparticles > 1
            % select particle closest to the previous particle location
            if i == 1
                refpos = size(Stats(i).IM3D)/2; % or the image centroid for the first image
            else
                refpos = Stats(i-1).Centroidzm;
            end
            [~,minloc] = min(rssq(reshape([CCstats.Centroid]',3,nparticles)'- refpos,2));
            CCs.PixelIdxList = CCs.PixelIdxList(minloc);
            CCs.NumObjects   = 1;
            CCstats = CCstats(minloc);
        end
        Stats(i).Centroidzm = [CCstats.Centroid];
    end

    % Fill out properties into MPStats structure
    Stats(i).IMlm           = lm;   % Labelmatrix
    Stats(i).CCs            = CCs;
    Stats(i).BackgroundInt  = backgroundint;
    Stats(i).PixelIdxList   = CCs.PixelIdxList;
    Stats(i).Volume         = [CCstats.Volume];
    Stats(i).EquivDiameter  = [CCstats.EquivDiameter];
    
end

Stats = orderfields(Stats);

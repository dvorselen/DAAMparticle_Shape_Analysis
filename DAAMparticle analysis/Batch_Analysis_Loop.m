% Batch_Analysis_Loop

C = whos;

if ~isempty(C)
    
    Nfiles = length(FileName);
    wbh = waitbar(0,['Analyzing your images: 0/ ' num2str(Nfiles)]);
    Opts = {'noparticlesOK',1,'multipleparticlesOK',1,'watershed',1,'checkneighbours',1,'SphericityTreshold',0.9};

    % Start logging
    logstr = [datestr(now) ':    Started with the following settings:' newline...
        'Deconvolution: ' num2str(perform_deconvolution) newline 'Z-correction factor: ' num2str(zcorrfactor) newline...
        'Negative value pixels: ' replaceorkeep newline 'Grid: ' userinput{2} ', with spacing ' userinput{1} newline...
        'Smooth (pixelsize): ' num2str(smoothpixelsize) newline 'Smooth stain (pixelsize): ' num2str(stainoptions(2)) newline...
        'Max or intergrated stain: ' userinputstain{1} newline 'Radial margin stain: ' num2str(stainoptions(1)) newline...
        'Stain indicates contact: ' userinputstain{2} newline newline];

    propertyTable = table([],[],[],[],[],'VariableNames',{'Wrapping','Radius','Sphericity','Image','Index'});

    ibatch         = 1;
    batchsize      = str2double(inputdlg('How many images do you want to read in per batch?','Batchsize',[1 70],{'3'}));
    global_stain_threshold = [];

% This part is used when matlab is restarted for memory issues    
else
    
    % read back in user parameters and current progress
    load temp.mat;    delete temp.mat
      
    % Reload java classes
    if perform_deconvolution
        if ~exist('DL2','class')  % Check if deconvolutionlab2 can be found by matlab
            javaaddpath([matlabroot filesep 'java' filesep 'jar' filesep 'DeconvolutionLab_2.jar'])
        end
    end
    wbh = waitbar(max(idx)/Nfiles,['Analyzing your images: ' num2str(max(idx)) '/' num2str(Nfiles)]);
    
end

% Create outputfolder
if ~isfolder(fullfile(OutputFolder,'MATfiles'));    mkdir(OutputFolder,'MATfiles');     end

while ibatch <= ceil(Nfiles/batchsize)

    try 
        idx = ibatch*batchsize+((-batchsize+1):0);
        idx = idx(idx<=Nfiles);
        % Read the data files
        data = ReadBFImages('.tif',PathName(idx),FileName(idx));

        % Populate IMStats with metadata
        dummycell = cell(numel(idx),1);
        IMStats = struct('PathName',PathName(idx),'FileName',FileName(idx),...
            'PixelSizeXY',VoxelSize(1),'PixelSizeZ',VoxelSize(2)/zcorrfactor,'PixelSizeZ_original',VoxelSize(2),...
            'NChannels',NChannels,'SlicesFirst',SlicesFirst,'ChannelNumber',dummycell,...
            'zcorrfactor',zcorrfactor,'IM3D',dummycell,'ChannelColors',dummycell,'MPchannel',MPchannel,...
            'NSlices',num2cell(int16(cellfun(@length,data(:,1)))/NChannels));

        [IM3Dcell,LLSMMask] = Extract_Images(data,IMStats,MPchannel,'zcorrfactor',zcorrfactor);
        [IMStats.IM3D]      = IM3Dcell{:};      clear IM3Dcell
        [IMStats.LLSMMask]  = LLSMMask{:};      clear LLSMMask

        % Threshold the images, watershed and identify particles
        IMStats = Threshold_images_and_identify_particles(IMStats,Opts);

        % Isolate particles and create a particle-based structure
        MPStats = IMStats_to_MPStats(IMStats);
        
        if ~isempty(MPStats)
            MPStats = CropParticles(MPStats,IMStats,[4 5 10]);

            % Deconvolve
            if perform_deconvolution
                % Deconvolution is done using deconvolutionlab:
                % see http://bigwww.epfl.ch/deconvolution/deconvolutionlab2/
                PSF_Preparation
                IMs_deconv = Deconvolve_using_DeconvolutionLab2({MPStats.IM_bgcorr},{MPStats.PSF3D});
                [MPStats.IM3D_nondeconv] = convert_structfield({MPStats.IM3D},'single');
                [MPStats.IM3D          ] = IMs_deconv{:};    clear IMs_deconv
            end

            % Detect edges using the 3D sobel operator and superlocalize edges using gaussian fitting
            XYZedges = Apply_3DSobel_filter({MPStats.IM3D},smoothpixelsize./[1 1 MPStats(1).PixelSizeZ/MPStats(1).PixelSizeXY]);
            [MPStats.IM3D   ] = convert_structfield({MPStats.IM3D},'single');            
            [MPStats.IMedges] = XYZedges{:};    clear XYZedges

            % Superlocalize edges using gaussian fitting
            [MPStats,Residuals_Problems_excluded] = ...
                Superlocalize_edges_using_gaussian_fitting(MPStats,str2double(userinput{1}),userinput{2},'usePPM',0,'verbose',0);

            % Triangulate surface and determine particle statistics
            MPStats = Triangulate_surface_and_determine_particle_statistics(MPStats);

            % Determine particle coverage by a secondary signal    
            [MPStats,IMStats] = Analyze_Secondary_Signal(data,MPStats,IMStats,Stain_channel,'stain',[],stainoptions);

            % determine particle coverage by additional signals
            if ~isempty(Stain_channel2)
                [MPStats,IMStats] = Analyze_Secondary_Signal(data,MPStats,IMStats,Stain_channel2,'stain',1,stainoptions);
            end
            clear data 
            
            % Convert secondary signal to mask, and determine the position of the base of the cup
            if isempty(Stain_channel2) || ~use_both_stains
                [MPStats,global_stain_threshold] = Convert_secondary_signal_to_mask(MPStats,stain_indicates_contact,...
                    'globalthreshold',global_stain_threshold,'use_integrated_intensity',use_integrated_intensity);
            else
                fnames   = fieldnames(MPStats);
                adchname = regexp(fnames,'stain_ch\d_int','match');
                adchname = [adchname{:}];
                if stain_indicates_contact
                    [MPStats,global_stain_threshold] = Convert_double_signal_to_mask({MPStats.(adchname{1})},{MPStats.stain_int},MPStats,...
                        'globalthreshold',global_stain_threshold,'use_integrated_intensity',use_integrated_intensity);
                else
                    [MPStats,global_stain_threshold] = Convert_double_signal_to_mask({MPStats.stain_int},{MPStats.(adchname{1})},MPStats,...
                        'globalthreshold',global_stain_threshold,'use_integrated_intensity',use_integrated_intensity);
                end
            end

            % Determine the position of the base and align cups
            MPStats = Determine_base_position_and_align(MPStats);
            
            % Calculate curvatures 
            [PC,GC,MC] = Smooth_and_Get_Curvatures(MPStats,1);
            [MPStats.Curvature_principals] = PC{:};
            [MPStats.Curvature_gaussian]   = GC{:};
            [MPStats.Curvature_mean]       = MC{:};
            clear('PC','GC','MC')

        end
        
        % Save IMStats
        [~,startfilename] = fileparts(FileName{idx(1)});
        [IMStats.IM3D] = convert_structfield({IMStats.IM3D},'single');
        save(fullfile(OutputFolder,'MATfiles',['IMStats_' startfilename '.mat']),'IMStats','-v7.3')
        
        % Save all other output (figs, statistics etc.) 
        if ~isempty(MPStats)   
            
            % Save MPStats (per image, such that it is easier to reload data later)
            MPStats    = orderfields(MPStats);
            [~,uqidx]  = unique([MPStats.IMnumber]);
            uqidx      = [uqidx; length(MPStats)+1];  %#ok<AGROW>
            AllMPStats = MPStats;
            for i = 1:length(uqidx)-1 % Loop through the individual images
                MPStats           = AllMPStats(uqidx(i):uqidx(i+1)-1);
                [~,startfilename] = fileparts(MPStats(1).FileName);
                save(fullfile(OutputFolder,'MATfiles',['MPStats_' startfilename '.mat']),'MPStats','-v7.3')
            end
            MPStats = AllMPStats;            clear('AllMPStats','uqidx','i')
         
            % Save the coordinates, triangulation and mask
            Save_Coordinates_Triangulation_Mask(MPStats,OutputFolder,1)
            
            % Get some of the most important properties in a table
            propertyTablei = table('Size',[length(MPStats) 5],'VariableTypes',{'single','single','single','string','uint8'},'VariableNames',{'Wrapping','Radius','Sphericity','Image','Index'});
            propertyTablei.Wrapping   = vertcat(MPStats.Fraction_engulfed)  ;
            propertyTablei.Radius     = vertcat(MPStats.Equiv_Diameter_um)/2;
            propertyTablei.Sphericity = vertcat(MPStats.Sphericity)         ;
            propertyTablei.Image      = {MPStats.FileName}'                 ;
            propertyTablei.Index      = (1:length(MPStats))'                ;
            propertyTable = [propertyTable;propertyTablei]; %#ok<AGROW>
            
            % Save plots
            Plots_batchmode(MPStats,IMStats,fullfile(OutputFolder,'Figures'),userinputstain(1),{'jpeg'})
            
        end
        
    catch ME % In case we encounter an error
        
        % If not a memory related error, we log the error message, and continue with the next batch of particles
        if ~contains(ME.message,'Java heap space')
            
            combFileNamestr = [FileName(idx), repmat({newline},length(idx),1)]';
            logstr = [logstr newline datestr(now) ' Error occurred in one of the following images: ' newline ...
                combFileNamestr{:} '    (image numbers ' num2str(idx) ')' newline...
                'These images have not been analyzed. The following error occurred: ' newline ME.message newline...
                'This error could be tracked to: ' ME.stack(1).name ', line: ' num2str(ME.stack(1).line) ',' newline...
                'which was called in: ' ME.stack(2).name ', line: ' num2str(ME.stack(2).line) newline];         %#ok<AGROW>
        
        % We can not continue once we encounter a heapspace error, which can occurr through a memoryleak in deconvolutionlab. 
        else 
             
            % This is a ridiculous solution, but I fail to clear the heapspace by amongst others:
            % "clear java" and "clear all". Instead, we will reopen matlab and rerun this script
            % in a new matlab instance, starting from our current loop iteration. We wil close this session           
            logstr = [logstr newline datestr(now) ' Heapspace error occurred in iteration: ' newline ...
                num2str(ibatch) '    (image numbers ' num2str(idx) ')' newline...
                'Started a new matlab instance and continued. ' newline];       %#ok<NASGU,AGROW>
            
            clear('MPStats','IMStats','PC','GC','MC','propertyTablei','dummycell','IM3Dcell','IM3Dsingle','LLSMMask','wbh')
            save temp.mat;  
            system([fullfile(matlabroot,'bin','matlab') ' -r Batch_Analysis_Loop & '])
            exit;
            
        end        
    end
    
    % Clean up workspace and move to next loop iteration
    clear('MPStats','IMStats')
    ibatch = ibatch +1;
    waitbar(max(idx)/Nfiles,wbh,['Analyzing your images: ' num2str(max(idx)) '/' num2str(Nfiles)]);
    
end

% Save property table
if ~isempty(propertyTable)
    writetable(propertyTable,fullfile(OutputFolder,'Particle properties.csv'))
end

% Save log file
logstr = [logstr newline datestr(now) ':    Finished'];
fid = fopen(fullfile(OutputFolder, 'LogFile.txt'), 'w');
if fid == -1;   warning('Cannot write log file.');        end
fprintf(fid,'%s', logstr);
fclose(fid);

% Clean up workspace
delete(wbh);
clear('logstr','fid','propertyTablei','PropertyTable','wbh','batchsize','dummycell','ibatch','idx','ME','uqidx')

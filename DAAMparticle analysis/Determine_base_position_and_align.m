function[MPStats] = Determine_base_position_and_align(MPStats,varargin)

% Check user input and otherwise use default method
DefStruct = struct('BaseLat',0,'BaseColongitude',-pi/2);
Args = parseArgs(varargin,DefStruct,[]);

ThetaPhi  = cellfun(@(x) x(:,[1 2]),{MPStats.edgecoor_sph_wcatorigin},'UniformOutput',false);

% The centroid of the contact area is taken
[~,~,~,theta_base,phi_base] = Determine_Centroid(ThetaPhi,{MPStats.isincontact});

[ThetaRot,PhiRot] = Align_spheres_by_Rotation(ThetaPhi,double([theta_base phi_base-Args.BaseLat]));
% Note that it is still possible to rotate around the base - opposite apex axis

NMPs = length(MPStats);

% Redo triangulation
for iMP = 1:NMPs 

    if all(MPStats(iMP).isincontact) || ~any(MPStats(iMP).isincontact)
        continue;
    end
    
    % Determine theta, phi, R
    theta     = wrapToPi(ThetaRot{iMP}+Args.BaseColongitude); % We rotate theta away from the center for visualization on 2D maps
    phi       = PhiRot{iMP};
    r         = MPStats(iMP).edgecoor_sph_wcatorigin(:,3);
       
    MPStats(iMP).edgecoor_sph_aligned     = [theta,phi,r]      ;
    MPStats(iMP).TRI_Connectivity_aligned = delaunay(theta,phi);
    
    [x,y,z] = sph2cart(theta,phi,r);
    MPStats(iMP).edgecoor_cart_aligned = [x,y,z];
    
    % Also save the location of the cupbase
    [~,minloc] = min(pdist2([theta,phi],[.5*pi,0]));
    r_base = r(minloc);
    MPStats(iMP).CupBase = [theta_base(iMP,:) phi_base(iMP,:) r_base];
    
end

MPStats = orderfields(MPStats);

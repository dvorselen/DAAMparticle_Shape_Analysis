function[aH] =  Select_subplot_GUI(varargin)
% This GUI allows you to select a subplot of a figure.

% OPTIONAL INPUT:
% figure: if not defined, the GUI will allow you to select a figure

% OUTPUT:
% aH    : handle to the right axes

% parse args
DefStruct = struct('figure',[]);
Args = parseArgs(varargin,DefStruct,[]);


if isempty(Args.figure)
    
    % Get all figures
    f = [];
    figs = get(0,'Children');

    % if pushed on a figure we will now select it
    set(figs,'ButtonDownFcn',@SelectFigFcn)

    % Tell the user to push enter when done and wait for enter
    disp('Push anywhere in the figure you want to select, then press enter')
    pause
else
    f = Args.figure;
end
    
% if pushed on an axes it will now be selected    
aHs = get(f,'Children');
set(aHs,'ButtonDownFcn',@SelectFcn)

% Tell the user to push enter when done and wait for enter
disp('Push anywhere on the image that you want to select')
waitforbuttonpress

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Function that makes the selected figure the current figure, displays
    % the figure number and releases the buttondownfcn
    function SelectFigFcn(varargin)
        f = gcf;
        if verLessThan('matlab','8.4')
            disp(['Selected figure ' num2str(f)])
        else
            disp(['Selected figure ' num2str(f.Number)])
        end
        set(figs,'ButtonDownFcn','')
    end

    % Function that makes the selected axes the current axes, displays
    % the axes number and releases the buttondownfcn
    function SelectFcn(varargin)
        aH = gca;
        disp('Selected an axes ')
        set(aHs,'ButtonDownFcn','')
    end

end
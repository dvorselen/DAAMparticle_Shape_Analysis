function[IM3D,llsmmask,isLLSMdata] = LLSM_Check(IM3D,isLLSMdata,varargin)

% Check function input and set default parameters
DefStruct = struct('knownmask',[]);
Args = parseArgs(varargin,DefStruct,[]);

if isempty(isLLSMdata)
    % Check the fraction of zeros, zeropadding is typical for LLSM data
    fraczeros = sum(IM3D(:)==0)/numel(IM3D);
    
    if fraczeros > .025
    % Check with the user if we are right
        isLLSMdata = questdlg(['There are many zero-valued pixels. ',...
            'Is this LLSM data and do you want to remove zero-padded pixels?'],...
            'zero-valued pixels','yes','no','yes');
    end

    isLLSMdata =  strcmp(isLLSMdata,'yes');
end
     
if isLLSMdata
    
    if ~isempty(Args.knownmask)  
        
        llsmmask = Args.knownmask;
        
    else
        
        IM3Dsize = size(IM3D);
        % We will clip the data and create a mask, which we can use to
        % prevent detecting the image edges during particle shape analysis
        llsmmask = IM3D>0;

        % Use the hough transform
        bwhough   = edge(squeeze(sum(llsmmask,1)))';
        [bwhought,Theta,Rho] = hough(bwhough);
        hpeaks    = houghpeaks(bwhought,4,'Threshold',0.2*max(bwhought(:)));
        hlines    = houghlines(bwhough,Theta,Rho,hpeaks);

        % Find the slope coordinates 
        slantedlines       = abs([hlines.theta])> 12 & abs([hlines.theta]) < 78;  
        % Remove intersecting lines
        hlinesslanted      = rem_is_lines(hlines(slantedlines));
        hlinesflat         = rem_is_lines(hlines(~slantedlines));

        % If more than 2 lines are detected, we use the longest ones
        if length(hlinesslanted)>2
            [~,sortidx] = sort(sum(abs(vertcat(hlinesslanted.point1)-vertcat(hlinesslanted.point2)),2),'descend');
            hlinesslanted(~ismember(sortidx,[1 2])) = [];
        elseif length(hlinesslanted) == 2 && length(hlinesflat) == 1
            [~,sortidx] = sort(sum(abs(vertcat(hlinesslanted.point1)-vertcat(hlinesslanted.point2)),2),'descend');
            hlinesslanted(sortidx~=1) =[];
        end
        slopeslantedlines  = 1/tand(mean([hlinesslanted.theta]));
        midpoints_x1y1x2y2 = mean([hlinesslanted.point1;hlinesslanted.point2]);     
        if length(hlinesslanted) == 2 % Case both sides are affected
            zerointersect      = -midpoints_x1y1x2y2([2 4]) + slopeslantedlines * -midpoints_x1y1x2y2([1 3]);
        else % Case 1 side is affected
            zerointersect      = -midpoints_x1y1x2y2(2) + slopeslantedlines * -midpoints_x1y1x2y2(1);
        end
        
        % We setup the new mask
        include_innewllsmmask = (1:IM3Dsize(2)) * -slopeslantedlines - zerointersect';
        if length(hlinesslanted) == 2
            if include_innewllsmmask(2,1) < include_innewllsmmask(1,1)
                include_innewllsmmask = flipud(include_innewllsmmask);
            end
        end
        linenumbers = repmat((1:IM3Dsize(3))',1,IM3Dsize(2));
        if length(hlinesslanted) == 2
            newmaskslice = (linenumbers>min(include_innewllsmmask) & linenumbers<max(include_innewllsmmask));
        else
            newmaskslice = linenumbers>include_innewllsmmask;
            if sum(newmaskslice(:))<0.5*numel(newmaskslice)
                newmaskslice = ~newmaskslice;
            end
        end
        llsmmask = permute(repmat(newmaskslice,1,1,IM3Dsize(1)),[3 2 1]);
        % And also clip off the top and bottom part of the image for the mask
        if length(hlinesflat) > 2
            [~,sortidx] = sort(sum(abs(vertcat(hlinesflat.point1)-vertcat(hlinesflat.point2)),2),'descend');
            hlinesflat(~ismember(sortidx,[1 2])) = [];
        end
        midpoints_flatlines = mean([hlinesflat.point1;hlinesflat.point2]);
        % Deal with the case that two lines are found but they are close together and
        if length(hlinesflat) == 2 && abs(midpoints_flatlines(2)-midpoints_flatlines(4)) < IM3Dsize(1)/3
            [~,sortidx] = sort(sum(abs(vertcat(hlinesflat.point1)-vertcat(hlinesflat.point2)),2),'descend');
            hlinesflat(sortidx~=1) =[];
        end
        if length(hlinesflat) == 2
            zrange = round([midpoints_flatlines(2):midpoints_flatlines(4) midpoints_flatlines(4):midpoints_flatlines(2)]);
        elseif isempty(hlinesflat)
            zrange = 1:IM3Dsize(3);
        elseif midpoints_flatlines(2)<(0.5*length(linenumbers))
            zrange = round(midpoints_flatlines(2):length(linenumbers));            
        else
            zrange = 1:midpoints_flatlines(2);
        end
        llsmmask(:,:,setdiff(1:IM3Dsize(3),zrange)) = 0;
    end

    % Also, we can crop some of the sides that only contains data at extreme Z's
    if length(hlinesslanted) == 2
        usefulpix_alongx = sum((squeeze(llsmmask(round(size(llsmmask,2)/2),:,:))'));        
        tocrop = usefulpix_alongx>(0.5*max(usefulpix_alongx));

        if any(tocrop == 0)
            IM3D           = IM3D(:,tocrop,:);
            llsmmask       = llsmmask(:,tocrop,:);
        end
    end
    
else

    llsmmask   = [];

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % This nested function removes intersecting lines and continuing lines
    function[hlines] = rem_is_lines(hlines)
    
        if numel(hlines) > 1
        
            p1xy = vertcat(hlines.point1);
            p2xy = vertcat(hlines.point2);
            a = (p2xy(:,2)-p1xy(:,2))./(p2xy(:,1)-p1xy(:,1));
            b = p1xy(:,2)-a.*p1xy(:,1);
            if any(isinf(a))
                a = (p2xy(:,1)-p1xy(:,1))./(p2xy(:,2)-p1xy(:,2));
                b = p1xy(:,1)-a.*p1xy(:,2);
            end
            xis = (b-b')./(a'-a);       %Determine intersects        yis = xis.*a+b;
            %Determine if intersect is in image
            is_in_image = triu(xis>0 & xis<size(bwhough,2),1); % & yis>0 & yis<size(bwhough,2),1);
            if any(is_in_image(:))
                
                [isp1,isp2] = ind2sub(length(p1xy)*ones(1,2),find(is_in_image));
                isp12 = [isp1 isp2];

                if ~isempty(isp12)
                    linelength = sqrt((p2xy(:,1)-p1xy(:,1)).^2+(p2xy(:,2)-p1xy(:,2)).^2);
                end

                todel = [];

                while ~isempty(isp12)
                    all_islines = unique(isp12);
                    [~,maxloc]  = max(linelength(all_islines));
                    isrows      = any(isp12 == all_islines(maxloc),2);
                    isstodel    = isp12(isrows,:);
                    todel       = [todel; isstodel(isstodel~=all_islines(maxloc))]; %#ok<AGROW>
                    isp12(isrows,:) = [];
                end
                
                 hlines(todel) = [];
                 a(todel) = [];
                 b(todel) = [];
                
            end

            
            %Also check for lines that are continuations of each other
            continuedlines = abs(1-(triu(mean(cat(3,a./a',b./b'),3))-diag(ones(length(hlines),1))))<0.02;
            
            while any(continuedlines(:))
                
                [cp1,cp2] = ind2sub(length(hlines)*ones(1,2),find(continuedlines,1,'first'));
                hlines(cp1).point1 = min([hlines(cp1).point1; hlines(cp2).point1]);
                hlines(cp1).point2 = max([hlines(cp1).point2; hlines(cp2).point2]);
                
                hlines(cp2) = [];
                a(cp2) = [];
                b(cp2) = [];
                
                continuedlines = abs(1-(triu(mean(cat(3,a./a',b./b'),3))-diag(ones(length(hlines),1))))<0.02;

            end
        
        end

    end


end


function[IM3Dcell,LLSMMask,zcorrfactor,Stats] = Extract_Images(data,Stats,channel,varargin)
% Extract the images from ometiff data on a per channel basis, creating a 3D matrix
% for each. Also can be used to resize the image (using interpolation) such
% that voxeldimensions are equal. Can also create a mask (and clip)
% deskewed LLSM data and performs a check for negative pixel values.

Nfiles    = length(data(:,1));

% Check function input and set default parameters
DefStruct = struct('equalize_voxeldims',0,'isLLSMdata',[],'zcorrfactor',[],'channelname','IM3D');
DefStruct.llsmmask = cell(1,Nfiles);
Args = parseArgs(varargin,DefStruct,[]);

if ~isempty(Args.zcorrfactor)
    zcorrfactor = Args.zcorrfactor;
end

% Set up output variables
IM3Dcell  = cell(1,Nfiles);
LLSMMask  = cell(1,Nfiles);
% Set loop variables
isLLSMdata    = Args.isLLSMdata;
replaceorkeep = [];

% Put the images in a structure for further processing
for ifile = 1:Nfiles
    
    if iscell(channel)
        channeli = channel{ifile};
    else
        channeli = channel;
    end
    
    % Take the MP image from the cell array data. 
    if Stats(ifile).SlicesFirst
        Nslices = Stats(ifile).NSlices;
        firstSlice = Nslices*(channeli-1)+1;
        IM3D = cat(3,data{ifile}{firstSlice:(firstSlice+Nslices-1),1});
    else
        firstSlice = channeli;
        IM3D = cat(3,data{ifile}{firstSlice:Stats(ifile).NChannels:end,1});
    end

    IM3D = double(IM3D);

    if ~exist('zcorrfactor','var')
        zcorrfactor = str2double(inputdlg(['Refractive index mismatch between immersion fluid and sample ',...
                'can cause apparent elongation along the z-direction' newline ...
                '(see methods section in https://www.nature.com/articles/s41467-019-13804-z for how to estimate)'...
                newline newline 'Please enter the measured/estimated correction factor'],'Z-correction factor',[1 120],{'1'}));
    end
    
    if Args.equalize_voxeldims
        % Make pixelsize same in all dimensions
        IM3Dsize = size(IM3D);
        if isfield(Stats,'PixelSizeZ_original')
            newZsize = round(IM3Dsize(3) * Stats(ifile).PixelSizeZ_original / Stats(ifile).PixelSizeXY / zcorrfactor);
        else
            newZsize = round(IM3Dsize(3) * Stats(ifile).PixelSizeZ / Stats(ifile).PixelSizeXY / zcorrfactor);
        end
        % Calibrate X,Y vs Z
        IM3D = imresize3(IM3D,[IM3Dsize([1 2]) newZsize]);
    end
        
    % Some types of data are zeropadded (e.g. LLSM data). We remove many of
    % the padded zeros to speed up the analysis
    if nargout > 1
        [IM3D,llsmmask,isLLSMdata] = LLSM_Check(IM3D,isLLSMdata,'knownmask',Args.llsmmask{ifile});
        if isLLSMdata ;     LLSMMask{ifile} = llsmmask;      end
    end
    
    % In rare cases some negative pixel values show up, we throw them out
    [IM3D,replaceorkeep] = Negative_Value_Check(IM3D,replaceorkeep);   
    
    IM3Dcell{ifile} = IM3D;
    
    if nargout > 3
        Stats(ifile).(Args.channelname) = IM3D;
        Stats(ifile).LLSMMask                     = llsmmask;
        Stats(ifile).zcorrfactor                  = zcorrfactor;
    end
    
end
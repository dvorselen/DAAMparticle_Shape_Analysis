function[lm,CCs,CCstats] = Identify_particles(bw,VoxelSize,varargin)    
% Use a thresholded image to keep only particles appropriate for analysis,
% i.e. particles that are large enough, not on the image edge, and not
% touching another particle. Also get some basic particle statistic
% estimates.

% Check function input and set default parameters
DefStruct = struct('minradius',2,'noparticlesOK',0,'multipleparticles','OK','watershed',0,'RescaleFactor',0.3,...
    'checkneighbours',0','particleID','??','UseIncreasedThreshold',[],'SphericityTreshold',0,'SmoothingSD',0.2./[VoxelSize(1) VoxelSize(1) VoxelSize(2)/2]);
Args = parseArgs(varargin,DefStruct,[]);

if Args.watershed
    LMorBC = WaterShed3D(bw,'rescale',Args.RescaleFactor,'smooth',Args.SmoothingSD);
else
    LMorBC = bw;
end
    
CCs = bwconncomp(LMorBC);

if CCs.NumObjects > 0
    
    % Identify small particles 
    V = cellfun(@length,CCs.PixelIdxList);
    small_volume_idx = V<round((4*Args.minradius^3)/(VoxelSize(1).^2*VoxelSize(2)));

    % Exclude particles that are so close together that the distance between 
    % their centroids is smaller than the summed radii (+ 1 um for safety)
    if Args.checkneighbours
        rprops = regionprops(CCs,'Centroid'); % Get centroid
        pweudist = squareform(pdist(vertcat(rprops.Centroid).*repmat([VoxelSize(1) VoxelSize(1) VoxelSize(2)],CCs.NumObjects,1)));   % calculate pairwise distances
        Radii = ((3/4/pi)*cellfun(@length,CCs.PixelIdxList)*(VoxelSize(1)^2*VoxelSize(2))).^(1/3);     % Calculate particle radii
        exclude = (pweudist-1) < repmat(Radii',1,CCs.NumObjects)+repmat(Radii,CCs.NumObjects,1);
        exclude(1:(CCs.NumObjects+1):end) = 0;
        tooclosetoneighbour_idx = logical(sum(exclude));
    else
        tooclosetoneighbour_idx = zeros(1,length(CCs));
    end

    if Args.SphericityTreshold  > 0
        SAprop = regionprops3(CCs,'SurfaceArea');
        Sphericity = pi^(1/3)*(6*V).^(2/3)./SAprop.SurfaceArea'; % Calculate sphericity
        b = VoxelSize(1)/VoxelSize(2);  % Correct sphericity for voxelsize
        pxsizecorrection = 2*b^(2/3)/(1+b^2/sqrt(1-b^2)*log(1+sqrt(1-b^2)/b));
        toodeformed = Sphericity/pxsizecorrection < Args.SphericityTreshold;
    else 
        toodeformed = false(1,length(CCs));
    end
    
    bw_bc = imclearborder(LMorBC);
    onborder = ~cellfun(@(x) all(bw_bc(x)),CCs.PixelIdxList);
    
    % Here, we exclude both small particles and those too close to their neighbours
    exclude = small_volume_idx | tooclosetoneighbour_idx | onborder | toodeformed;
    CCs.PixelIdxList(exclude) = [];
    CCs.NumObjects = CCs.NumObjects - sum(exclude);
end

if CCs.NumObjects == 0 && ~Args.noparticlesOK

    figure()
    imshow(bw(:,:,round(end/2)))
    error(['No particles found that are fully contained within the image (not touching any'...
        ' borders or neighbours) for particle/image ' Args.particleID '.']) 

end

if CCs.NumObjects > 1 && ~strcmpi(Args.multipleparticles,'ok')

    % If the user desired only one particle but multiple are detected let
    % the user choose or pick the most central one
    lm = labelmatrix(CCs);
    multipleMPstats = regionprops3(CCs,'Centroid');
    if strcmpi(Args.multipleparticles,'choose')
        fh = figure; imagesc(max(lm,[],3))
        for i = 1:CCs.NumObjects
            text(multipleMPstats.Centroid(i,1),multipleMPstats.Centroid(i,2),num2str(i),'Color','r','FontSize',24)
        end
        warning(['Multiple particles present for particle/image ' num2str(i)])
        selectedparticle = str2double(inputdlg('Enter the number of your particle','Select particle',[1 50],{'1'}));
        delete(fh)
    elseif strcmpi(Args.multipleparticles,'central')
        [~,selectedparticle] = min(sum((multipleMPstats.Centroid-size(bw)/2).^2,2));
    else
        error(['Argument "multipleparticles" should be "ok", "choose" or "central", you entered ' Args.multipleparticles]);
    end

    CCs.NumObjects   = 1;
    CCs.PixelIdxList = CCs.PixelIdxList(selectedparticle);       

end

lm = uint8(labelmatrix(CCs));

if nargout > 2
    CCstats = table2struct(regionprops3(CCs,'Centroid','Volume','EquivDiameter'));
end

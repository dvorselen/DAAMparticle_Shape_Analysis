function[IM3D] = imreadstack(PathFileName,slices)
% Function to get a 3D matrix from a tiff stack file. It reads the
% specified slices, or all slices when the second input argument is a
% string. It works for grayscale images and in case of color images only
% uses the first color.

imageinfo = imfinfo(PathFileName);   

if ischar(slices)
    slices = 1:length(imageinfo);
end

if strcmp(imageinfo(1).ColorType,'grayscale')
    
    IM3D(:,:,1) = imread(PathFileName,slices(1));

    % READ the planes 
    for i = 2:length(slices)
        IM3D(:,:,i) = imread(PathFileName,slices(i));
    end
    
else
    IM3D(:,:,1:3) = imread(PathFileName,slices(1));

    % READ the planes 
    for i = 2:length(slices)
        IM3D(:,:,i:i+2) = imread(PathFileName,slices(i));
    end

    IM3D(:,:,end-1:end) = [];   
end 
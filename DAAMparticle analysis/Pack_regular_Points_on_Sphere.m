function[Theta,Phi,GridSizeOUT] = Pack_regular_Points_on_Sphere(GridSize)

%GridSize = floor(pi*(MPDiamcorr*PixelSizeXYZ(1))/spacing);

if mod(GridSize,2);     GridSize = GridSize - 1;     end 
GridSizePhi = (GridSize+2)/2;
NAngles = GridSize*GridSizePhi;
Theta = repmat(linspace(-pi,pi * ( 1 - (2 / GridSize ) ),GridSize),1,GridSizePhi);
Phi   = reshape(repmat(linspace(-.5*pi,.5*pi,GridSizePhi),GridSize,1),1,NAngles);
GridSizeOUT = [GridSize, GridSizePhi];
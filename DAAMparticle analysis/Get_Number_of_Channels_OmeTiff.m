function[Nchannels,Slicesfirst] = Get_Number_of_Channels_OmeTiff(metadata,psc_string)
% This function tries to obtain the number of channels, and the order in
% which the images are saved

Nchannels = int32(metadata.getChannelCount(0));

if Nchannels == 0
    
    c_locs    = strfind(psc_string,'C?=');
    if ~isempty(c_locs)
        Nchannels = str2double(psc_string(c_locs+3));
        Slicesfirst = strcmp(psc_string(c_locs(end)+1),'1');
    else
        Nchannels   = 1;
        Slicesfirst = 1;
    end
      
else

    % May need to add something when wanting to process multiple positions
    if Nchannels == 1
        Slicesfirst = true;
    else
        pixdimorder = metadata.getPixelsDimensionOrder(0);
        if strfind(pixdimorder,'C')>strfind(pixdimorder,'Z')
            Slicesfirst = true;
        else
            Slicesfirst = false;
        end
    end
    
end


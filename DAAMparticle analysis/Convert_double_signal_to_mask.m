function[MPStats,global_stain_threshold] = Convert_double_signal_to_mask(Stain1,Stain2,MPStats,varargin)

% Masks are calculated based on the integrated intensity or on the maximum intensity. 
% Depending on your signal, one or the other may work better. Binary masks are made in two steps: 
% First we make an initial estimate by thresholding, then this mask is used as a seed for a subsequent active
% contour algorithm ("snake")

% Check function input and set default parameters
DefStruct = struct('globalthreshold',[],'use_global_threshold',true,...
    'stain_indicates_contact',[1 0],'coorstain2',[]);
Args = parseArgs(varargin,DefStruct,[]);

% Make sure no negative values are present:
Stain2 = cellfun(@(x) x-min(x)+1,Stain2,'UniformOutput',false);

NMPs = length(MPStats);

% First identify fully internalized based on a global threshold
if NMPs == 1    
    global_stain_threshold  = Args.globalthreshold;
elseif  Args.use_global_threshold
    warning('off','all')
    [global_stain_threshold,~,~,exitflag] = Find_logIntensity_threshold(vertcat(Stain1{:}),'UseLogCounts',0,'Npeaks',2,'bounds',[1 99],'useksdensity',1);
    warning('on','all')
    % If we can't find a good global threshold we will use the one provided by the user or not use one at all
    if exitflag == -1;        global_stain_threshold  = Args.globalthreshold;       end 
end

% Determine fully internal particles based on the global threshold      
% Here we determine two thresholds, since the inside of partial coated
% particles typically forms a distinct peak from fully internalized particles
if ~isempty(global_stain_threshold)
    
    Frac_above_global_threshold = cell2mat(cellfun(@(x) (sum(x>global_stain_threshold')/length(x)),Stain1,'UniformOutput',false)');
    % Fully internal means majority of the intensity below the first threshold and almost entirely below the second 
    Fully_internalized = all(Frac_above_global_threshold < [0.5 0.025],2);
    % Fully external means almost entirely above the second threshold
    Fully_external     = Frac_above_global_threshold(:,2) > 0.9 & sum(vertcat(Stain1{:})>global_stain_threshold(1))/numel(vertcat(Stain1{:}))<0.5;    
    % Determine the overall range of the data
    % dataspan = diff(prctile(log(vertcat(Stain2{~Fully_internalized})),[1,99]));
    
else    
    Fully_internalized = zeros(1,NMPs);
    Fully_external     = zeros(1,NMPs);
    % dataspan = diff(prctile(log(Stain2{1}),[1,99]));
end

for iMP = 1:NMPs
    
    currstain1 = Stain1{iMP};
    currstain2 = Stain2{iMP};
    
    % Conversion to regular grid because of the snake algorithm
    if strcmp(MPStats(iMP).Gridding,'equi')
        
       [Theta,Phi,gridsize] = ...
            Pack_regular_Points_on_Sphere(round(sqrt(length(MPStats(iMP).edgecoor_cart_wcatorigin)*2)));
        MPStats(iMP).edgecoor_gridsize = gridsize;
        
        % sort theta values and add some additional values to correctly interpolate on a sphere
        theta_irr = MPStats(iMP).edgecoor_sph_wcatorigin(:,1);
        phi_irr   = MPStats(iMP).edgecoor_sph_wcatorigin(:,2);
        % Interpolate to obtain a regular grid
        currstain1 = Interpolate_spherical_surface(Theta,Phi,theta_irr,phi_irr,currstain1);
        if isempty(Args.coorstain2)
            currstain2 = Interpolate_spherical_surface(Theta,Phi,theta_irr,phi_irr,currstain2);
        else
            currstain2 = Interpolate_spherical_surface(Theta,Phi,...
                Args.coorstain2{iMP}(:,1),Args.coorstain2{iMP}(:,2),currstain2);            
        end
        MPStats(iMP).IMstain1_reg = currstain1;
        MPStats(iMP).IMstain2_reg = currstain2;  
        
    else
        
        Theta    = MPStats(iMP).stain_coor_sph(:,1)'; 
        Phi      = MPStats(iMP).stain_coor_sph(:,2)'  ; 
        gridsize = MPStats(iMP).edgecoor_gridsize;
        
    end
        
    if Fully_internalized(iMP)      % We can skip the upcoming steps if the particle is fully internal 
        
        if strcmp(MPStats(iMP).Gridding,'reg') 
            MPStats(iMP).isincontact = true(gridsize);    
        else
            MPStats(iMP).isincontact = true(numel(Stain1{iMP}),1);
        end        
        MPStats(iMP).Fraction_engulfed = 1;
      
    elseif Fully_external(iMP)     % or fully external 
        
        if strcmp(MPStats(iMP).Gridding,'reg') 
            MPStats(iMP).isincontact = false(gridsize);    
        else
            MPStats(iMP).isincontact = false(numel(Stain1{iMP}),1);
        end        
        MPStats(iMP).Fraction_engulfed = 0;        
        
    else
    
        pixintgrid  = reshape(currstain1,gridsize(1),gridsize(2));
        pixintgrid2 = reshape(currstain2,gridsize(1),gridsize(2));
    
        % deal with overpresentation over points at the apices
        if mod(gridsize(:,2),2)   % deal with even and odd number of pixels in phi
            offset = 0;
        else
            offset = 0.5*pi/gridsize(2);            
        end
        pointsperlat = round(gridsize(1)*cos(linspace(-0.5*pi+offset,0.5*pi-offset,gridsize(2))));
        indices = zeros(sum(pointsperlat),2);
        % Here, for each latitude, we select indices of the appropriate number of pixels
        for ilat = 1:gridsize(2)
            latidx = linspace(0,gridsize(1),pointsperlat(ilat)+1);
            range = (sum(pointsperlat(1:ilat-1))+1):sum(pointsperlat(1:ilat));
            indices(range,1) = ilat;
            indices(range,2) = round(latidx(2:end));
        end
        indices = sub2ind([gridsize(1) gridsize(2)],indices(:,2),indices(:,1));
        
        % Now we make a corrected histogram of log intensity values
        smoothstainIM = imgaussfilt(pixintgrid,3);
        pixints = smoothstainIM(indices);
        warning off
        [stain_threshold,~,peakint] = Find_logIntensity_threshold(pixints,'iteration',iMP,'bounds',[0.001 99.9],'useksdensity',1);
        warning on
        
        initialmask = imgaussfilt(real(log(pixintgrid)),2) < log(stain_threshold); % apply threshold        
        if ~Args.stain_indicates_contact(1)
            initialmask = ~initialmask;
        end
            
        % Area's smaller than 500 nm in radius are excluded
        initialmask = bwareaopen(initialmask,round(0.7854/(pi*MPStats(iMP).Equiv_Diameter_um/gridsize(1))^2));
        initialmask = ~bwareaopen(~initialmask,round(0.7854/(pi*MPStats(iMP).Equiv_Diameter_um/gridsize(1))^2));

        % Area's where actin is depleted, but the I/O signal is too, we remove from the mask
        CC = bwconncomp(initialmask);
        meanstain = mean(log(pixintgrid2(indices)));
        fill_area = [];
        for iarea = 1:CC.NumObjects
            currpix = CC.PixelIdxList{iarea};
            currpix_sel = currpix(ismember(currpix,indices));
            if isempty(currpix)
                confirmed_outside = false;
            else
                confirmed_outside = mean(log(pixintgrid2(currpix_sel)))*1.01>meanstain;
                if Args.stain_indicates_contact(2)
                    confirmed_outside = ~confirmed_outside;
                end
            end
            if ~confirmed_outside
                initialmask(currpix) = 0;
                fill_area = [fill_area; currpix]; %#ok<AGROW>
            end
        end
        initialmask = ~initialmask;
        
        % Particle can now seem fully internalized
        if all(initialmask(:) == 1)
            
            if strcmp(MPStats(iMP).Gridding,'reg') 
                MPStats(iMP).isincontact = true(gridsize);    
            else
                MPStats(iMP).isincontact = true(numel(Stain1{iMP}),1);
            end        
            MPStats(iMP).Fraction_engulfed = 1;
            continue;
        
        % We also change these pixelvalues in pixintgrid such that localized_segmentation won't segment them
        elseif ~isempty(fill_area)
            rem_areas = zeros(gridsize(1),gridsize(2));
            rem_areas(fill_area) = 1;
            rem_areas = logical(imdilate(rem_areas,strel('disk',3)));
            pixintgrid(rem_areas) = peakint;
        end
        
        % Use an active contour algorithm to optimize the boundary
        % extend the grid to deal with boundaries in x,y
        [newmask,phi] = localized_seg(log(pixintgrid),initialmask,200,5,.2,[],[],1);
    
        MPStats(iMP).initial_mask  = single(initialmask);           
        MPStats(iMP).actcont_mask  = single(newmask);
        MPStats(iMP).actcont_phi   = phi;
    
        % Determine for each point if it is inside or outside and calculate fraction engulfed
        if strcmp(MPStats(iMP).Gridding,'reg')

            MPStats(iMP).isincontact = logical(newmask);
        
        else
        
            GI = griddedInterpolant({[Theta(1:gridsize(1)) pi],Phi(1:gridsize(1):end)},double([newmask; newmask(1,:)]));
            iscovered = logical(round(GI(MPStats(iMP).edgecoor_sph_wcatorigin(:,1),MPStats(iMP).edgecoor_sph_wcatorigin(:,2))));  
            iscovered(iscovered<0) = 0;
            MPStats(iMP).isincontact = iscovered;
        
        end
        
        % Calculate the fraction engulfed
        MPStats(iMP).Fraction_engulfed = Calc_Frac_engulfed(MPStats(iMP).edgecoor_sph_wcatorigin(:,1),...
            MPStats(iMP).edgecoor_sph_wcatorigin(:,2),MPStats(iMP).isincontact,size(MPStats(iMP).edgecoor_cart,1));
        
    end
end

MPStats = orderfields(MPStats);

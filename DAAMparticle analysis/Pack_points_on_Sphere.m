function [labda,phi] = Pack_points_on_Sphere(N,varargin)
% Function that provides the angles (lambda and phi) of roughly equally
% spaced points on a sphere. See:
% http://web.archive.org/web/20120421191837/http://www.cgafaq.info/wiki/Evenly_distributed_points_on_sphere

% Check user input
points_at_apices = 1;
if nargin > 1
    if islogical(varargin{1})
        points_at_apices = varargin{1};
    end
end

% Determine longitudes
dlong = pi*(3-sqrt(5)); %  ~2.39996323
long = dlong*(0:N-1);

% This includes points at the apices, which can cause non-uniform spreading
% in case of few points
if points_at_apices
    dz    = 2.0/(N-1);
    Z    = 1 - dz*(0:N-1);
else
    dz    = 2.0 / N;
    Z    = 1 - .5* dz - dz*(0:N-1);    
end

% Convert longitude and Z coordinates into theta and phi
labda = rem(long+pi,2*pi)-pi;
phi   = asin(Z);

% Initially, I calculated these values (involving some unnecesary steps) by
%r = sqrt(1-Z.^2);
%X =  cos(long).*r;
%Y =  sin(long).*r;
%[theta,phi] = cart2sph(X,Y,Z);

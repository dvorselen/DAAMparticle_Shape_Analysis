function[VoxelSize] = Get_PixelSize_OmeTiff(metadata)
% This function tries to obtain the pixel sizes from OME-tiff data, if it
% fails it asks the user for input

try   
    
    VoxelSize(1) = double(metadata.getPixelsPhysicalSizeX(0).value(ome.units.UNITS.MICROMETER));  
    VoxelSize(2) = double(metadata.getPixelsPhysicalSizeZ(0).value(ome.units.UNITS.MICROMETER));
    if all(VoxelSize == 1);  error('voxelsize probably not accurate');     end   

catch
    
    % Ask the user to input the voxelsize of the images
    VoxelSize = str2double(inputdlg({'PixelSize x and y (um)','Absolute PixelSize z (um)'},...
        'Enter the voxelsize in um',[1 50; 1 50],{ '0.104', '0.211'})); 

end


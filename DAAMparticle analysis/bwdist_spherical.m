function[pgcdist] = bwdist_spherical(Theta,Phi,bw)
% Currently requires a regular grid. Calculates a distance transform over the
% unit sphere and returns distances in radians

if nargin == 1 % Allow use with only the black and white image as input: bwdist_spherical(bw)
    bw = Theta;
end

if size(bw,2)>size(bw,1)
    flip = 1;
    bw = bw';
else
    flip = 0;
end

if nargin == 1
    phivals   = linspace(-.5*pi,.5*pi,size(bw,2))';
    Phi   = reshape(repmat(phivals,1,size(bw,1))',[],1);
    Theta = linspace(-pi,pi-2*pi/size(bw,1),size(bw,1))';     
    Theta = repmat(Theta,size(bw,2),1);
else  % check inputs in case Theta and Phi are provided
    if size(Theta,1) == 1;      Theta = Theta'  ;    end
    if size(Phi,1) == 1  ;      Phi = Phi'      ;    end
    phivals = unique(Phi);
end

boundarymask = bwperim([bw(end,:); bw; bw(1,:)]); % properly wrap the coordinates
boundarymask([1 end],:) = []; % remove additional pixels
boundarymask(:,[1 end]) = 0; % update the apices of the particles

if ~any(boundarymask(:))
    pgcdist = -Inf(size(boundarymask));
else
    % Convert to cartesian coordinates
    [X,Y,Z] = sph2cart(Theta,Phi,ones(size(Theta)));
    Theta_bound = Theta(boundarymask);
    Phi_bound   = Phi(boundarymask)  ;

    % Calculate minimal euclidian distances with the boundary to find closest
    % points (much faster than calculating great circle distance)
    Eudist = pdist2([X,Y,Z],[X(boundarymask),Y(boundarymask),Z(boundarymask)]);
    [~,minloc] = min(Eudist,[],2);

    % Calculate great circle distance, only between closest points.
    pgcdist = real(acos(sin(Phi).*sin(Phi_bound(minloc))+cos(Phi).*cos(Phi_bound(minloc)).*cos(Theta-Theta_bound(minloc))));
    pgcdist(bw==1) = -pgcdist(bw==1);
    pgcdist = reshape(pgcdist,size(bw,1),size(bw,2));
    
    % Compensate that the boundary is now taken as the boundary pixel of the
    % foreground, instead of in between the fore and background
    add05pix = repmat(cos(phivals')*pi/size(bw,2),length(bw),1);
    pgcdist  = pgcdist - add05pix;
end

if flip
    pgcdist = pgcdist';
end



function[MPStats] = IMStats_to_MPStats(IMStats)  
% Now that we have identified the individual particles, we can do
% subsequent analasysis on a per-particle basis

dummyStats = IMStats;
dummyStats = rmfield(dummyStats,{'IM3D','IMlm','CCs'});

NMPs = cellfun(@length,{IMStats.PixelIdxList});
MPStats(1:NMPs(1)) = dummyStats(1);

for iIM = 2:length(IMStats)
    MPStats(end+(1:NMPs(iIM))) = dummyStats(iIM);
end

nMPs_in_previous_IMs = 0;

% Find to which image each particle belongs and add the particle statistics
for iIM = 1:length(IMStats)
    
    MPs_in_curr_IM = NMPs(iIM);
    
    if ~isempty(MPs_in_curr_IM)
        
        currrange = nMPs_in_previous_IMs+(1:MPs_in_curr_IM);
    
        [MPStats(currrange).IMnumber] = deal(iIM);
        nMPs_in_previous_IMs = nMPs_in_previous_IMs + MPs_in_curr_IM;

        % Add some particle statistics
        [MPStats(currrange).Centroid     ] = dummyStats(iIM).Centroid{:};
        [MPStats(currrange).PixelIdxList ] = IMStats(iIM).PixelIdxList{:};
        [MPStats(currrange).Volume       ] = ArraytoCSL(dummyStats(iIM).Volume);
        [MPStats(currrange).EquivDiameter] = ArraytoCSL(dummyStats(iIM).EquivDiameter);
        
    end 
end

function varargout=ArraytoCSL(C)
% Turns an array into a comma seperated list. 

% First put the array into a cell
if isnumeric(C) || isstruct(C) 
    C=num2cell(C); 
end

% Then make the cell into a SCL
C=C(:).';

% varargout is used to fill up the fields of a structure for example
varargout=C(1:nargout);
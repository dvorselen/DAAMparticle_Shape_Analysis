function [XYZedges] = Apply_3DSobel_filter(IMs,smoothpixelsize)

% Create the 3 3dimensional sobel filters
sobel3Dz(:,:,1) = [1 2 1; 2 4 2; 1 2 1];
sobel3Dz(:,:,2) = zeros(3,3);
sobel3Dz(:,:,3) = -sobel3Dz(:,:,1);
sobel3Dx = permute(sobel3Dz,[1 3 2]);
sobel3Dy = permute(sobel3Dz,[3 1 2]);

%prepare output var
XYZedges = cell(1,length(IMs));

for iIM = 1:length(IMs)
    
    currIM = double(IMs{iIM});
    
    % smooth the individual images
    if nargin > 1
        if all(smoothpixelsize > 0)
            if length(smoothpixelsize) == 1
                currIM = imgaussfilt3(currIM,smoothpixelsize);
            elseif length(smoothpixelsize) == 3
                currIM = imgaussfilt3(currIM,[smoothpixelsize(1),...
                    smoothpixelsize(2),smoothpixelsize(3)]);
            end
        end
    end
       
    % Convolve in the 3 directions
    xedges = convn(currIM,sobel3Dx,'same');
    yedges = convn(currIM,sobel3Dy,'same');
    zedges = convn(currIM,sobel3Dz,'same');

    % Take the sum of squares of the 3 volumetric images
    xyzedges = sqrt(xedges.^2 + yedges.^2 + zedges.^2);
    % Make the outer edge zero
    xyzedges([1 end],:,:) = 0;
    xyzedges(:,:,[1 end]) = 0;
    xyzedges(:,[1 end],:) = 0;
    % Store output
    XYZedges{iIM} = xyzedges;
     
end
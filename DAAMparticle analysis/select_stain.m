function stainname = select_stain(Stats)

Fnames = fieldnames(Stats);

if any(contains(Fnames,'_ch'))
    
    % Determine channels present in MPStats
    channelstrings  = regexp(Fnames,'_ch\d','match');
    channelstrings  = unique([channelstrings{:}]);
    channelopts     = ["_ch1","_ch2","_ch3","_ch4","_ch5"];
    channelspresent = ismember(channelopts,channelstrings);
    channelspresent = strjoin(channelopts(channelspresent),',  ');

    % Create input dialog box
    select_channel = questdlg('Which signal do you want to analyze or visualize?',...
        'Select stain','IMStain',channelspresent{:},'IMStain');

    % Evaluate user input
    if strcmp(select_channel,'IMStain')
        stainname = 'stain';
    else
        stainname = ['stain' select_channel];
    end
    
else
    
    stainname = 'stain';
    
end
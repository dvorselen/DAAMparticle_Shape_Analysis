function[Stain_channel] = Find_Stain_Channel(data,MPStats)

% Find the stain channel
if MPStats(1).NChannels == 1
    error('Only 1 channel, and hence no stain channel, present')
elseif MPStats(1).NChannels == 2 
    Stain_channel = setdiff([1 2],MPStats(1).MPchannel);   
else 
    if MPStats(1).SlicesFirst
        checkchannels = round(double(size(data{1},1)/MPStats(1).NChannels).*(double((1:MPStats(1).NChannels))-.5));
    else
        checkchannels = floor(size(data{1},1)/2/MPStats(1).NChannels)*MPStats(1).NChannels+(1:MPStats(1).NChannels);
    end
    nonMPchannels = setdiff(1:MPStats(1).NChannels,MPStats(1).MPchannel);
    checkchannels = checkchannels(nonMPchannels);
    Stain_channel = nonMPchannels(UI_Select_Channel(data{1}(checkchannels,1),[]));
end

% Update channels if channels were taken in different orders
if exist('IMStats','var') && NMPs > 1 && MPStats(1).NChannels > 1 && ~any(cellfun(@isempty,[MPStats.ChannelColors]))
    StainChannels    = cell(1,NMPs);
    StainChannels{1} = Stain_channel;
    for i = 2:NMPs
        StainChannels{i} = find(all(reshape([MPStats(i).ChannelColors{:}],3,[])==MPStats(1).ChannelColors{Stain_channel}'));
    end
    Stain_channel  = StainChannels;
end
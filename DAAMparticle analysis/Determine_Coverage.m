function [Peak_coordinates,Peak_heights,Peak_widths,Peak_integratedintensities,exitflag] = ...
    Determine_Coverage(stain_signal, PartCentroid, PartDiam, Theta, Phi, pixelsizeXYZ,varargin)
% Note: Particle diameter, centroid etc. should be given in pixels
% Currently doesn't deal well with the situation that no peaks are present at all

% Important: all variables should be provided in units Px, except for R &
% radialinterval, which should be provided in um

% Check user input and otherwise use default method
DefStruct = struct('method','FWHM','radialinterval',1,'R',[],'Gaussiansignal',1);
Args = parseArgs(varargin,DefStruct,[]);

exitflag = 0;

if Args.Gaussiansignal && ~any(strcmp(Args.method,{'FWHM','Gaussian'}))
    warning('method must be either "FWHM" or "Gaussian", used default (FWHM)')
end

nMPs = length(PartDiam);

%Ensure thate theta phi and R are columns
R = Args.R;
if size(Theta,1) == 1;  Theta = Theta';    end
if size(Phi,1)   == 1;  Phi   = Phi'  ;    end
if size(R,1)     == 1;  R     = R'    ;    end

% Determine the number of angles 
if nMPs == 1 
    NAngles = length(Theta);
    [Xunit,Yunit,Zunit] = sph2cart(Theta,Phi,ones(NAngles,1));
else
    NAngles = cellfun(@length,Theta);
end

% Setup a fitting function, in case method is Gaussian
if Args.Gaussiansignal && strcmp(Args.method,'Gaussian')
    gausFun = @(hms,x) hms(1) .* exp (-(x-hms(2)).^2 ./ (2*hms(3)^2));
    options = optimset('Display','off','Algorithm','levenberg-marquardt');
    optionsbounded = optimset('Display','off');
    Problems_encountered = cell(nMPs,max(NAngles));
end

% If R is not provided, create lines from the centroid of the particle crossing the boundary
if isempty(R)   
    LineLength = round(PartDiam* .8 );     
end

% calculated normalized pixelsizes
norm_Z     = pixelsizeXYZ(2)/pixelsizeXYZ(1);
norm_XYZ   = nthroot(norm_Z,3);
norm_XYZsq = norm_XYZ^2;

% Setup output vars
Peak_coordinates                    = cell(nMPs,1) ;
Peak_heights                        = cell(nMPs,1) ;
Peak_widths                         = cell(nMPs,1) ;
Peak_integratedintensities          = cell(nMPs,1) ;

% Main loop
for iMP = 1:nMPs
       
    partcentroidi = PartCentroid(iMP,:);
    
    % Construct the lines passing through the particle edge
    if isempty(R)
        linelengthi = LineLength(iMP);
        xcoor = partcentroidi(1) + Xunit * norm_XYZ   * (1:linelengthi);
        ycoor = partcentroidi(2) + Yunit * norm_XYZ   * (1:linelengthi);
        zcoor = partcentroidi(3) + Zunit / norm_XYZsq * (1:linelengthi);  
    else  % Look for the intensity signal within user specified distance of the particle boundary
        linesize = round(Args.radialinterval/pixelsizeXYZ(1));
        linei = repmat(-linesize:linesize,NAngles(iMP),1) + repmat(R,1,2*linesize+1)/pixelsizeXYZ(1);
        xcoor = partcentroidi(1) + Xunit .* linei;
        ycoor = partcentroidi(2) + Yunit .* linei;
        zcoor = partcentroidi(3) + Zunit .* linei/norm_Z;
    end
    
    if any(vertcat(xcoor(:),ycoor(:),zcoor(:))<0) || any(any([xcoor(:) ycoor(:) zcoor(:)]>size(stain_signal)))
        exitflag = -1;
    end
    
    % Create a lineprofile using interpolation
    LineProfile = interp3(stain_signal,xcoor,ycoor,zcoor,'linear');
       
    if Args.Gaussiansignal
    
        % Set up output vars for internal loop
        nrots = size(LineProfile,2);
        pk  = zeros(1,nrots);
        loc = zeros(nrots,1);
        w   = zeros(1,nrots);

        % Find maxima, FWHM and a linear index for them
        for jrot = 1:NAngles(iMP)   

            [pkj,locj,wj] = findpeaks(LineProfile(jrot,:),'Npeaks',1,'Sortstr','descend'); 

            if ~isempty(pkj)

                if ~strcmp(Args.method,'Gaussian') 

                    pk(jrot)  = pkj;
                    loc(jrot) = locj;
                    w(jrot)   = wj;

                else               

                    Line = 1:length(lineprofilei);
                    include = ~excludedata(Line,lineprofilei,'domain',[ceil(loc(jrot)-w(jrot)) floor(loc(jrot)+w(jrot))]);
                    % Make sure that NaNs (arrising from interpolation beyond image
                    % boundaries) are not included in the fit
                    include(isnan(lineprofilei)) = 0;
                    % If the fit creates and error, we will ignore it and count it as a missed peak
                    try
                        lineseg = Line(include);
                        lineprofileseg = lineprofilei(include);
                        % We first try a faster non-bounded fit
                        [fitted_hms] = lsqcurvefit(gausFun,[pk(jrot),loc(jrot),w(jrot)/2.35],lineseg,lineprofileseg,[],[],options);
                        % If the outcome of the fit is off, we try a bounded fit
                        if fitted_hms(2) < lineseg(1) || fitted_hms(2) > lineseg(end) || fitted_hms(3) > w(jrot)*2
                            Problems_encountered{iMP,jrot} = 'Initial fitting parameters out of bound';
                            [fitted_hms] = lsqcurvefit(gausFun,[pk(jrot),loc(jrot),w(jrot)],lineseg,lineprofileseg,...
                                [0 lineseg(1) 0],[Inf lineseg(end) w(jrot)*2],optionsbounded);
                        end 
                        % Update the estimates
                        pk(jrot)  = fitted_hms(1);
                        loc(jrot) = fitted_hms(2);
                        w(jrot)   = fitted_hms(3); 
                    catch ME
                        Problems_encountered{iMP,jrot} = ME;
                        continue
                    end
                end

            else    

                % In case no peak was detected, the first or last pixel was the max
                [pk(jrot),loc(jrot)]  = max(LineProfile(jrot,:));
                w(jrot)   = NaN;
                Problems_encountered{iMP,jrot} = 'No peak detected';

            end
        end

        % Warn user in case of suspected clipping of intensity profiles
        if strcmp(Args.method,'FWHM') &&  any(abs(nanmean((LineProfile(:,end)-LineProfile(:,1))./pk))>.1)   
            warning(['For some particles it appears that the lineprofile is clipped, which '...
                'results in poor width and integrated intensity estimates. Consider using Gaussian fitting'])
        end

        % Calculate the integrated intensity (assuming a Gaussian signal!)
        intint  = w.*pk; 
       
    else
        
        [pk,loc] = max(LineProfile,[],2);
        intint   = sum(LineProfile,2);
        w        = [];        
        
    end
     
    % Determine the coordinates of the maximum line crossing for each      
    lin_idx = sub2ind(size(LineProfile),1:NAngles,loc'); 
    curr_peak_coordinates = [xcoor(lin_idx)', ycoor(lin_idx)',...
        zcoor(lin_idx)', Theta(1:NAngles), Phi(1:NAngles), loc]';
     
    % Store output vars
    Peak_coordinates{iMP}           = curr_peak_coordinates;
    Peak_heights{iMP}               = pk;
    Peak_widths{iMP}                = w;
    Peak_integratedintensities{iMP} = intint;
    
end


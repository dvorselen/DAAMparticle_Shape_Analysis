function[TimeStamp,TimeUnit] = Get_TimeStep_OmeTiff(data,Nfiles)
% This function tries to obtain the time step between stacks from OME-tiff data. 
% This function is not expected to be superstable at this point
% fails it asks the user for input

psc_string = data{1}{1,2};

if Nfiles > 1
    
    if contains(psc_string,'sec')
        starttimestr       = regexp(psc_string,'\d*.sec','match','once');
        starttimestrNoUnit = regexp(starttimestr,'\d*','match','once');
        starttime          = str2double(starttimestrNoUnit);
        TimeUnit           = starttimestr(length(starttimestrNoUnit)+1:end);
        TimeStamp          = zeros(1,Nfiles);
        for ifile = 2:Nfiles
            currtimestr      = regexp(data{ifile}{2,2},'\d*.sec','match','once');
            TimeStamp(ifile) = str2double(regexp(currtimestr,'\d*','match','once'))-starttime;
        end

    else
     
        % Ask the user to input the voxelsize of the images
        TimeStamp = str2double(inputdlg('Timestep between frames in seconds (leave empty if irregular or not applicable)',...
            'Enter the framerate (s)',[1 50],{''})); 
        try 
            TimeStamp = linspace(0,TimeStamp*(Nfiles-1),Nfiles);
        catch
            TimeStamp = cell(1,Nfiles);
        end
        TimeUnit = 's';   
        
    end
    
    % Milliseconds are really unnessecary
    if strcmp(TimeUnit(1),'m')
        TimeStamp = TimeStamp/1000;
        TimeUnit  = TimeUnit(2:end);
    end
    
    % We use SI abbreviations for units
    if length(TimeUnit)>2 
        if strcmp(TimeUnit(end-1:end),'ec')
            TimeUnit = TimeUnit(1:end-2);
        end
    end
    
    if ~iscell(TimeStamp)
        TimeStamp = num2cell(TimeStamp);
    end
 
% Case that they are all contained in a single hyperstack, which probably
% will run into errors later
elseif ~isempty(data{1,4}.getPixelsTimeIncrement(0))
    
    TimeInterval = double(data{1,4}.getPixelsTimeIncrement(0).value(ome.units.UNITS.S));
    TimeStamp = num2cell((0:1:size(data,1))*TimeInterval);
    TimeUnit  = 's';
    
else
    
    TimeStamp = {[]};
    TimeUnit  = [];
    
end


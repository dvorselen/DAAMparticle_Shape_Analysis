function[MPStats] = Triangulate_surface_and_determine_particle_statistics(MPStats)

NMPs = length(MPStats);
V  = cell(1,NMPs);
WC = cell(1,NMPs);
    
% First we determine if we have a regular grid or not. In case of a regular grid triangulation is faster
if strcmp(MPStats(1).Gridding,'reg')
    reggrid = 1; 
elseif strcmp(MPStats(1).Gridding,'equi')
    reggrid = 0;
end

for iMP = 1:NMPs 

    % Determine theta, phi, R
    theta     = MPStats(iMP).edgecoor_sph(:,1);
    phi       = MPStats(iMP).edgecoor_sph(:,2);
    
    % Triangulate 
    [TRI,TRI_nosphbound] = Triangulate_spherical_object(theta,phi,reggrid);
    MPStats(iMP).TRI_Connectivity_SPHbound = TRI;
    MPStats(iMP).TRI_Connectivity = TRI_nosphbound;
    
    % Create a triangulated object
    curredgecoor_physunit = MPStats(iMP).edgecoor_cart.*[repmat(MPStats(iMP).PixelSizeXY,1,2) MPStats(iMP).PixelSizeZ];
    TRIobject = triangulation(TRI,curredgecoor_physunit);
    
    % Done, now calculate particles statistics
    [MPStats(iMP).Area,V{iMP},MPStats(iMP).Sphericity,WC{iMP}] = Area_Volume_trisurf(TRIobject);

    % Update the cartesian coordinatees with origin at weighted centroid
    MPStats(iMP).edgecoor_cart_wcatorigin = (curredgecoor_physunit-repmat(WC{iMP}',size(curredgecoor_physunit,1),1));
    % Also update spherical coordinates, note that we do need to redo the triangulation
    [theta_upd,phi_upd,R_upd] = cart2sph(MPStats(iMP).edgecoor_cart_wcatorigin(:,1),...
        MPStats(iMP).edgecoor_cart_wcatorigin(:,2),MPStats(iMP).edgecoor_cart_wcatorigin(:,3));
    
    if reggrid  % deal with the repeated values at top and bottom
        repeats_at_apex = theta_upd == theta_upd(end);
        theta_upd(repeats_at_apex) = repmat(linspace(-pi,pi-2*pi/sum(repeats_at_apex)/2,...
            sum(repeats_at_apex)/2),1,2);
    end
    
    MPStats(iMP).edgecoor_sph_wcatorigin = [theta_upd,phi_upd,R_upd];
    [~,MPStats(iMP).TRI_Connectivity_wcatorigin] = Triangulate_spherical_object(theta_upd,phi_upd,reggrid);
    
end

% Save in our data structure
[MPStats.Volume]                      = V{:} ;
[MPStats.Weighted_Centroid]           = WC{:};
[MPStats.Equiv_Diameter_um]           = ArraytoCSL(([V{:}]*6/pi).^(1/3));

MPStats = orderfields(MPStats);

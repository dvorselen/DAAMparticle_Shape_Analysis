function[ThetaPhiRsmooth,N,signal] = Smooth_spherical_mesh(ThetaPhiR,threshold,varargin)
    % This function allows smoothing of a signal (like the radial component) of
    % a spherical mesh. It's principle is similar to a moving average filter,
    % using a circular window on a spherical surface. 

    % Required input:
    % ThetaPhiR: a nx3 (to nxm, with m>3) array with columns longitude,
    %            latitude, and the signals to be smoothed in subsequent
    %            columns. Theta and phi should be in radians. Note that the 3rd
    %            column has to correspond to R in case using thresholdtype 'arc'.
    % threshold: the radius of the circle that is the moving average circle. Can 
    %            be either given as an angle, or as a distance along the
    %            spherical surface (arc)

    % Check input (set defaults)
    thresholdtype = 'angle';   
    iter          = 1      ; 
    filter        = 'mean' ; 
    weighted      = false  ;   

    % or use user inputs
    if nargin > 2  && ~isempty(varargin{1})
        thresholdtype = varargin{1};
        if ~any(strcmp(thresholdtype,{'angle','arc'}))
            error('input 3, the thresholdtype, should be either "angle" or "arc"');
        end
    end
    if nargin > 3  && ~isempty(varargin{2})
        iter = varargin{2};
    end
    if nargin > 4  && ~isempty(varargin{3})
        filter = varargin{3};
        if ~any(strcmp(filter,{'mean','median'}))
            error('input 5, the filtertype, should be either "mean" or "median"');
        end
    end
    if nargin > 5 && ~isempty(varargin{4})
        weighted = varargin{4};
    end
    if nargin == 7 && ~isempty(varargin{5})
        signal = varargin{5};
    else
        signal = [];
    end

    if threshold ~=0
        % Weighted median filtering is currently not supported (but can be implemented later)
        if weighted && strcmp(filter,'median')
            error('weighted median filtering currently not supported')
        end

        % Handle duplicate values (in case of regular grid)
        [uqThetaPhiR,~,idxmap] = unique(ThetaPhiR,'rows','stable');

        Theta = uqThetaPhiR(:,1);
        Phi   = uqThetaPhiR(:,2);
        R     = uqThetaPhiR(:,3:end);

        % Calculate point to point distance. This is rather slow.
        sinPhi  = sin(Phi);
        cosPhi  = cos(Phi);
        pgcdist = real(acos(sinPhi.*sinPhi'+cosPhi .*cosPhi'.*cos(Theta'-Theta))); % times Radius gives real arclength!

        % Determine which points are close together (neighbours)
        if strcmp(thresholdtype,'arc');     threshold = threshold/mean(R(:,1));      end
        neighbours = pgcdist<threshold;

        % Determine the number of neighbours of each point
        N = sum(neighbours,2);    
        if all(neighbours==1) % warn the user if no points have neighbours
            warning('windowsize (threshold) too small and no points had neighbours, output = input')
            ThetaPhiRsmooth = ThetaPhiR;
            return
        end

        % Calculate weights
        if weighted
            weights = 1./pgcdist(neigbours);                % weights are inverse of distance
            weights(1:(length(R)+1):end) = 0;               % element itself gets non-Inf weight
            weights(1:(length(R)+1):end) = max(weights);    
            weights = weights./sum(weights);                % normalize weights
        end

        R = Smoothit(R);

        if ~isempty(signal) && nargout > 2
            signal = Smoothit(signal);
        end

        % prepare output
        uqThetaPhiR = [Theta,Phi,R];
        ThetaPhiRsmooth = uqThetaPhiR(idxmap,:); % include duplicated points
    else
        ThetaPhiRsmooth = ThetaPhiR;
    end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function S = Smoothit(S)

        if strcmp(filter,'mean')
            for i = 1:iter  % We iterate
                if ~weighted
                    S = (neighbours*S)./N;
                else
                    S = weights*S;
                end
            end
        else
            neighbours = double(neighbours);
            neighbours(neighbours==0) = NaN;
            for i = 1:iter  % We iterate
                S = nanmedian(neighbours.*S',2);
            end
        end

    end  

end
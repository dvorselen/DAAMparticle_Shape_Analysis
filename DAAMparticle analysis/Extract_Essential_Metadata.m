function[Stats,data] = Extract_Essential_Metadata(data,FileName,PathName,varargin)

% This functions gathers metadata that we will need for the rest of the analysis
% Input argument forcechannelsepstrings is a cell of strings that contains
% unique identifiers for each channel (e.g. {'ch01','ch02'})

% Check function input and set default parameters
DefStruct = struct('istimeseries',1,'forcechannelsepstrings',[],'forcechannelsepN',[],'forcechannelsepOrder','slicesfirst');
Args = parseArgs(varargin,DefStruct,[]);

% Organize metadata
metadata = data{1,4};
psc_string  = data{1}{1,2}; % String with the number of planes, slices and colors
Nplanes = int32(cellfun(@(x) size(x,1),data(:,1)));

% Find number of timepoints and split stack, if all timepoints were
% provided in a single stack
if Args.istimeseries && size(data,1) == 1
    Ntimepoints = int32(double(metadata.getPixelsSizeT(0).getNumberValue));
    data = [mat2cell(data{1},repmat(Nplanes/Ntimepoints,Ntimepoints,1),2), [data(1,2:4); repmat({{},{},{}},Ntimepoints-1,1)]];
    Expanded_data = 1;
else
    Ntimepoints = 1;
    Expanded_data = 0;
end

% Set up the output structure
Nfiles = length(data(:,1));
dummycell = cell(Nfiles,1);
Stats = struct('PathName',PathName,'FileName',FileName,'PixelSizeXY',dummycell,...
    'PixelSizeZ',dummycell,'NChannels',dummycell,'NSlices',dummycell,'SlicesFirst',dummycell,...
    'ChannelColors',dummycell,'ChannelNumber',dummycell,'IM3D',dummycell);

% Find number of channels
[Nchannels,Slicesfirst] = Get_Number_of_Channels_OmeTiff(metadata,psc_string);
if ~isempty(Args.forcechannelsepN)
    Nchannels   = Args.forcechannelsepN;
    if ~strcmp(Args.forcechannelsepOrder,'slicesfirst')
        data{1}(:,1) = data{1}(reshape((1:Nplanes/Nchannels)+Nplanes/Nchannels.*(0:(Nchannels-1))',1,[]),1);
    end   
end
 Nslices                = Nplanes./Nchannels/Ntimepoints;
[Stats.NSlices        ] = ArraytoCSL(Nslices);

% Ask the user to select the channel that contains the microparticles
if Slicesfirst
    checkchannels = round(double(size(data{1},1)/Nchannels).*(double((1:Nchannels))-.5));
else
    checkchannels = floor(size(data{1},1)/2/Nchannels)*Nchannels+(1:Nchannels);
end
MPchannel = deal(UI_Select_Channel(data{1}(checkchannels,1),[]));

% Find the channel colors
if ~Expanded_data
    channelcolors = Get_ChannelsColors_OmeTiff(data(:,4),data(:,1),Nchannels,MPchannel,Slicesfirst);
else
    channelcolors = Get_ChannelsColors_OmeTiff(data(1,4),data(1,1),Nchannels,MPchannel,Slicesfirst);
    channelcolors = repmat(channelcolors,Ntimepoints,1);
end

% Check if different channels were supplied as different images
if Nchannels == 1 && (~any(cellfun(@isempty,channelcolors(1,:))) || ~isempty(Args.forcechannelsepstrings))
    
    if ~isempty(Args.forcechannelsepstrings)
        
        if length(Args.forcechannelsepstrings)>1            
            Nchannels        = length(Args.forcechannelsepstrings);
            [channelidx,~]   = find(cell2mat(cellfun(@(x) contains(FileName,x),Args.forcechannelsepstrings,'UniformOutput',false))');
            [~,uqchannelidx] = unique(channelidx);      
        else
            [~,uqchannelidx] = unique(cell2mat(cellfun(@(x) x{:},channelcolors,'Uniformoutput',false)),'rows');
            Nchannels        = length(uqchannelidx);            
        end
        
        if Nchannels > 1
            datatocheck = vertcat(data{uqchannelidx,1});
            MPchannel   = deal(UI_Select_Channel(datatocheck(round(Nslices(1)/2)+(0:Nslices(1):Nslices(1)*(Nchannels-1)),1),[]));
            Slicesfirst = 1;
        end
    
        % reorganize image data
        tokeep = 1:Nchannels:Nfiles;
        for iIM = tokeep
            data{iIM,1} = vertcat(data{uqchannelidx+iIM-1,1});
        end
        % update further parameters
        data   = data(tokeep,:) ;
        Stats  = Stats(tokeep,:);
        Nfiles = Nfiles/Nchannels;
        if isempty(Args.forcechannelsepstrings)
            channelcolors    = mat2cell(repmat([channelcolors{uqchannelidx}],Nfiles,1),ones(1,Nfiles),Nchannels);
        end
    end
end

[Stats.ChannelColors] = channelcolors{:};
[Stats.SlicesFirst]   = deal(Slicesfirst);  
[Stats.NChannels  ]   = deal(Nchannels);

% If multiple channelcolors are detected, we update the MPchannel for subsequent images 
% (such that the code is stable for when you switched channels during an experiment)
if Nfiles > 1 && Nchannels > 1 && ~any(cellfun(@isempty,channelcolors(1,:)))
    MPChannels    = [MPchannel zeros(1,Nfiles-1)];
    for i = 2:Nfiles
        MPChannels(i) = find(all(reshape([channelcolors{i}{:}],3,[])==channelcolors{1}{MPchannel}'));
    end
    [Stats.MPchannel] = ArraytoCSL(MPChannels);
else
    [Stats.MPchannel] = deal(MPchannel); 
    warning('Channel colors not detected: all images need to be acquired with channels in the same order')
end
    
% Find pixel size or ask user
VoxelSize = Get_PixelSize_OmeTiff(metadata);
[Stats.PixelSizeXY] = deal(VoxelSize(1));
[Stats.PixelSizeZ ] = deal(VoxelSize(2));

% This can be incorporated later. Currently, the timestep is not used for
% anything in subsequent analysis, so there is little point in obtaining it.
% Obtain timestep
%if Args.istimeseries
%    [TimeStamp,TimeUnit] = Get_TimeStep_OmeTiff(data,Nfiles);
%    [Stats.TimeStamp]  = TimeStamp{:}  ;
%    [Stats.TimeUnit]   = deal(TimeUnit);   
%end

function[deconvIMs] = Deconvolve_using_DeconvolutionLab2(IMs,PSFs,iter)

if nargin < 3
    iter = 25;
end

NMPs      = length(IMs);
deconvIMs = cell(1,NMPs);

for iMP = 1:NMPs
    
    java.lang.Runtime.getRuntime.gc;     % Clean up java heapspace
    deconvIMs{iMP} = DL2.LW(single(IMs{iMP}), single(PSFs{iMP}), iter , 1,'-monitor no -verbose quiet -display no ');
       
end

% Final cleaning of java heapspace
java.lang.Runtime.getRuntime.gc;

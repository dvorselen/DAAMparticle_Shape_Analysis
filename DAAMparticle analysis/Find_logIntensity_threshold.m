function[threshold,backgroundint,peakint,exitflag] = Find_logIntensity_threshold(IM,varargin)

% Exitflags:
% 1  found thresholds as expected
% -1 could not find expected number of thresholds, used percentiles of the data instead
% -2 could find expected number of thresholds, but struggled to find peak/and or background intensity

% Check function input and set default parameters
DefStruct = struct('iteration',[],'UseLogCounts',1,'minbins',50,'BinWidth',[],'bounds',[10 99.9],'Npeaks',1,'useksdensity',0);
Args = parseArgs(varargin,DefStruct,[]);

% Make a histogram and look for the dip between the two (log) intensity peaks (background,particles)
currpxints = double(IM(:));
currpxints(currpxints<=0) = [];    % zeros are usually artefacts (and can give problems with logarithms)
bounds = prctile(currpxints,Args.bounds);
currpxints = log(currpxints(currpxints>bounds(1) & currpxints<bounds(2)));
if Args.useksdensity
    [ele,bincentra] = ksdensity(currpxints);
    ele([1 end])       = [];
    bincentra([1 end]) = [];
else
    if isempty(Args.BinWidth)
        [ele,edges] = histcounts(currpxints);
    else
        [ele,edges] = histcounts(currpxints,'BinWidth',Args.BinWidth);
    end
    if length(edges)<Args.minbins && isempty(Args.BinWidth)
        [ele,edges] = histcounts(currpxints,Args.minbins);
    end
    bincentra = edges(1:end-1)+(edges(2)-edges(1))/2;
end

% Using log counts is helpful if one of the populations is rare (e.g. particles vs background)
if Args.UseLogCounts 
    logIntdistb = log(smooth(ele,max(ceil(length(ele)/100),5)));
    minpeakprm  = 0.01;
else
    logIntdistb = smooth(ele,max(ceil(length(ele)/100),5));
    minpeakprm  = ceil(sum(ele)/1000);
end

% Search for min intensity between the background and particle signal
[~,minloc,~,prom] = findpeaks(-logIntdistb,'MinPeakProminence',minpeakprm,'MinPeakWidth',3);
if numel(minloc)>=Args.Npeaks
    [~, promsortidx ] = sort(prom,'descend');   % pick the most prominent valleys as threshold
    minloc = minloc(promsortidx(1:Args.Npeaks)); 
    if length(minloc) > Args.Npeaks   
        minloc = movmean(minloc,2)   ;
        minloc = round(minloc(2:end));      
    end
end

if numel(minloc)<Args.Npeaks
    warning(['Couldn''t detect a proper threshold for particle/frame ' num2str(Args.iteration)...
        ' this could be because of poor signal-to-noise, and can likely be fixed by'...
        ' changing some of the parameters in Threshold_logIntensity'])
    threshold     = exp(prctile(log(bounds),100/(Args.Npeaks+1)*(1:Args.Npeaks)))';
    backgroundint = exp(prctile(currpxints,[5 100/(Args.Npeaks)*(1:Args.Npeaks-1)])); 
    peakint       = exp(prctile(currpxints,95));
    exitflag      = -1;
else
    threshold = exp(bincentra(minloc));     % Set the threshold at the dip in the log intensity histogram
    % Also determine the background intensity
    if ~Args.useksdensity % don't use a minpeak
        [~,backgroundloc] = findpeaks(logIntdistb,'MinPeakProminence',minpeakprm,'MinPeakWidth',3,'Sortstr','descend','Npeaks',Args.Npeaks+1);
    else
        [~,backgroundloc] = findpeaks(logIntdistb(1:minloc),'MinPeakProminence',minpeakprm,'Sortstr','descend','Npeaks',Args.Npeaks);       
        [~,peakloc] = findpeaks(logIntdistb(minloc-1:end),'MinPeakProminence',minpeakprm,'Sortstr','descend','Npeaks',Args.Npeaks);
        backgroundloc = [backgroundloc peakloc+minloc];
    end        
    if numel(backgroundloc) ~= (Args.Npeaks+1)
        backgroundint = exp(prctile(currpxints,[5 100/(Args.Npeaks)*(1:Args.Npeaks-1)])); 
        peakint       = exp(prctile(currpxints,95));
        warning(['foreground and background intensity not detected automatically for '...
            num2str(Args.iteration) '. Changing the "bounds" parameter will likely help.'])
        exitflag      = -2;
    else
        backgroundint     = exp(bincentra(backgroundloc(1:end-1))); 
        peakint           = exp(bincentra(backgroundloc(end))); 
        exitflag          = 1;
    end
end

if Args.Npeaks > 1
    threshold = sort(threshold);
end
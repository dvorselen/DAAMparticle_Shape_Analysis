function[PC,GC,MC,XYZ,ThetaPhiR] = Smooth_and_Get_Curvatures(MPStats,smootharea)

NMPs = length(MPStats);
PC = cell(1,NMPs);
GC = cell(1,NMPs);
MC = cell(1,NMPs);

for iMP = 1:NMPs
    
    [~,~,edgecoor_sph] = check_processing_status(MPStats(iMP));
    
    % Obtain smoothed version
    ThetaPhiR = Smooth_spherical_mesh(edgecoor_sph,sqrt(smootharea/pi),'arc'); 
    [X,Y,Z] = sph2cart(ThetaPhiR(:,1),ThetaPhiR(:,2),ThetaPhiR(:,3));
    [XYZ]   = [X,Y,Z];

    % Create patch object
    FVsmooth.faces    = MPStats(iMP).TRI_Connectivity_SPHbound;
    FVsmooth.vertices = XYZ;
    
    % Obtain Gaussian an mean curvature maps (smoothed as above)
    pc = GetCurvatures(FVsmooth,0);
    GC{iMP} = pc(1,:).*pc(2,:);
    MC{iMP} = mean(pc); 
    PC{iMP} = pc;
    
end
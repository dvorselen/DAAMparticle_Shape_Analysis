% ImageAnalysis

% For a general overview of the methodology, see:
% Vorselen, D. et al. Microparticle traction force microscopy reveals subcellular force exertion patterns in immune cell�target interactions. Nat. Commun. 11, 20 (2020).
% (https://www.nature.com/articles/s41467-019-13804-z)

% This code will allow you to analyze the deformation and fluorescent signals 
% surrounding multiple deformable particles and at single timepoints (fixed samples)

% Run one section at a time by pressing ctrl-enter (or cmnd-enter) 

% If only a fluorescent signal from a particle (i.e. no cell-stain or label 
% for the free particle surface), the last useful cell is "Detect edges using 
% the 3D sobel operator and superlocalize edges using gaussian fitting". 
% MPStats (the structure containing the data) and text files with the edge 
% coordinates and triangulation, can still be saved using the last two cells

% Useful_plots.m can be used to make a couple of example plots

% If you use any part of this code , please cite the manuscript above in any publications

%% Read data files 

% Initialize a parallel pool and add the required dynamic javapaths
p = gcp;
p.IdleTimeout = 90;  % Give user additional time
w = warning('off', 'MATLAB:Java:DuplicateClass');
javaaddpath(fileparts(which('ParforProgressMonitor.class')));
warning(w);
% Check if deconvolutionlab2 can be found by matlab
if ~exist('DL2','class')
    javaaddpath([matlabroot filesep 'java' filesep 'jar' filesep 'DeconvolutionLab_2.jar'])
end
if ~exist('DL2','class')
    warning(['Deconvolutionlab2 is required for deconvolution. Please follow the instructions on '...
        'http://bigwww.epfl.ch/deconvolution/deconvolutionlab2/ and put DeconvolutionLab_2 '...
        'into the java folder of Matlab. Currently, deconvolution will be impossible.']);
end
clear('w');

% Open UI to select the data files
[data,Nfiles,FileName,PathName,xtra_metadata] = ReadBFImages('.tif');

%% Get required information from metadata

% If other info is required, check:
% http://static.javadoc.io/org.openmicroscopy/ome-xml/5.4.0/ome/xml/meta/MetadataRetrieve.html#getChannelName-int-int-

if exist('OGdata','var'); data = OGdata;  end
[IMStats,data1] = Extract_Essential_Metadata(data,FileName,PathName,'istimeseries',0);
% Check_Piezo_calibration;
[IM3Dcell,LLSMMask,zcorrfactor] = Extract_Images(data1,IMStats,IMStats(1).MPchannel);
[IMStats.IM3D]                  = IM3Dcell{:};
[IMStats.LLSMMask]              = LLSMMask{:};
[IMStats.zcorrfactor]           = deal(zcorrfactor);
[IMStats.PixelSizeZ_original]   = IMStats.PixelSizeZ;
[IMStats.PixelSizeZ]            = ArraytoCSL([IMStats.PixelSizeZ]/zcorrfactor);
if exist('Check_z_direction','file')==2
    try
        [IMStats] = Check_z_direction(IMStats,'IM3D',data,xtra_metadata,0);
    catch
         warning(['Failed to automatically detect acquistion direction of stack (up or down),'...
             ' this is only important when using deconvolution. If you are, confirm that the '...
             ' acquisition direction of the PSF and the data to analyze are the same.']);
    end
end
  
IMStats = orderfields(IMStats);
if ~all(size(data)==size(data1));     OGdata = data    ;   data = data1;     end
clear('IM3Dcell','LLSMMask','zcorrfactor','data1');  

%% Threshold the images, watershed and isolate particles

% Ask the user for the size of the desired padding around each particle when cropping particles
boxpadding = str2double(inputdlg({'in X,Y', 'in Z (bottom)', 'in Z (top)'},...
    'How many microns should the image around the particles be extended?',[1 90],{'4','5','10'}));

if isempty(boxpadding)
    
    warning('Operation cancelled by user')

else

    Opts = {'noparticlesOK',1,'multipleparticles','ok','watershed',1,'checkneighbours',0,'UseIncreasedThreshold',0};
    IMStats = Threshold_images_and_identify_particles(IMStats,Opts);

    % Isolate particles and create a particle-based structure
    MPStats = IMStats_to_MPStats(IMStats);
    MPStats = CropParticles(MPStats,IMStats,boxpadding);
    
end

clear('boxpadding','Opts');

%% Deconvolve

% Deconvolution is done using deconvolutionlab:
% see http://bigwww.epfl.ch/deconvolution/deconvolutionlab2/

PSF_Preparation
IMs_deconv = Deconvolve_using_DeconvolutionLab2({MPStats.IM_bgcorr},{MPStats.PSF3D});
[MPStats.IM3D_nondeconv] = MPStats.IM_bgcorr;
[MPStats.IM3D          ] = IMs_deconv{:};

clear('IMs_deconv')

%% Superlocalize particle edges and triangulate surface

% Ask the user about the use of sobel filtering and how much smoothing to apply to the image
edgedetectionsettings = inputdlg({'Use Sobel filter ("sobel") or derivative of line profiles ("direct")?',...
    'How much smoothing do you want to apply (in pixels)?',['Do you want to use stable mode? '...
    '(increases computational complexity and time, but can work better especially with adjacent particles)'],...
    'Desired (max) spacing between the points (in nm)?',['Do you want to use a regular spaced grid (reg) or '...
    'equidistant points (equi)?'],'Range in which is searched for the edge (in R)',...
    'Relative minimum peak intensity (1 - 100)'},'Edge Detection Settings',...
    [1 80],{'direct','1','0','250','equi','0.25 - 0.75','99'});

% Apply sobel filtering (if desired)
usesobel = strcmpi(edgedetectionsettings{1},'sobel');
if usesobel
    XYZedges = Apply_3DSobel_filter({MPStats.IM3D},str2double(edgedetectionsettings{2}));
    [MPStats.IMedges] = XYZedges{:};
end

dashloc = strfind(edgedetectionsettings{6},'-');
radialbounds = [str2double(edgedetectionsettings{6}(1:(dashloc-1)))...
     str2double(edgedetectionsettings{6}((dashloc+1):end))];
 
[MPStats,Residuals_Problems_excluded] = Superlocalize_edges_using_gaussian_fitting(MPStats,...
    str2double(edgedetectionsettings{4})/1000,edgedetectionsettings{5},'DirectDerivative',~usesobel,...
    'usestablemode',str2double(edgedetectionsettings{3}),'smooth',str2double(edgedetectionsettings{2}),...
    'RadialBounds',radialbounds,'RelMinPeakSize',edgedetectionsettings{7});

%% Triangulate surface and determine particle statistics



MPStats = Triangulate_surface_and_determine_particle_statistics(MPStats);

if ~exist('p','var')
    p = gcp;
elseif p.Connected == 0
    p = gcp;
else
    p.IdleTimeout = 30;   % reset idletime on parpool
end 

clear('XYZedges','edgedetectionsettings','usesobel','dashloc','radialbounds')

%% Determine particle coverage by a secondary signal
   
if exist('data','var')
    Stain_channel = Find_Stain_Channel(data,MPStats);
    [MPStats,IMStats] = Analyze_Secondary_Signal(data,MPStats,IMStats,Stain_channel,'stain');
else
    stainname = select_stain(MPStats);
    [MPStats,IMStats] = Analyze_Secondary_Signal([],MPStats,IMStats,[],stainname);
end

clear('Stain_channel','stain_indicates_contact','use_integrated_intensity','stainname')

%% Convert secondary signal to mask, and determine the position of the base of the cup

% Choose to use the maximum intensity or integrated intensity. To choose, you can plot both by running this line:
% figure; subplot(2,1,1); imagesc(max(MPStats(1).IMstain_radial,[],3)'); axis equal; axis off; subplot(2,1,2); imagesc(sum(MPStats(1).IMstain_radial,3)'); axis equal; axis off;
% The upper one is max intensity, lower integrated intensity

fnames   = fieldnames(MPStats);

if any(contains(fnames,'stain_ch'))  
    userinputstain = inputdlg({'Use maximum intensity (max) or integrated (int) intensity signal?',...
        'Does the first stain indicate contact (fluorescent labeled cell, 1) or lack of contact (free boundary stain, 0)',...
        'Two stains found. Do you want, to use both for masking?'},...
        'Stain analysis options',[1 70],{'max','0','0'});  
    use_two_stains = str2double(userinputstain(3));
else
    userinputstain = inputdlg({'Use maximum intensity (max) or integrated (int) intensity signal?',...
        'Does stain indicate contact (fluorescent labeled cell, 1) or lack of contact (free boundary stain, 0)'},...
        'Stain analysis options',[1 70],{'max','0'});
    use_two_stains = 0;
end

use_integrated_intensity = strcmp(userinputstain(1),'int');
stain_indicates_contact  = str2double(userinputstain(2));

if use_two_stains
    
    adchname = regexp(fnames,'stain_ch\d_int','match');
    adchname = [adchname{:}];
    adchname = adchname{1};
    if use_integrated_intensity 
        adchname  = [adchname '_integrated'];
        regchname = 'stain_int_integrated';
    else
        regchname = 'stain_int';
    end
    
    [MPStats.stain_indicates_contact] = deal(stain_indicates_contact);       
    if stain_indicates_contact
        stain_indicates_contact = [1,0];
    else
        stain_indicates_contact = [0,1];
    end
    
    MPStats = Convert_double_signal_to_mask({MPStats.(regchname)},{MPStats.(adchname)},...
        MPStats,'stain_indicates_contact',stain_indicates_contact);

else
    MPStats = Convert_secondary_signal_to_mask(MPStats,stain_indicates_contact,...
        'use_integrated_intensity',use_integrated_intensity);
end

% Determine the position of the base and align cups
MPStats = Determine_base_position_and_align(MPStats,'BaseLat',-pi/2,'BaseColongitude',0);

clear('use_integrated_intensity','stain_indicates_contact','use_two_stains','fnames','regchname','adchname')

%% Optional: determine particle coverage by additional signals

% Check if at least one stain has been analyzed so far
if ~isfield(MPStats,'IMstain')
    error(['If this is the first fluorescent signal you analyze, use "Determine'...
        ' particle coverage by a secondary signal" two cells up'])
end

if exist('data','var')
    Stain_channel = Find_Stain_Channel(data,MPStats);
    [MPStats,IMStats] = Analyze_Secondary_Signal(data,MPStats,IMStats,Stain_channel,'stain',1);
else
    stainname = select_stain(MPStats);
    [MPStats,IMStats] = Analyze_Secondary_Signal([],MPStats,IMStats,[],stainname);
end

clear('stainname')

%% Optional: manually check and update masks

MPStats2 = MPStats;
for i = 1:length(MPStats)
    MPStats(i) = Update_Mask_GUI(MPStats(i));
end

warning(['If you are not satisfied with your changes you ' ...
    'can revert them all by typing: "MPStats = MPStats2", ' ...
    'or revert individual particles by typing "MPStats(x) = MPStats2(x)"'...
    newline 'When you are all done, you can enter "clear MPStats2"']);

%% Optional: sort particles by the contact area fraction

[~,sortidx] = sort([MPStats.Fraction_engulfed]);
MPStats = MPStats(sortidx);

clear sortidx

%% Save IMStats and MPStats and the coordinates, triangulation and mask

% To reduce the size of IMStats, I convert a couple of large variables to
% singles (the main image and the stain channels)
fnames = fieldnames(IMStats);
fnames = fnames(contains(fnames,'IM3D'));
for i = 1:length(fnames)
    IM3Dsingle = cellfun(@single,{IMStats.(fnames{i})},'UniformOutput',false);
    [IMStats.(fnames{i})] = deal(IM3Dsingle{:});
end    

[restpathname,p1] = fileparts(IMStats(1).PathName(1:end-1));       [~,p2] = fileparts(restpathname);
save(['MPStats_' p2 '_' p1 '_' datestr(now,'yyyymmdd_HHMM') '.mat'],'MPStats','-v7.3')
save(['IMStats_' p2 '_' p1 '_' datestr(now,'yyyymmdd_HHMM') '.mat'],'IMStats','-v7.3')

% Save the coordinates, triangulation and mask
Save_Coordinates_Triangulation_Mask(MPStats)

clear('IM3Dsingle','p1','p2','restpathname','fnames');
